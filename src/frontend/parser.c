#include "parser.h"
#include "ast.c"

struct AstNode *parse_stmt(struct Parser *p, int prec);
struct AstNode *parse_expr(struct Parser *p, int prec);
struct ProcHeader *parse_header(struct Parser *p);
struct AstRoot *parse_enum(struct Parser *p);
struct AstRoot *parse_struct(struct Parser *p);
struct AstRoot *parse_block(struct Parser *p);

// TODO: fix all the edge cases, implement proper error handling, add paren handling, put line numbers in nodes yada yada

void parerror(struct Parser *p, char *string);
void breakpoint() {};

int eat_token(struct Parser *p) {
	if (p->pos < p->token_len) {
		p->pos++;
		return 1;
	}
	return 0;
}

struct Token *get_token(struct Parser *p) {
	if (p->pos < p->token_len) {
		return &p->tokens[p->pos];
	} else {
		parerror(p, "reached end");
		return NULL;
	}
}

void parerror(struct Parser *p, char *string) {
	p->panic = 1;
	printf("ERROR: %s\n", string);
	struct Token *token = get_token(p);
	int curl = 1;
	int i = 0;
	int t = 0;
	int s = 0;
	char curc;
	for (; i < p->t->len; i++) {
		curc = p->t->string[i];
		if (t) {
			if (curc == '\n') {
				break;
			}
		} else {
			if (curc == '\n') {
				curl++;
			}
			if (curl == token->l0) {
				s = i;
				t = 1;
			}
		}
	}
	int tabs = 0;
	for (int b = s; b < i; b++) {
		if (p->t->string[b] == '\t') {
			tabs++;
		}
		printf("%c", p->t->string[b]);
	}
	printf("\n");
	for (i = 0; i < tabs; i++) {
		printf("\t");
	}
	for (i = 0; i < token->c0 - tabs; i++) {
		printf(" ");
	}
	printf("^\n");
	printf("at: %i:%i\n", token->l0, token->c0);
}

struct Token *peek_parser(struct Parser *p, int amount) {
	if (p->pos+amount >= p->token_len) {
		parerror(p, "reached end");
		return NULL;
	}
	return &p->tokens[p->pos+amount];
}


void insert_token_string(char **string, struct Token *t) {
	*string = malloc(t->len + 1);
	memcpy(*string, t->string, t->len);
	(*string)[t->len] = '\0';
}

void insert_token_string_lit(char **string, struct Token *t) {
	*string = malloc(t->len + 1);
	int len = 0;
	int escaped = 0;
	for (int i = 0; i < t->len; i++) {
		switch (t->string[i]) {
			case '\\':
				escaped = 1;
				continue;
			case 'n':
				if (escaped) {
					string[0][len] = '\n';
					len++;
					continue;
				}
			case 't':
				if (escaped) {
					string[0][len] = '\t';
					len++;
					continue;
				}
		}
		string[0][len] = t->string[i];
		len++;
	}
	(*string)[len] = '\0';
}

/*
int get_precedence(struct AstNode *node) {
	switch (node->type) {
		case ast_proc_call:
			return 1;
		case ast_binary:
			{
				struct BinaryOp *binary = node->node;
				switch (binary->op) {
					case op_subscript:
						return 1;
					case op_cast:
						return 2;
					// * / %
					case op_mult:
					case op_div:
					case op_mod:
						return 3;
					// + -
					case op_plus:
					case op_minus:
						return 4;
					// < > <= >=
					case op_lt:
					case op_gt:
					case op_lt_eq:
					case op_gt_eq:
						return 6;
					// == !=
					case op_eq:
					case op_neq:
						return 7;
					// &&
					case op_log_and:
						return 11;
					// ||
					case op_log_or:
						return 12;
					// range
					case op_range:
						return 13;
					// wtf
					case op_assign:
					case op_decl:
					case op_def:
					case op_const:
					case op_plus_a:
					case op_minus_a:
					case op_mult_a:
					case op_div_a:
					case op_mod_a:
						return 14;
				}
			}
		case ast_unary:
			{
				struct UnaryOp *unary = node->node;
				switch (unary->op) {
					case op_paren:
						return 1;
					case op_plus:
					case op_minus:
					case op_not:
					case op_take_pointer:
					case op_ptr_type:
					case op_deref:
						return 2;
				}
			}
		// TODO: fix this stupid stuff and just dont do any precedence ordering with these
		case ast_literal:
		case ast_ident:
		case ast_struct:
		case ast_enum:
		case ast_proc:
		case ast_cproc_hdr:
		case ast_arr_type:
		case ast_array:
		// TODO: hacky, just fix the semicolon parsing smh my head
		case -1:
			return 0;
	}
	printf("this is not supposed to happen\n");
	print_node(node);
	return 0;
}

struct AstNode *fix_precedence(struct AstNode *node) {
	int prec_top = get_precedence(node);
	struct BinaryOp *top = node->node;
	int prec_bot = get_precedence(top->right);
	struct AstNode *node_bot = top->right;
	// bruh the array :'(
	if (top->op == op_subscript && ((struct BinaryOp*)node_bot->node)->op == op_range) {
		return node;
	}
	if (prec_bot > prec_top) {
		switch (node_bot->type) {
			case ast_binary:
				{
					struct BinaryOp *bot = node_bot->node;
					top->right = bot->left;
					bot->left = node;
					return node_bot;
				}
		}
	}
	return node;
}
*/

int get_precedence(struct Token *t) {
	switch (t->type) {
		case ';':
			return 0;
		case '=':
		case token_const:
			return 0;
		case '(':
		case '[':
		case '.':
			return 9;
		case token_kw_as:
			return 8;
		case token_range:
			return 1;
		case token_compare_equal:
		case token_compare_nequal:
			return 4;
		case '<':
		case '>':
		case token_compare_lt_eq:
		case token_compare_gt_eq:
			return 5;
		case token_logical_or:
			return 2;
		case token_logical_and:
			return 3;
		case '+':
			return 6;
		case '-':
			return 6;
		case '*':
			return 7;
		case '/':
			return 7;
		case '%':
			return 7;
		default:;
			// printf("no prec defined for token\n");
			// print_token(t);
	}
}

struct AstNode *ident(struct Token *t) {
	struct AstNode *node = malloc(sizeof(struct AstNode));
	node->l0 = t->l0;
	node->c0 = t->c0;
	node->l1 = t->l1;
	node->c1 = t->c1;
	insert_token_string((char **)&node->node, t);
	node->type = ast_ident;
	return node;
}

struct AstNode *operation(enum Operator operator, struct AstNode *left, struct AstNode *right) {
	struct AstNode *node = malloc(sizeof(struct AstNode));
	struct BinaryOp *operation = malloc(sizeof(struct BinaryOp));
	node->type = ast_binary;
	operation->op = operator;
	operation->left = left;
	operation->right = right;
	node->l0 = left->l0;
	node->c0 = left->c0;
	node->l1 = right->l1;
	node->c1 = right->c1;
	node->node = operation;
	//node = fix_precedence(node);
	return node;
}

struct AstNode *type(struct Parser *p) {
	eat_token(p);
	struct Token *cur_token;
	cur_token = get_token(p);
	struct AstNode *node = malloc(sizeof(struct AstNode));
	node->l0 = cur_token->l0;
	node->c0 = cur_token->c0;
	node->l1 = cur_token->l1;
	node->c1 = cur_token->c1;
	node->type = -1;
	switch (cur_token->type) {
		case '[':
			{
				struct ArrayLit *array = malloc(sizeof(struct ArrayLit));
				eat_token(p);
				cur_token = get_token(p);
				switch (cur_token->type) {
					case token_int_literal:
						array->type = arrayt_stack;
						array->capacity = cur_token->integer;
						break;
					case token_range:
						array->type = arrayt_dynamic;
						array->capacity = 0;
						break;
					case ']':
						array->type = arrayt_static;
						array->capacity = 0;
						array->on = type(p);
						node->node = array;
						node->type = ast_arr_lit;
						return node;
				}
				eat_token(p);
				if (get_token(p)->type != ']') {
					parerror(p, "expected ']'");
				}
				array->on = type(p);
				node->node = array;
				node->type = ast_arr_lit;
				return node;
			}
		case '*':
			{
				struct UnaryOp *unary = malloc(sizeof(struct UnaryOp));
				unary->op = op_mult;
				unary->on = type(p);
				node->node = unary;
				node->type = ast_unary;
				return node;
			}
		case token_ident:
			{
				node->type = ast_ident;
				insert_token_string((char **)&node->node, cur_token);
				if (peek_parser(p, 1)->type == '.') { // getting a type from another module
					while (peek_parser(p, 1)->type == '.') {
						eat_token(p);
						eat_token(p);
						// cur_token = get_token(p);
						node = operation(op_access, node, type(p));
					}
				}
				return node;
			}
		case token_kw_struct:
			node->type = ast_struct;
			eat_token(p);
			node->node = parse_struct(p);
			return node;

		case token_kw_enum:
			node->type = ast_enum;
			eat_token(p);
			node->node = parse_enum(p);
			return node;

		case token_kw_cproc:
			node->type = ast_cproc_hdr;
			eat_token(p);
			node->node = parse_header(p);
			return node;

		case '(':
			{
				struct ProcHeader *hdr = parse_header(p);
				cur_token = peek_parser(p, 1);
				if (cur_token->type == ';') {
					node->node = hdr;
					node->type = ast_proc_hdr;
					return node;
				}
				eat_token(p);
				struct Proc *proc = malloc(sizeof(struct Proc));
				proc->header = hdr;
				proc->body = parse_block(p);
				node->node = proc;
				node->type = ast_proc;
				return node;
			}

	}
	parerror(p, "unexpected token, parsing type");
	return 0;
}

struct AstRoot *parse_args(struct Parser *p) {
	struct AstRoot *args = malloc(sizeof(struct AstRoot));
	u64 args_max = 20;
	args->len = 0;
	args->nodes = malloc(sizeof(struct AstNode)* args_max);
	// TODO: init all the rest
	struct Token *cur_token;
	cur_token = get_token(p);
	if (peek_parser(p, 1)->type == ')') {
		eat_token(p);
		return args;
	}
	for (;;) {
		if (args->len == args_max) {
			args_max += 20;
			args->nodes = realloc(args->nodes, sizeof(struct AstNode) * args_max);
		}
		args->nodes[args->len] = *parse_expr(p, 0);
		args->len++;
		eat_token(p);
		cur_token = get_token(p);
		if (cur_token->type == ',') {
			continue;
		}
		if (cur_token->type == ')') {
			return args;
		}
		parerror(p, "expected ',' or ')'");
		return args;
	}
}

struct ProcHeader *parse_header(struct Parser *p) {
	struct AstRoot *args = malloc(sizeof(struct AstRoot));
	struct ProcHeader *proc_hdr = malloc(sizeof(struct ProcHeader));
	proc_hdr->args = args;
	u64 args_max = 20;
	args->len = 0;
	args->nodes = malloc(sizeof(struct AstNode)* args_max);
	eat_token(p);
	struct Token *cur_token;
	cur_token = get_token(p);
	for (;;) {
		if (args->len == args_max) {
			args_max += 20;
			args->nodes = realloc(args->nodes, sizeof(struct AstNode) * args_max);
		}
		cur_token = get_token(p);
		switch (cur_token->type) {
			case token_ident:
				{
					struct AstNode *temp = ident(cur_token);
					eat_token(p);
					cur_token = get_token(p);
					if (cur_token->type != ':') {
						parerror(p, "expected ':'");
					}
					args->nodes[args->len] = *operation(op_decl, temp, type(p));
					args->len++;
					eat_token(p);
					break;
				}
			case ',':
				eat_token(p);
				break;
			case ')':
				{
					int temp = peek_parser(p, 1)->type;
					if (temp != '{' && temp != ';') {
						proc_hdr->return_val = type(p);
					} else {
						proc_hdr->return_val = 0;
					}
					return proc_hdr;
				}
			default:
				parerror(p, "unexpected token, parsing args");
				return proc_hdr;
		}
	}
}

struct AstRoot *parse_array(struct Parser *p) {
	struct AstRoot *array = malloc(sizeof(struct AstRoot));
	u64 arr_max = 20;
	array->len = 0;
	array->nodes = malloc(sizeof(struct AstNode) * arr_max);
	if (get_token(p)->type == ']') {
		return array;
	}
	for (;;) {
		if (array->len == arr_max) {
			arr_max += 100;
			array->nodes = realloc(array->nodes, sizeof(struct AstNode) * arr_max);
		}
		array->nodes[array->len] = *parse_expr(p, 0);
		array->len++;
		eat_token(p);
		if (get_token(p)->type == ',') {
			continue;
		}
		if (get_token(p)->type == ']') {
			return array;
		}
		parerror(p, "expected ',' or ']'");
		return array;
	}
}

struct AstRoot *parse_struct(struct Parser *p) {
	struct AstRoot *root = malloc(sizeof(struct AstRoot));
	u64 root_max = 30;
	root->len = 0;
	struct Token *cur_token = peek_parser(p, 1);
	if (cur_token->type != token_ident) {
		return root;
	}
	struct AstNode *temp;
	root->nodes = malloc(sizeof(struct AstNode) * root_max);
	for (;;) {
		if (root->len == root_max) {
			root_max += 100;
			root->nodes = realloc(root->nodes, sizeof(struct AstNode) * root_max);
		}
		cur_token = peek_parser(p, 1);
		if (cur_token->type == '}') {
			eat_token(p);
			return root;
		}
		if (cur_token->type != token_ident) {
			parerror(p, "expected ident");
			segexit();
		}
		temp = parse_expr(p, 0);
		eat_token(p);
		cur_token = get_token(p);
		if (cur_token->type == ':') {
			temp = operation(op_decl, temp, type(p));
			eat_token(p);
			cur_token = get_token(p);
			if (cur_token->type == '=') {
				temp = operation(op_assign, temp, parse_expr(p, 0));
				eat_token(p);
			}
		} else if (cur_token->type == token_def) {
			temp = operation(op_def, temp, parse_expr(p, 0));
			eat_token(p);
		} else {
			parerror(p, "expected ':' or ':='");
			segexit();
		}
		root->nodes[root->len] = *temp;
		root->len++;
		cur_token = get_token(p);
		if (cur_token->type != ',') {
			parerror(p, "expected ','");
			segexit();
			return root;
		}
	}
}

struct AstRoot *parse_enum(struct Parser *p) {
	struct AstRoot *root = malloc(sizeof(struct AstRoot));
	u64 root_max = 30;
	root->len = 0;
	struct Token *cur_token = peek_parser(p, 1);
	if (cur_token->type != token_ident) {
		return root;
	}
	struct AstNode *temp;
	root->nodes = malloc(sizeof(struct AstNode) * root_max);
	for (;;) {
		if (root->len == root_max) {
			root_max += 100;
			root->nodes = realloc(root->nodes, sizeof(struct AstNode) * root_max);
		}
		cur_token = peek_parser(p, 1);
		if (cur_token->type == '}') {
			eat_token(p);
			return root;
		}
		if (cur_token->type != token_ident) {
			parerror(p, "expected ident");
		}
		temp = parse_expr(p, 0);
		eat_token(p);
		cur_token = get_token(p);
		if (cur_token->type == '=') {
			temp = operation(op_assign, temp, parse_expr(p, 0));
			eat_token(p);
		}
		root->nodes[root->len] = *temp;
		root->len++;
		cur_token = get_token(p);
		if (cur_token->type != ',') {
			parerror(p, "expected ','");
			return root;
		}
	}
}

struct AstRoot *parse_block(struct Parser *p) {
	struct AstRoot *root = malloc(sizeof(struct AstRoot));
	struct AstNode *temp_node;
	u64 root_max = 100;
	root->nodes = malloc(sizeof(struct AstNode) * root_max);
	root->len = 0;
	for (;;) {
		if (root->len == root_max) {
			root_max += 100;
			root->nodes = realloc(root->nodes, sizeof(struct AstNode) * root_max);
		}
		temp_node = parse_stmt(p, 0);
		if (temp_node->type == -1) {
			if (p->tokens[p->pos].type == '}') {
				break;
			}
			continue;
		}
		root->nodes[root->len] = *temp_node;
		root->len++;
	}
	return root;
}

struct AstNode *parse_expr(struct Parser *p, int precedence) {
	// struct Tokenizer *t = p->t;
	struct AstNode *node = malloc(sizeof(struct AstNode));
	struct Token *cur_token;
	node->type = -1;
	eat_token(p);
	cur_token = get_token(p);
	node->l0 = cur_token->l0;
	node->c0 = cur_token->c0;

	switch (cur_token->type) {
		case token_ident:
			node->type = ast_ident;
			insert_token_string((char **)&node->node, cur_token);
			if (peek_parser(p, 1)->type == '(') {
				struct ProcCall *call = malloc(sizeof(struct ProcCall));
				call->name = node->node;
				node->type = ast_proc_call;
				node->node = call;
				eat_token(p);
				call->args = parse_args(p);
				break;
			}
			break;

		case token_int_literal:
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 0;
				lit->integer = cur_token->integer;
			}
			break;

		case token_float_literal:
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 1;
				lit->fl = cur_token->fl;
			}
			break;

		case token_string_literal:
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 2;
				insert_token_string_lit(&lit->string, cur_token);
			}
			break;

		case token_true:
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 3;
				lit->integer = 1;
			}
			break;

		case token_false:
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 3;
				lit->integer = 0;
			}
			break;

		case token_no_init:
			node->type = ast_no_init;
			break;

		case token_char_literal:
			//TODO do escape thingies like unicode
			node->type = ast_literal;
			node->node = malloc(sizeof(struct Literal));
			{
				struct Literal *lit = node->node;
				lit->type = 5;
				insert_token_string_lit(&lit->string, cur_token);
			}
			break;

		case '-':
			eat_token(p);
			cur_token = get_token(p);
			node->type = ast_unary;
			struct UnaryOp *op = malloc(sizeof(struct UnaryOp));
			node->node = op;
			op->op = op_minus;
			op->on = malloc(sizeof(struct AstNode));
			switch (cur_token->type) {
				case token_ident:
					op->on->type = ast_ident;
					insert_token_string((char**)&op->on->node, cur_token);
					break;

				case token_int_literal:
					op->on->type = ast_literal;
					op->on->node = malloc(sizeof(struct Literal));
					{
						struct Literal *lit = op->on->node;
						lit->type = 0;
						lit->integer = cur_token->integer;
					}
					break;

				case token_float_literal:
					op->on->type = ast_literal;
					op->on->node = malloc(sizeof(struct Literal));
					{
						struct Literal *lit = op->on->node;
						lit->type = 1;
						lit->fl = cur_token->fl;
					}
					break;
			}
			break;


		case '!':
			{
				struct UnaryOp *op = malloc(sizeof(struct UnaryOp));
				node->node = op;
				node->type = ast_unary;
				op->op = op_not;
				op->on = parse_expr(p, 0);
				break;
			}

		case '*':
			{
				struct UnaryOp *op = malloc(sizeof(struct UnaryOp));
				node->node = op;
				node->type = ast_unary;
				op->op = op_deref;
				op->on = parse_expr(p, 0);
				break;
			}

		case '&':
			{
				struct UnaryOp *op = malloc(sizeof(struct UnaryOp));
				node->node = op;
				node->type = ast_unary;
				op->op = op_take_pointer;
				op->on = parse_expr(p, 0);
				// TODO fix precedence on all 3 of them
				break;
			}

		case '(':
			{
				//u64 pos = p->pos;
				//struct AstNode *next_node = parse_expr(p);
				//p->pos = pos;
				// TODO: dont do something this stupid
				int paren_depth = 1;
				int paren_pos = 1;
				struct Token *paren_cur;
				int header = 0;
				paren_cur = peek_parser(p, paren_pos);
				if (paren_cur->type == ')') {
					header = 1;
				} else {
					for (;;) {
						paren_cur = peek_parser(p, paren_pos);
						if (paren_cur->type == '(') {
							paren_depth++;
						} else if (paren_cur->type == ')') {
							paren_depth--;
						}
						if (paren_depth == 1) {
							if (paren_cur->type == ':') {
								header = 1;
								break;
							}
						}
						if (paren_depth == 0) {
							break;
						}
						paren_pos++;
					}
				}
				if (header) {
				} else {
					struct UnaryOp *op = malloc(sizeof(struct UnaryOp));
					node->node = op;
					node->type = ast_unary;
					op->op = op_paren;
					op->on = parse_expr(p, 0);
					eat_token(p);
					break;
				}
			}

		case token_range:
			{
				struct AstNode *temp = parse_expr(p, 0);
				node = operation(op_range, node, temp);
				return node;
			}

		case '.':
			{
				struct Token *temp = peek_parser(p, 1);
				if (temp->type != token_ident) {
					parerror(p, "unexpected token when parsing enum literal, expected ident");
				}
				eat_token(p);
				cur_token = get_token(p);
				node->type = ast_literal;
				node->node = malloc(sizeof(struct Literal));
				{
					struct Literal *lit = node->node;
					lit->type = 4;
					insert_token_string(&lit->string, cur_token);
				}
				break;
			}


		case '[':
			{
				node->type = ast_array;
				node->node = parse_array(p);
				break;
			}

		case ']':
			return node;

	}

	struct Token *temp;
	temp = peek_parser(p, 1);
	int prec = get_precedence(temp);
	if (prec < precedence) {
		return node;
	}
	while (prec >= precedence) {//precedence <= prec) {
		switch (temp->type) {
			case '{':
				return node;
			case ';':
				return node;
			case ',':
				return node;
			case ']':
				return node;
			case ')':
				return node;

			// stuff so that deref storing doesn't break
			case '=':
			case ':':
			case token_def:
			case token_plus_a:
			case token_minus_a:
			case token_mult_a:
			case token_div_a:
			case token_mod_a:
				return node;


			// struct member access
			case '.':
				eat_token(p);
				cur_token = get_token(p);
				node = operation(op_access, node, parse_expr(p, prec + 1));
				break;
			case '[':
				eat_token(p);
				struct AstNode *temp = parse_expr(p, 0);
				if (temp->type == ast_binary && ((struct BinaryOp*)temp->node)->op == op_range) {
					node = operation(op_slice, node, temp);
					if (((struct BinaryOp*)temp->node)->right->type != -1) {
						eat_token(p);
					}
				} else {
					node = operation(op_subscript, node, temp);
					eat_token(p);
				}
				break;
			case token_kw_as:
				eat_token(p);
				node = operation(op_cast, node, type(p));
				break;


			case token_range:
				eat_token(p);
				struct AstNode *fard = parse_expr(p, 0);
				node = operation(op_range, node, fard);
				//TODO fix precedence
				/*
				   struct Range *range = malloc(sizeof(struct Range));
				   range->low = node;
				   range->high = parse_expr(p);
				   node = malloc(sizeof(struct AstNode));
				   node->type = ast_range;
				   node->node = range;
				   */
				break;

				// comparisons
			case token_compare_equal:
				eat_token(p);
				node = operation(op_eq, node, parse_expr(p, prec + 1));
				break;
			case token_compare_nequal:
				eat_token(p);
				node = operation(op_neq, node, parse_expr(p, prec + 1));
				break;
			case '<':
				eat_token(p);
				node = operation(op_lt, node, parse_expr(p, prec + 1));
				break;
			case '>':
				eat_token(p);
				node = operation(op_gt, node, parse_expr(p, prec + 1));
				break;
			case token_compare_lt_eq:
				eat_token(p);
				node = operation(op_lt_eq, node, parse_expr(p, prec + 1));
				break;
			case token_compare_gt_eq:
				eat_token(p);
				node = operation(op_gt_eq, node, parse_expr(p, prec + 1));
				break;

				// logical
			case token_logical_or:
				eat_token(p);
				node = operation(op_log_or, node, parse_expr(p, prec + 1));
				break;
			case token_logical_and:
				eat_token(p);
				node = operation(op_log_and, node, parse_expr(p, prec + 1));
				break;

				// math
			case '+':
				eat_token(p);
				node = operation(op_plus, node, parse_expr(p, prec + 1));
				break;
			case '-':
				eat_token(p);
				node = operation(op_minus, node, parse_expr(p, prec + 1));
				break;
			case '*':
				eat_token(p);
				node = operation(op_mult, node, parse_expr(p, prec + 1));
				break;
			case '/':
				eat_token(p);
				node = operation(op_div, node, parse_expr(p, prec + 1));
				break;
			case '%':
				eat_token(p);
				node = operation(op_mod, node, parse_expr(p, prec + 1));
				break;
		}
		temp = peek_parser(p, 1);
    prec = get_precedence(temp);
		if (!temp) {
			return node;
		}
	}
	// TODO remove
	return node;
}

struct AstNode *parse_stmt(struct Parser *p, int precedence) {
	// struct Tokenizer *t = p->t;
	struct AstNode *node = malloc(sizeof(struct AstNode));
	struct Token *cur_token;
	node->type = -1;

	if (!p->first) {
		eat_token(p);
	} else {
		p->first = 0;
	}
	cur_token = get_token(p);
	node->l0 = cur_token->l0;
	node->c0 = cur_token->c0;

	switch (cur_token->type) {
		case token_ident:
			node->type = ast_ident;
			insert_token_string((char**)&node->node, cur_token);
			break;
		
		case token_kw_foreign:
			node->type = ast_foreign;
			node->node = malloc(sizeof(struct AstForeign));
			struct AstForeign *af = node->node;
			eat_token(p);
			cur_token = get_token(p);
			insert_token_string(&af->libname, cur_token);
			eat_token(p);
			af->block = parse_block(p); // TODO add error handling for putting anything that is not a proc header def in there
			return node;

		case token_kw_for:
			node->type = ast_for;
			node->node = malloc(sizeof(struct AstFor));
			struct AstFor *loop = node->node;
			eat_token(p);
			cur_token = get_token(p);
			if (cur_token->type != token_ident) {
				parerror(p, "for loop variable name has to be a name");
			}
			loop->var = malloc(cur_token->len + 1);
			insert_token_string(&loop->var, cur_token);
			eat_token(p);
			cur_token = get_token(p);
			if (cur_token->type != token_kw_in) {
				parerror(p, "for loop variable name has to be followed by \"in\" keyword");
			}
			loop->iterator = parse_expr(p, 0);
			eat_token(p);
			loop->block = parse_block(p);
			return node;

		case token_kw_loop:
			eat_token(p);
			node->node = parse_block(p);
			node->type = ast_loop;
			return node;

		case token_kw_while:
			{
				struct AstWhile *wl = malloc(sizeof(struct AstWhile));
				wl->cond = parse_expr(p, 0);
				eat_token(p);
				wl->block = parse_block(p);
				node->node = wl;
				node->type = ast_while;
				return node;
			}

		case token_kw_match:
			{
				struct Match *match = malloc(sizeof(struct Match));
				node->node = match;
				node->type = ast_match;
				match->on = parse_expr(p, 0);
				eat_token(p);
				cur_token = get_token(p);
				if (cur_token->type != '{') {
					parerror(p, "expected '{' in match statement");
				}
				u64 maxs = 20;
				match->len = 0;
				match->conds = malloc(sizeof(struct AstNode*) * maxs);
				match->blocks = malloc(sizeof(struct AstRoot*) * maxs);
				if (peek_parser(p, 1)->type == '}') {
					eat_token(p);
					break;
				}
				for (;;) {
					if (match->len == maxs) {
						maxs += 20;
						match->conds = realloc(match->conds, sizeof(struct AstNode*) * maxs);
						match->blocks = realloc(match->blocks, sizeof(struct AstRoot*) * maxs);
					}
					match->conds[match->len] = parse_expr(p, 0);
					eat_token(p);
					cur_token = get_token(p);
					if (cur_token->type != '{') {
						parerror(p, "expected '{' in match statement");
					}
					match->blocks[match->len] = parse_block(p);
					cur_token = get_token(p);
					if (cur_token->type != '}') {
						parerror(p, "expected '}' in match statement");
					}
					if (peek_parser(p, 1)->type == '}') {
						eat_token(p);
						match->len++;
						break;
					}
					eat_token(p);
					cur_token = get_token(p);
					if (cur_token->type != ',') {
						parerror(p, "expected ',' in match statement");
					}
					match->len++;
				}
				return node;
			}

		case token_kw_if:
			node->type = ast_if;
			node->node = malloc(sizeof(struct AstIf));
			struct AstIf *cond = node->node;
			cond->cond = parse_expr(p, 0);
			eat_token(p);
			cond->block = parse_block(p);
			if (p->pos+1 < p->token_len && peek_parser(p, 1)->type == token_kw_else) {
				eat_token(p);
				if (p->pos+1 < p->token_len && peek_parser(p, 1)->type == token_kw_if) {
					cond->else_block = parse_stmt(p, 0);
				} else if (p->pos+1 < p->token_len && peek_parser(p, 1)->type == '{') {
					struct AstNode *else_node = malloc(sizeof(struct AstNode));
					cur_token = get_token(p);
					node->l0 = cur_token->l0;
					node->c0 = cur_token->c0;
					node->l1 = cur_token->l1;
					node->c1 = cur_token->c1;
					eat_token(p);
					else_node->type = ast_block;
					else_node->node = parse_block(p);
					cond->else_block = else_node;
				}
			}
			return node;

		case token_kw_break:
			{
			node->type = ast_cflow;
			struct CFlow *cf = malloc(sizeof(struct CFlow));
			cf->type = 0;
			node->node = cf;
			cf->on = 0;
			if (peek_parser(p, 1)->type == ';') {
				return node;
			}
			return node;
			}

		case token_kw_return:
			node->type = ast_cflow;
			struct CFlow *cf = malloc(sizeof(struct CFlow));
			cf->type = 1;
			node->node = cf;
			if (peek_parser(p, 1)->type == ';') {
				return node;
			}
			cf->on = parse_expr(p, 0);
			return node;


		case token_kw_module:
			node->type = ast_module_stmt;
			eat_token(p);
			cur_token = get_token(p);
			insert_token_string((char **)&node->node, cur_token);
			return node;

		case token_kw_import:
			node->type = ast_import;
			eat_token(p);
			cur_token = get_token(p);
			insert_token_string((char **)&node->node, cur_token);
			return node;

		case token_kw_include:
			node->type = ast_include;
			eat_token(p);
			cur_token = get_token(p);
			insert_token_string((char **)&node->node, cur_token);
			return node;




		case '*':
			{
				struct PointerLit *lit = malloc(sizeof(struct PointerLit));
				lit->pointer_level = 0;
				cur_token = peek_parser(p, 1);
				lit->pointer_level++;
				while (cur_token->type == '*') {
					lit->pointer_level++;
					eat_token(p);
					cur_token = peek_parser(p, 1);
				}
				lit->on = parse_expr(p, 0);
				node->node = lit;
				node->type = ast_ptr_literal;
				break;
			}
		case '#':
			eat_token(p);
			cur_token = get_token(p);
			if (cur_token->type != token_ident) {
				parerror(p, "directive needs ident, unexpected token");
			}
			struct Directive *dir = malloc(sizeof(struct Directive));
			node->node = dir;
			node->type = ast_directive;
			if (!memcmp(cur_token->string, "include", 7)) {
				dir->type = dir_include;
				eat_token(p);
				insert_token_string((char **)&dir->on, get_token(p));
				return node;
			}
		case '}':
			return node;

		case token_comment:
			return node;

		case ';':
			return node;
	}

	struct Token *temp = peek_parser(p, 1);
	int prec = get_precedence(temp);
	if (prec < precedence) {
		return node;
	}
	while (prec >= precedence) {//precedence <= prec) {
		switch (temp->type) {
			// returns
			case '{':
				return node;
			case ';':
				return node;

			case token_plus_a:
				eat_token(p);
				node = operation(op_plus_a, node, parse_expr(p, 0));
				return node;
			case token_minus_a:
				eat_token(p);
				node = operation(op_minus_a, node, parse_expr(p, 0));
				return node;
			case token_mult_a:
				eat_token(p);
				node = operation(op_mult_a, node, parse_expr(p, 0));
				return node;
			case token_div_a:
				eat_token(p);
				node = operation(op_div_a, node, parse_expr(p, 0));
				return node;
			case token_mod_a:
				eat_token(p);
				node = operation(op_mod_a, node, parse_expr(p, 0));
				return node;

				// other syntax
			case '(':
				if (node->type == ast_ident) {
					struct ProcCall *call = malloc(sizeof(struct ProcCall));
					call->name = node->node;
					node->type = ast_proc_call;
					node->node = call;
					eat_token(p);
					call->args = parse_args(p);
					eat_token(p);
					return node;
				}
				return node;
		
			// struct member access
			case '.':
				eat_token(p);
				node = operation(op_access, node, parse_expr(p, prec + 1));
				break;
			case '[':
					eat_token(p);
					struct AstNode *tempn = parse_expr(p, 0);
					if (tempn->type == ast_binary && ((struct BinaryOp*)tempn->node)->op == op_range) {
						node = operation(op_slice, node, tempn);
					} else {
						node = operation(op_subscript, node, tempn);
					}
					eat_token(p);
				break;

				// operations
			case '=':
				eat_token(p);
				node = operation(op_assign, node, parse_expr(p, 0));
				return node;

			case ':':
				eat_token(p);
				//struct AstNode *temp = parse_expr(p);
				//if (temp->type == ast_binary && ((struct BinaryOp*)temp->node)->op == op_assign) {
					//node = operation(op_decl, node, ((struct BinaryOp*)temp->node)->left);
					//((struct BinaryOp*)temp->node)->left = node;
					//node = temp;
				//} else {
					node = operation(op_decl, node, type(p));
					temp = peek_parser(p, 1);
					if (temp->type == '=') {
						eat_token(p);
						node = operation(op_assign, node, parse_expr(p, 0));
					}
					eat_token(p);
					if (get_token(p)->type != ';') {
						parerror(p, "expected semicolon");
					}
				//}
				return node;

			case token_def:
				eat_token(p);
				node = operation(op_def, node, parse_expr(p, 0));
				eat_token(p);
				return node;

			case token_const:
				eat_token(p);
				node = operation(op_const, node, type(p));
				if (get_token(p)->type != '}') {
					eat_token(p);
				}
				return node;

		}
		temp = peek_parser(p, 1);
    prec = get_precedence(temp);
		if (!temp) {
			return node;
		}
	}
	// TODO remove
 	//p->pos -= 1;
	//return parse_expr(p);
	return node;
}


struct AstRoot *parse(struct Tokenizer *tokenizer) {
	struct Parser *parser = malloc(sizeof(struct Parser));
	parser->t = tokenizer;
	struct AstRoot *root = malloc(sizeof(struct AstRoot));
	struct AstNode *temp_node;
	u64 root_max = 100;
	root->len = 0;
	root->nodes = malloc(root_max * sizeof(struct AstNode));

	u64 tokens_max = 500;
	struct Token *tokens = malloc(tokens_max * sizeof(struct Token));
	u64 token_len = 0;
	while (!get_next_token(parser->t)) {
		if (token_len == tokens_max) {
			tokens_max += 500;
			//struct Token *temp = malloc(sizeof(struct Token) * tokens_max);
			//memcpy(temp, tokens, sizeof(struct AstNode) );
			tokens = realloc(tokens, sizeof(struct Token) * tokens_max);
		}
		// print_token(parser->t->cur_token);
		tokens[token_len] = *parser->t->cur_token;
		token_len++;
	}
	parser->tokens = tokens;
	parser->token_len = token_len;
	parser->pos = 0;
	parser->first = 1;
	while (parser->pos < token_len - 1) {
		if (root->len == root_max) {
			root_max += 100;
			root->nodes = realloc(root->nodes, sizeof(struct AstNode) * root_max);
		}
		temp_node = parse_stmt(parser, 0);
		if (!temp_node)
			break;
		root->nodes[root->len] = *temp_node;
		// print_node(temp_node);
		root->len++;
	}
	//printf("num: %li\n", root->len);
	//for (int i; i < root->len; i++) {
	//print_node(&root->nodes[i]);
	//}
	// this here leaks the parser and parser->tokens which is fine
	return root;
}
