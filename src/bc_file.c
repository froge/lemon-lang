struct BytecodeData {
  u64 flen;
  struct Function *procs;
  struct ForeignLib flibs;
  struct Chunk rodata;
};

void serialize_chunk(void *file, struct Chunk *c) {
  u64 len = c->len;
  fwrite(&len, sizeof(u64), 1, file);
  fwrite(c->code, c->len, 1, file);
  fwrite(&c->dlen, sizeof(u64), 1, file);
  // if (c->dlen) {
    fwrite(c->debug, c->dlen * sizeof(struct Debug), 1, file);
  // }
}

void deserialize_chunk(void *file, struct Chunk *c) {
  fread(&c->len, sizeof(u64), 1, file);
  c->code = malloc(c->len);
  fread(c->code, c->len, 1, file);
  fread(&c->dlen, sizeof(u64), 1, file);
  // if (c->dlen) {
    c->debug = malloc(c->dlen * sizeof(struct Debug));
    fread(c->debug, c->dlen * sizeof(struct Debug), 1, file);
  // } else {
    // c->debug = 0;
  // }
}

void serialize_typeinfo(void *file, struct Typeinfo *type) {
  fwrite(&type->type, sizeof(i32), 1, file);
  fwrite(&type->pointer_level, sizeof(i32), 1, file);
  fwrite(&type->size, sizeof(i32), 1, file);
  u64 fard = strlen(type->name);
  fwrite(&fard, sizeof(i32), 1, file);
  fwrite(type->name, fard, 1, file);

  switch (type->type) {
  	case t_int:
      fwrite(&type->isigned, sizeof(i32), 1, file);
      break;
  	case t_float:
      break;
  	case t_struct:
      {
      struct StructInfo *si = type->struct_info;
      fwrite(&si->len, sizeof(i32), 1, file);
      for (int i = 0; i < si->len; i++) {
        serialize_typeinfo(file, si->types[i]);
      }
      break;
      }
  	case t_enum:
      // all that matters is the size of the type in foreign functions
      break;
  	case t_array:
      // not supported in foreign functions
      break;
  	case t_proc:
      {
      struct ProcInfo *pi = type->proc_info;
      fwrite(&pi->len, sizeof(i32), 1, file);
      fwrite(&pi->cproc, sizeof(i32), 1, file);
      for (int i = 0; i < pi->len; i++) {
        serialize_typeinfo(file, pi->types[i]);
      }
      if (pi->return_val) {
        serialize_typeinfo(file, pi->return_val);
      } else {
        u64 poopy = -1;
        fwrite(&poopy, sizeof(i32), 1, file);
      }
      break;
      }
  	case t_type:
      // not supported yet
      break;
  	case t_any:
      // not supported yet
      break;
  }
}

struct Typeinfo *deserialize_typeinfo(void *file) {
  struct Typeinfo *type = malloc(sizeof(struct Typeinfo));
  fread(&type->type, sizeof(i32), 1, file);
  if (type->type == -1) {
    free(type);
    return 0;
  }
  fread(&type->pointer_level, sizeof(i32), 1, file);
  fread(&type->size, sizeof(i32), 1, file);
  i32 fard;
  fread(&fard, sizeof(i32), 1, file);
  type->name = malloc(fard + 1);
  fread(type->name, fard, 1, file);
  type->name[fard] = 0;

  switch (type->type) {
  	case t_int:
      fread(&type->isigned, sizeof(i32), 1, file);
      break;
  	case t_float:
      break;
  	case t_struct:
      {
      type->struct_info = malloc(sizeof(struct StructInfo));
      struct StructInfo *si = type->struct_info;
      fread(&si->len, sizeof(i32), 1, file);
      si->types = malloc(sizeof(void*) * si->len);
      for (int i = 0; i < si->len; i++) {
        si->types[i] = deserialize_typeinfo(file);
      }
      break;
      }
  	case t_enum:
      // all that matters is the size of the type in foreign functions
      break;
  	case t_array:
      // not supported in foreign functions
      break;
  	case t_proc:
      {
      type->proc_info = malloc(sizeof(struct ProcInfo));
      struct ProcInfo *pi = type->proc_info;
      fread(&pi->len, sizeof(i32), 1, file);
      fread(&pi->cproc, sizeof(i32), 1, file);
      pi->types = malloc(sizeof(void*) * pi->len);
      for (int i = 0; i < pi->len; i++) {
        pi->types[i] = deserialize_typeinfo(file);
      }
      pi->return_val = deserialize_typeinfo(file);
      break;
      }
  	case t_type:
      // not supported yet
      break;
  	case t_any:
      // not supported yet
      break;
  }
  return type;
}

int generate_bc_file(struct BytecodeGen *bg, char *name) {
  void *file = fopen(name, "wb");
  fwrite("LBC ", 4, 1, file);
  struct BytecodeData data;
  data.flen = bg->flen;
  data.procs = bg->functions;
  data.flibs = *bg->flibs;
  data.rodata = bg->rodata;
  fwrite(&data.flen, sizeof(u64), 1, file);
  for (int i = 0; i < data.flen; i++) {
    u64 fard = strlen(data.procs[i].name);
    fwrite(&fard, sizeof(u64), 1, file);
    fwrite(data.procs[i].name, fard, 1, file);
    serialize_chunk(file, data.procs[i].labels);
    serialize_chunk(file, data.procs[i].allocas);
    serialize_chunk(file, &data.procs[i].c);
  }
  struct ForeignLib *flibs = &data.flibs;
  fwrite(&flibs->liblen, sizeof(u32), 1, file);
  for (int i = 0; i < flibs->liblen; i++) {
    u64 fard = strlen(flibs->libname[i]);
    fwrite(&fard, sizeof(u64), 1, file);
    fwrite(flibs->libname[i], fard, 1, file);
    fwrite(&flibs->cslen[i], sizeof(u64), 1, file);
    for (int y = 0; y < flibs->cslen[i]; y++) {
      serialize_typeinfo(file, flibs->cprocs[i][y]);
    }
  }
  serialize_chunk(file, &data.rodata);
  fclose(file);
}

struct BytecodeData deserialize_bc_file(char *name) {
  void *file = fopen(name, "rb");
  if (!file) {
    printf("file doesnt exist: %s\n", name);
    segexit();
  }
  char magic[4];
  fread(magic, 4, 1, file);
  if (strncmp(magic, "LBC ", 4)) {
    printf("file given is not a lemon bytecode file\n");
    segexit();
  }
  struct BytecodeData data;
  fread(&data.flen, sizeof(u64), 1, file);
  data.procs = malloc(sizeof(struct Function) * data.flen);
  for (int i = 0; i < data.flen; i++) {
    u64 fard;
    fread(&fard, sizeof(u64), 1, file);
    data.procs[i].name = malloc(fard + 1);
    fread(data.procs[i].name, fard, 1, file);
    data.procs[i].name[fard] = 0;
    data.procs[i].labels = malloc(sizeof(struct Chunk));
    data.procs[i].allocas = malloc(sizeof(struct Chunk));
    deserialize_chunk(file, data.procs[i].labels);
    deserialize_chunk(file, data.procs[i].allocas);
    deserialize_chunk(file, &data.procs[i].c);
  }
  struct ForeignLib *flibs = &data.flibs;
  fread(&flibs->liblen, sizeof(u32), 1, file);
  flibs->cprocs = malloc(sizeof(void*) * flibs->liblen);
  flibs->libname = malloc(sizeof(void*) * flibs->liblen);
  flibs->cslen = malloc(sizeof(u64) * flibs->liblen);
  for (int i = 0; i < flibs->liblen; i++) {
    u64 fard;
    fread(&fard, sizeof(u64), 1, file);
    flibs->libname[i] = malloc(fard + 1);
    fread(flibs->libname[i], fard, 1, file);
    flibs->libname[i][fard] = 0;
    fread(&flibs->cslen[i], sizeof(u64), 1, file);
    flibs->cprocs[i] = malloc(sizeof(void*) * flibs->cslen[i]);
    for (int y = 0; y < flibs->cslen[i]; y++) {
      flibs->cprocs[i][y] = deserialize_typeinfo(file);
    }
  }
  deserialize_chunk(file, &data.rodata);
  fclose(file);
  return data;
}
