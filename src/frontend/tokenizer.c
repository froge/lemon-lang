#include <stdint.h>
typedef uint64_t u64;
typedef uint32_t u32;
typedef int64_t i64;
typedef double f64;
#include <stdlib.h>
#include <stdio.h>
#include <float.h>

static inline int is_word(char ch) {
    return (ch > 64 && ch < 91) || ch == 95 || (ch > 96 && ch < 123);
}

static inline int is_number(char ch) {
    return (ch > 47 && ch < 58);
}

static inline int is_alphanumeric(char ch) {
    return is_word(ch) || is_number(ch);
}

enum TokenType {
  token_ident = 256,
  token_string_literal,
  token_char_literal,
  token_int_literal,
  token_float_literal,

  token_range,
	token_varargs,
  token_const,
  token_def,

  token_compare_equal,
  token_compare_nequal,
  token_compare_lt_eq,
  token_compare_gt_eq,

	token_plus_a,
	token_minus_a,
	token_mult_a,
	token_div_a,
	token_mod_a,

  token_logical_or,
  token_logical_and,

  token_lshift,
  token_rshift,

  token_kw_for,
  token_kw_loop,
  token_kw_while,
  token_kw_if,
  token_kw_in,
  token_kw_as,
  token_kw_break,
	token_kw_return,
  token_kw_struct,
  token_kw_enum,
  token_kw_module,
  token_kw_import,
  token_kw_include,
  token_kw_match,
  token_kw_else,
  token_kw_cproc,
	token_kw_foreign,

	token_true,
	token_false,

  token_directive,

	token_no_init,

	token_comment,
};


struct Token {
    enum TokenType type;
    u64 offset;
    u64 l0;
    u64 c0;
    u64 l1;
    u64 c1;
    u64 len;
    union {
        char *string;
        u64 integer;
        f64 fl;
    };
};

struct Tokenizer {
	char *filename;
    char *string;
    u64 len;
    u64 offset;
    u64 line;
    u64 ch;
    char cur_char;
    char last_char;
    struct Token *cur_token;
    struct Token *last_token;
};

struct Tokenizer *init_tokenizer() {
    struct Tokenizer *tok = malloc(sizeof(struct Tokenizer));
    tok->filename = 0;
    tok->string = 0;
    tok->len = 0;
    tok->offset = 0;
    tok->cur_char = 0;
    tok->last_char = 0;
    tok->ch = 1;
    tok->line = 1;
    tok->cur_token = malloc(sizeof(struct Token));
    tok->last_token = malloc(sizeof(struct Token));
    return tok;
}

struct Tokenizer *scan_file(char *filename) {
    struct Tokenizer *tok = init_tokenizer();
    tok->filename = filename;
    void *file = fopen(filename, "rb");
    u64 len = 0;
    fseek(file, 0, SEEK_END);
    len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char *string = malloc(len);
    fread(string, 1, len, file);
    tok->string = string;
    tok->len = len;
    return tok;
}

int get_keyword(char *string, int len) {
	switch (len) {
		case 2:
			if (!memcmp(string, "if", 2)) {
				return token_kw_if;
			} else if (!memcmp(string, "in", 2)) {
				return token_kw_in;
			} else if (!memcmp(string, "as", 2)) {
				return token_kw_as;
			}
		case 3:
			if (!memcmp(string, "for", 3)) {
				return token_kw_for;
			}
		case 4:
			if (!memcmp(string, "enum", 4)) {
				return token_kw_enum;
			} else if (!memcmp(string, "else", 4)) {
				return token_kw_else;
			} else if (!memcmp(string, "true", 4)) {
				return token_true;
			} else if (!memcmp(string, "loop", 4)) {
				return token_kw_loop;
			}
		case 5:
			if (!memcmp(string, "break", 5)) {
				return token_kw_break;
			} else if (!memcmp(string, "match", 5)) {
				return token_kw_match;
			} else if (!memcmp(string, "false", 5)) {
				return token_false;
			} else if (!memcmp(string, "cproc", 5)) {
				return token_kw_cproc;
			} else if (!memcmp(string, "while", 5)) {
				return token_kw_while;
			}
		case 6:
			if (!memcmp(string, "return", 6)) {
				return token_kw_return;
			} else if (!memcmp(string, "struct", 6)) {
				return token_kw_struct;
			} else if (!memcmp(string, "module", 6)) {
				return token_kw_module;
			} else if (!memcmp(string, "import", 6)) {
				return token_kw_import;
			}
		case 7:
			if (!memcmp(string, "include", 7)) {
				return token_kw_include;
			} else if (!memcmp(string, "foreign", 7)) {
				return token_kw_foreign;
			}

	}
    return 0;
}

int get_next_char(struct Tokenizer *tokenizer) {
    if (tokenizer->offset == tokenizer->len) {
		return 1;
    }
    tokenizer->last_char = tokenizer->cur_char;
    tokenizer->cur_char = tokenizer->string[tokenizer->offset];
    tokenizer->offset++;
    return 0;
}

u64 power(u64 one, u64 two) {
    u64 ret = 0;
    for (u64 i = 0; i < two; i++)
            ret *= one;
    return ret;
}

void set_cur_token(struct Tokenizer *tokenizer, enum TokenType type) {
    struct Token *t = tokenizer->cur_token;
    t->type = type;
    t->len = tokenizer->offset - t->offset;
    t->l1 = tokenizer->line;
    t->c1 = tokenizer->ch;
	t->string = tokenizer->string + t->offset;
    char *content = tokenizer->string + t->offset;
    switch (type) {
        case token_ident:
			{
				int type = get_keyword(tokenizer->string + t->offset, t->len);
				if (type) {
					t->type = type;
					break;
				}
			}
        case token_char_literal:
            break;
        case token_string_literal:
            break;
        case token_int_literal:
            t->integer = strtoull(content, NULL, 10);
            break;
        case token_float_literal:
            t->fl = atof(content);
    }
}

char peek(struct Tokenizer *tokenizer) {
    return tokenizer->string[tokenizer->offset];
}

char peek_by(int amount, struct Tokenizer *tokenizer) {
    return tokenizer->string[tokenizer->offset + amount - 1];
}

int get_string(struct Tokenizer *tokenizer) {
    for (;;) {
        if (peek(tokenizer) == '"' && tokenizer->cur_char != '\\') {
            set_cur_token(tokenizer, token_string_literal);
            tokenizer->offset += 1;
            return 0;
        }
        if (get_next_char(tokenizer)) {
            return 1;
        }
        tokenizer->ch++;
    }
}

int get_char(struct Tokenizer *tokenizer) {
    for (;;) {
        if (peek(tokenizer) == '\'' && tokenizer->cur_char != '\\') {
            set_cur_token(tokenizer, token_char_literal);
            tokenizer->offset += 1;
            return 0;
        }
        if (get_next_char(tokenizer)) {
            return 1;
        }
        tokenizer->ch++;
    }
}

int get_next_token(struct Tokenizer *tokenizer) {
    struct Token *tokentempptr = tokenizer->last_token;
    tokenizer->last_token = tokenizer->cur_token;
    tokenizer->cur_token = tokentempptr;
    struct Token *t = tokenizer->cur_token;
    t->type = -1;
    t->l0 = tokenizer->line;
    t->c0 = tokenizer->ch;
    t->offset = tokenizer->offset;
    for (;;) {
        if (get_next_char(tokenizer)) {
            return 1;
        }
        tokenizer->ch++;
        if (t->type == token_ident) {
            if (!is_alphanumeric(peek(tokenizer))) {
                set_cur_token(tokenizer, token_ident);
                return 0;
            }
        }
        if (t->type == token_float_literal) {
            if (!is_number(peek(tokenizer))) {
                tokenizer->ch++;
                set_cur_token(tokenizer, token_float_literal);
                return 0;
            }
        }
        if (t->type == token_int_literal) {
           if (!is_number(peek(tokenizer))) {
                if (peek(tokenizer) == '.' && is_number(peek_by(2, tokenizer))) {
                    t->type = token_float_literal;
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    tokenizer->ch++;
                    continue;
                }
                set_cur_token(tokenizer, token_int_literal);
                return 0;
            }
        }
        if (t->type == -1) {
            if (is_number(tokenizer->cur_char)) {
                t->type = token_int_literal;
                if (!is_number(peek(tokenizer))) {
                    if (peek(tokenizer) == '.' && is_number(peek_by(2, tokenizer))) {
                        t->type = token_float_literal;
						tokenizer->offset++;
                        tokenizer->ch++;
                        continue;
                    }
                    set_cur_token(tokenizer, token_int_literal);
                    return 0;
                }
                continue;
            }
        }
        if (t->type != token_ident) {
        switch (tokenizer->cur_char) {
            case '"':
                t->offset = tokenizer->offset;
                return get_string(tokenizer);
                break;
            case '\'':
                t->offset = tokenizer->offset;
				return get_char(tokenizer);
            case '(':
                set_cur_token(tokenizer, '(');
                return 0;
            case ')':
                set_cur_token(tokenizer, ')');
                return 0;
            case '[':
                set_cur_token(tokenizer, '[');
                return 0;
            case ']':
                set_cur_token(tokenizer, ']');
                return 0;
            case '{':
                set_cur_token(tokenizer, '{');
                return 0;
            case '}':
                set_cur_token(tokenizer, '}');
                return 0;
            case ';':
                set_cur_token(tokenizer, ';');
                return 0;
            case '#':
                set_cur_token(tokenizer, '#');
                return 0;
            case ',':
                set_cur_token(tokenizer, ',');
                return 0;
            case '+':
				if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					set_cur_token(tokenizer, token_plus_a);
					return 0;
				}
                set_cur_token(tokenizer, '+');
                return 0;
            case '-':
				if (peek(tokenizer) == '-') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					if (peek(tokenizer) == '-') {
						if (get_next_char(tokenizer)) {
							return 1;
						}
						set_cur_token(tokenizer, token_no_init);
						return 0;
					}
					return 1;
				}
				if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					set_cur_token(tokenizer, token_minus_a);
					return 0;
				}
                set_cur_token(tokenizer, '-');
                return 0;
            case '*':
				if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					set_cur_token(tokenizer, token_mult_a);
					return 0;
				}
                set_cur_token(tokenizer, '*');
                return 0;
            case '/':
				if (peek(tokenizer) == '*') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					t->type = token_comment;
					int depth = 1;
					while (depth > 0) {
						if (get_next_char(tokenizer)) {
							/*
							printf("reached end unexpectedly\n");
							int *end_program = 0; // ;)
							int bye = *end_program + 1;
							*/
							return 1;
						}
						tokenizer->ch++;
						if (tokenizer->cur_char == '\n') {
							tokenizer->line++;
							tokenizer->ch = 1;
						} else if (tokenizer->cur_char == '/' && peek(tokenizer) == '*') {
							if (get_next_char(tokenizer)) {
								return 1;
							}
							tokenizer->ch++;
							depth += 1;
						} else if (tokenizer->cur_char == '*' && peek(tokenizer) == '/') {
							if (get_next_char(tokenizer)) {
								return 1;
							}
							tokenizer->ch++;
							depth -= 1;
						}
					}
					set_cur_token(tokenizer, token_comment);
					return 0;
				}
                if (peek(tokenizer) == '/') {
					t->type = token_comment;
					while (tokenizer->cur_char != '\n') {
						if (get_next_char(tokenizer)) {
							return 1;
						}
						tokenizer->ch++;
					}
					tokenizer->line++;
					tokenizer->ch = 1;
                    set_cur_token(tokenizer, token_comment);
					return 0;
				}
				if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					set_cur_token(tokenizer, token_div_a);
					return 0;
				}
                set_cur_token(tokenizer, '/');
                return 0;
            case '%':
				if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					set_cur_token(tokenizer, token_mod_a);
					return 0;
				}
                set_cur_token(tokenizer, '%');
                return 0;
            case '^':
                set_cur_token(tokenizer, '^');
				if (peek(tokenizer) == '=') {
					set_cur_token(tokenizer, token_div_a);
				}
                return 0;
            case '<':
                if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_compare_lt_eq);
                    return 0;
                } else if (peek(tokenizer) == '<') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_lshift);
                    return 0;
                }
                set_cur_token(tokenizer, '<');
                return 0;
            case '>':
                if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_compare_gt_eq);
                    return 0;
                } else if (peek(tokenizer) == '>') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_rshift);
                    return 0;
                }
                set_cur_token(tokenizer, '>');
                return 0;
            case '&':
                if (peek(tokenizer) == '&') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_logical_and);
                    return 0;
                }
                set_cur_token(tokenizer, '&');
                return 0;
            case '|':
                if (peek(tokenizer) == '|') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_logical_or);
                    return 0;
                }
                set_cur_token(tokenizer, '|');
                return 0;
            case ':':
                if (peek(tokenizer) == ':') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_const);
                    return 0;
                } else if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_def);
                    return 0;
                }
                set_cur_token(tokenizer, ':');
                return 0;
            case '.':
                if (peek(tokenizer) == '.') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
					if (peek(tokenizer) == '.') {
						if (get_next_char(tokenizer)) {
							return 1;
						}
						set_cur_token(tokenizer, token_varargs);
						return 0;
					} else {
						set_cur_token(tokenizer, token_range);
						return 0;
					}
                }
                set_cur_token(tokenizer, '.');
                return 0;
            case '=':
                if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_compare_equal);
                    return 0;
                }
                set_cur_token(tokenizer, '=');
                return 0;
            case '!':
                if (peek(tokenizer) == '=') {
					if (get_next_char(tokenizer)) {
						return 1;
					}
                    set_cur_token(tokenizer, token_compare_nequal);
                    return 0;
                }
                set_cur_token(tokenizer, '!');
                return 0;
			case '\t':
            case ' ':
				t->l0 = tokenizer->line;
				t->c0 = tokenizer->ch;
                t->offset = tokenizer->offset;
                break;
            case '\n':
                tokenizer->line++;
                tokenizer->ch = 1;
				t->l0 = tokenizer->line;
				t->c0 = tokenizer->ch;
                t->offset = tokenizer->offset;
                //set_cur_token(tokenizer, '\n');
                //return 0;
		break;
            }
        }
        if (t->type == -1) {
            if (is_word(tokenizer->cur_char)) {
                t->type = token_ident;
                if (!is_alphanumeric(peek(tokenizer))) {
                    set_cur_token(tokenizer, token_ident);
                    return 0;
                }
            }
        }
    }
    return 0;
}

void print_token(struct Token *token) {
    printf("Token {\n\ttype: ");
    if (token->type < 256) {
        printf("%d", token->type);
    } else {
        switch (token->type) {
				  case token_ident:
					  printf("token_ident");
						break;
				  case token_string_literal:
					  printf("token_string_literal");
						break;
				  case token_char_literal:
					  printf("token_char_literal");
						break;
				  case token_int_literal:
					  printf("token_int_literal");
						break;
				  case token_float_literal:
					  printf("token_float_literal");
						break;

				  case token_range:
					  printf("token_range");
						break;
					case token_varargs:
						printf("token_varargs");
						break;
				  case token_const:
					  printf("token_const");
					break;
				  case token_def:
					  printf("token_def");
						break;

				  case token_compare_equal:
					  printf("token_compare_equal");
						break;
				  case token_compare_nequal:
					  printf("token_compare_nequal");
					break;
				  case token_compare_lt_eq:
					  printf("token_compare_lt_eq");
						break;
				  case token_compare_gt_eq:
					  printf("token_compare_gt_eq");
						break;

					case token_plus_a:
						printf("token_plus_a");
						break;
					case token_minus_a:
						printf("token_minus_a");
						break;
					case token_mult_a:
						printf("token_mult_a");
						break;
					case token_div_a:
						printf("token_div_a");
						break;
					case token_mod_a:
						printf("token_mod_a");
						break;

				  case token_logical_or:
					  printf("token_logical_or");
						break;
				  case token_logical_and:
					  printf("token_logical_and");
						break;

				  case token_lshift:
					  printf("token_lshift");
						break;
				  case token_rshift:
					  printf("token_rshift");
						break;

				  case token_kw_for:
					  printf("token_kw_for");
						break;
				  case token_kw_loop:
					  printf("token_kw_loop");
						break;
				  case token_kw_while:
					  printf("token_kw_while");
						break;
				  case token_kw_if:
					  printf("token_kw_if");
						break;
				  case token_kw_in:
					  printf("token_kw_in");
						break;
				  case token_kw_as:
					  printf("token_kw_as");
						break;
				  case token_kw_break:
					  printf("token_kw_break");
						break;
					case token_kw_return:
						printf("token_kw_return");
						break;
				  case token_kw_struct:
					  printf("token_kw_struct");
						break;
				  case token_kw_enum:
					  printf("token_kw_enum");
						break;
				  case token_kw_module:
					  printf("token_kw_module");
						break;
				  case token_kw_import:
					  printf("token_kw_import");
						break;
				  case token_kw_include:
					  printf("token_kw_include");
						break;
				  case token_kw_match:
					  printf("token_kw_match");
						break;
				  case token_kw_else:
					  printf("token_kw_else");
						break;
				  case token_kw_cproc:
					  printf("token_kw_cproc");
						break;
					case token_kw_foreign:
						printf("token_kw_foreign");
						break;

					case token_true:
						printf("token_true");
						break;
					case token_false:
						printf("token_false");
						break;

				  case token_directive:
					  printf("token_directive");
						break;

					case token_no_init:
						printf("token_no_init");
							break;

					case token_comment:
						printf("token_comment");
							break;
				}
    }
    printf("\n\toffset: %lu\n\tlen: %lu\n\tl0: %lu\n\tc0: %lu\n\tl1: %lu\n\tc1: %lu\n\tcontent: ", token->offset, token->len, token->l0, token->c0, token->l1, token->c1);
    switch (token->type) {
        case token_string_literal:
        case token_char_literal:
        case token_ident:
            printf("\"");
            for (int i = 0; i < token->len; i++) {
                printf("%c", token->string[i]);
            }
            printf("\"");
            break;
        case token_int_literal:
            printf("%lld", token->integer);
            break;
        case token_float_literal:
            printf("%f", token->fl);
    }
    if (token->type < 256) {
        printf("'%c'", token->type);
    }
    printf("\n}\n");
}
