struct VarList {
	u64 max;
	u64 len;
	u64 temp;
	// u64 pos; // next free space on the stack relative to the stack pointer
	char **vars;
	u64 *lens;
	u64 cur_label;
};

void eval_expr(struct Chunk *c, struct AstNode *node, struct VarList *vl, int var_num);
u64 gen_proc(struct BytecodeGen *bg, struct Typeinfo *info);
u64 init_to_zero(struct Chunk *c, u16 reg, struct Typeinfo *type, struct VarList *vl);

void init_chunk(struct Chunk *c, int debug) {
	c->max = 2000;
	c->len = 0;
	c->code = malloc(c->max);
	c->dmax = 200;
	c->dlen = 0;
	if (debug) {
		c->debug = malloc(sizeof(struct Debug) * c->dmax);
	} else {
		c->debug = 0;
	}

}

u64 tempr(struct VarList *vl) {
	u64 t = vl->len;
	vl->len++;
	return t;
}

u64 templ(struct VarList *vl) {
	u64 t = vl->len;
	vl->cur_label++;
	return t;
}

void dtemp(struct VarList *vl) {
	// vl->len--;
}

void resize_chunk(struct Chunk *c, u64 len) {
	if (c->max - c->len < len) {
		c->max += 2000;
		c->code = realloc(c->code, c->max);
		//TODO replace with malloc + memcpy
	}
}

u64 find_func(struct BytecodeGen *bg, char *name) {
	if (!strcmp(name, "___bytecode_call")) {
		return f_bc_call;
	}
	for (int i = 0; i < bg->flen; i++) {
		if (!strcmp(bg->functions[i].name, name)) {
			return i + f_last;
		}
	}
	return 0;
}

void add_var(struct VarList *vl, u16 reg, char *name, struct Typeinfo *info) {
	if (vl->temp == vl->max - 1) {
		vl->max += 20;
		vl->vars = realloc(vl->vars, vl->max * sizeof(char*));
		vl->lens = realloc(vl->lens, vl->max * sizeof(u64));
	}
	vl->vars[vl->temp] = name;
	vl->lens[vl->temp] = reg;
	vl->temp++;
}

u64 find_var(struct VarList *vl, char *name) {
	for (int i = 0; i < vl->temp; i++) {
		if (!strcmp(vl->vars[i], name)) {
			return vl->lens[i];
		}
	}
	return 0;
}

void arithi(struct Chunk *c, int itype, u16 reg1, u16 reg2, u16 reg3) {
	resize_chunk(c, sizeof(struct ArithmeticI));
	struct ArithmeticI inst;
	inst.instr_type = itype;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.reg3 = reg3;
	memcpy(c->code + c->len, &inst, sizeof(struct ArithmeticI));
	c->len += sizeof(struct ArithmeticI);
}

void add(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_add, reg1, reg2, reg3);
}

void sub(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_sub, reg1, reg2, reg3);
}

void mul(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_mul, reg1, reg2, reg3);
}

void _div(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_div, reg1, reg2, reg3);
}

void mod(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_mod, reg1, reg2, reg3);
}

void mulu(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_mulu, reg1, reg2, reg3);
}

void _divu(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_divu, reg1, reg2, reg3);
}

void modu(struct Chunk *c, u16 reg1, u16 reg2, u16 reg3) {
	arithi(c, i_modu, reg1, reg2, reg3);
}

void addf(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_addfs, reg1, reg2, reg3);
	} else {
		arithi(c, i_addfd, reg1, reg2, reg3);
	}
}

void subf(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_subfs, reg1, reg2, reg3);
	} else {
		arithi(c, i_subfd, reg1, reg2, reg3);
	}
}

void mulf(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_mulfs, reg1, reg2, reg3);
	} else {
		arithi(c, i_mulfd, reg1, reg2, reg3);
	}
}

void _divf(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_divfs, reg1, reg2, reg3);
	} else {
		arithi(c, i_divfd, reg1, reg2, reg3);
	}
}

void _modf(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_modfs, reg1, reg2, reg3);
	} else {
		arithi(c, i_modfd, reg1, reg2, reg3);
	}
}

void flt(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_flts, reg1, reg2, reg3);
	} else {
		arithi(c, i_fltd, reg1, reg2, reg3);
	}
}

void fge(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_fges, reg1, reg2, reg3);
	} else {
		arithi(c, i_fged, reg1, reg2, reg3);
	}
}

void feq(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	if (len == 4) {
		arithi(c, i_feqs, reg1, reg2, reg3);
	} else {
		arithi(c, i_feqd, reg1, reg2, reg3);
	}
}

void fgt(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	flt(c, len, reg1, reg3, reg2);
}

void fle(struct Chunk *c, u8 len, u16 reg1, u16 reg2, u16 reg3) {
	fgt(c, len, reg1, reg3, reg2);
}

void addi(struct Chunk *c, u16 reg1, u16 reg2, u64 val) {
	resize_chunk(c, sizeof(struct AddImmediateI));
	struct AddImmediateI inst;
	inst.instr_type = i_addi;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.val = val;
	memcpy(c->code + c->len, &inst, sizeof(struct AddImmediateI));
	c->len += sizeof(struct AddImmediateI);
}

void ftoi(struct Chunk *c, u8 fmt1, u8 fmt2, u16 reg1, u64 reg2) {
	resize_chunk(c, sizeof(struct ConvI));
	struct ConvI inst;
	inst.instr_type = i_ftoi;
	inst.fmt1 = fmt1;
	inst.fmt2 = fmt2;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	memcpy(c->code + c->len, &inst, sizeof(struct ConvI));
	c->len += sizeof(struct ConvI);
}

void itof(struct Chunk *c, u8 fmt1, u8 fmt2, u16 reg1, u64 reg2) {
	resize_chunk(c, sizeof(struct ConvI));
	struct ConvI inst;
	inst.instr_type = i_itof;
	inst.fmt1 = fmt1;
	inst.fmt2 = fmt2;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	memcpy(c->code + c->len, &inst, sizeof(struct ConvI));
	c->len += sizeof(struct ConvI);
}

void call(struct Chunk *c, u64 pid) {
	resize_chunk(c, sizeof(struct CallI));
	struct CallI inst;
	inst.instr_type = i_call;
	inst.func = pid;
	memcpy(c->code + c->len, &inst, sizeof(struct CallI));
	c->len += sizeof(struct CallI);
}

void ret(struct Chunk *c) {
	resize_chunk(c, sizeof(struct RetI));
	struct RetI inst;
	inst.instr_type = i_ret;
	memcpy(c->code + c->len, &inst, sizeof(struct RetI));
	c->len += sizeof(struct RetI);
}

void add_rodata(struct BytecodeGen *bg, void *data, u64 len) {
	struct Chunk *c = &bg->rodata;
	resize_chunk(c, len);
	memcpy(c->code + c->len, data, len);
	c->len += len;
}

void load_rodata(struct Chunk *c, u16 reg, u64 idx) {
	resize_chunk(c, sizeof(struct LoadRDI));
	struct LoadRDI inst;
	inst.instr_type = i_loadrd;
	inst.reg = reg;
	inst.addr = idx;
	memcpy(c->code + c->len, &inst, sizeof(struct LoadRDI));
	c->len += sizeof(struct LoadRDI);
}

void allocas(struct Chunk *cs, u16 reg, u8 len) {
	struct Chunk *c = cs->proc->allocas;
	resize_chunk(c, sizeof(struct AllocaSI));
	struct AllocaSI inst;
	inst.instr_type = i_allocas;
	inst.reg = reg;
	inst.len = len;
	memcpy(c->code + c->len, &inst, sizeof(struct AllocaSI));
	c->len += sizeof(struct AllocaSI);
}

// void allocass(struct Chunk *cs, u16 reg, u8 len) {
// 	// struct Chunk *c = cs->allocas;
// 	struct Chunk *c = cs;
// 	resize_chunk(c, sizeof(struct AllocaSI));
// 	struct AllocaSI inst;
// 	inst.instr_type = i_allocas;
// 	inst.reg = reg;
// 	inst.len = len;
// 	memcpy(c->code + c->len, &inst, sizeof(struct AllocaSI));
// 	c->len += sizeof(struct AllocaSI);
// }

void add_arg(struct Chunk *c, u16 reg) {
	resize_chunk(c, sizeof(struct ArgI));
	struct ArgI inst;
	inst.instr_type = i_aarg;
	inst.reg = reg;
	memcpy(c->code + c->len, &inst, sizeof(struct ArgI));
	c->len += sizeof(struct ArgI);
}

void load_arg(struct Chunk *c, u16 reg) {
	resize_chunk(c, sizeof(struct ArgI));
	struct ArgI inst;
	inst.instr_type = i_larg;
	inst.reg = reg;
	memcpy(c->code + c->len, &inst, sizeof(struct ArgI));
	c->len += sizeof(struct ArgI);
}

void load(struct Chunk *c, u8 len, u8 sign, u16 reg, u16 addr, i64 offset) {
	resize_chunk(c, sizeof(struct MemI));
	struct MemI inst;
	if (sign) {
		switch (len) {
			case 8:
				inst.instr_type = i_loadd;
				if (offset % 8 != 0) {
					offset = offset - (offset % 8) + 8;
				}
				break;
			case 4:
				inst.instr_type = i_loadw;
				if (offset % 4 != 0) {
					offset = offset - (offset % 4) + 4;
				}
				break;
			case 2:
				inst.instr_type = i_loadh;
				if (offset % 2 != 0) {
					offset = offset - (offset % 2) + 2;
				}
				break;
			case 1:
				inst.instr_type = i_loadb;
				break;
			default:
				printf("ERROR: improper alignment given (compiler error)\n");
				segexit();
		}
	} else {
		switch (len) {
			case 8:
				inst.instr_type = i_loaddu;
				if (offset % 8 != 0) {
					offset = offset - (offset % 8) + 8;
				}
				break;
			case 4:
				inst.instr_type = i_loadwu;
				if (offset % 4 != 0) {
					offset = offset - (offset % 4) + 4;
				}
				break;
			case 2:
				inst.instr_type = i_loadhu;
				if (offset % 2 != 0) {
					offset = offset - (offset % 2) + 2;
				}
				break;
			case 1:
				inst.instr_type = i_loadbu;
				break;
			default:
				printf("ERROR: improper alignment given (compiler error)\n");
				segexit();
		}
	}
	inst.reg = reg;
	inst.addr = addr;
	inst.offset = offset;
	memcpy(c->code + c->len, &inst, sizeof(struct MemI));
	c->len += sizeof(struct MemI);
}

void store(struct Chunk *c, u8 len, u16 reg, u16 addr, i64 offset) {
	resize_chunk(c, sizeof(struct MemI));
	struct MemI inst;
	switch (len) {
		case 8:
			inst.instr_type = i_stored;
			if (offset % 8 != 0) {
				offset = offset - (offset % 8) + 8;
			}
			break;
		case 4:
			inst.instr_type = i_storew;
			if (offset % 4 != 0) {
				offset = offset - (offset % 4) + 4;
			}
			break;
		case 2:
			inst.instr_type = i_storeh;
			if (offset % 2 != 0) {
				offset = offset - (offset % 2) + 2;
			}
			break;
		case 1:
			inst.instr_type = i_storeb;
			break;
		default:
			printf("ERROR: improper alignment given (compiler error)\n");
			segexit();
	}
	inst.reg = reg;
	inst.addr = addr;
	inst.offset = offset;
	memcpy(c->code + c->len, &inst, sizeof(struct MemI));
	c->len += sizeof(struct MemI);
}

void simplei(struct Chunk *c, u8 type, u16 reg1, u16 reg2) {
	resize_chunk(c, sizeof(struct SimpleI));
	struct SimpleI inst;
	inst.instr_type = type;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	memcpy(c->code + c->len, &inst, sizeof(struct SimpleI));
	c->len += sizeof(struct SimpleI);
}

void noti(struct Chunk *c, u16 reg1, u16 reg2) {
	simplei(c, i_not, reg1, reg2);
}

/*
void takep(struct Chunk *c, u16 reg1, u16 reg2) {
	resize_chunk(c, sizeof(struct TakePointerI));
	struct TakePointerI inst;
	inst.instr_type = i_takep;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	memcpy(c->code + c->len, &inst, sizeof(struct TakePointerI));
	c->len += sizeof(struct TakePointerI);
}
*/

void jump(struct Chunk *c, i64 addr) {
	resize_chunk(c, sizeof(struct JmpI));
	struct JmpI inst;
	inst.instr_type = i_jmp;
	inst.reg = r_zero;
	inst.addr = addr;
	memcpy(c->code + c->len, &inst, sizeof(struct JmpI));
	c->len += sizeof(struct JmpI);
}

void jumpr(struct Chunk *c, i64 addr) {
	resize_chunk(c, sizeof(struct JmpRI));
	struct JmpRI inst;
	inst.instr_type = i_jmpr;
	inst.addr = addr;
	memcpy(c->code + c->len, &inst, sizeof(struct JmpRI));
	c->len += sizeof(struct JmpRI);
}

void bne(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_bne;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void beq(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_beq;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void blt(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_blt;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void bltu(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_bltu;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void bgt(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	blt(c, reg2, reg1, pos);
}

void bgtu(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	bltu(c, reg2, reg1, pos);
}

void bge(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_bge;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void bgeu(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	resize_chunk(c, sizeof(struct BranchI));
	struct BranchI inst;
	inst.instr_type = i_bgeu;
	inst.reg1 = reg1;
	inst.reg2 = reg2;
	inst.addr = pos;
	memcpy(c->code + c->len, &inst, sizeof(struct BranchI));
	c->len += sizeof(struct BranchI);
}

void ble(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	bge(c, reg2, reg1, pos);
}

void bleu(struct Chunk *c, u16 reg1, u16 reg2, i64 pos) {
	bgeu(c, reg2, reg1, pos);
}

u16 push(struct Chunk *c, struct VarList *vl, u16 reg, u8 len) {
	u16 t1 = tempr(vl);
	allocas(c, t1, len);
	store(c, len, reg, t1, 0);
	return t1;
	// vl->pos; += len + fix_alignment(len, vl->pos);
	// return vl->pos - len;
}

// void pop(struct Chunk *c, struct VarList *vl, u16 reg, u8 len, u8 sign) {
// 	load(c, len, sign, reg, r_fp, vl->pos);
// 	vl->pos -= len;
// }

void set(struct Chunk *c, u16 reg, u64 val) {
	addi(c, reg, r_zero, val);
}

void compare(struct Chunk *c, struct BinaryOp *bin, struct VarList *vl, u16 var_num) {
	u16 t1 = tempr(vl);
	u16 t2 = tempr(vl);
	eval_expr(c, bin->left, vl, t1);
	eval_expr(c, bin->right, vl, t2);
	u8 signe = bin->right->info->isigned;
	u64 brpos = c->len;
	switch (bin->op) {
		case op_eq:
			beq(c, t1, t2, 0);
			break;
		case op_neq:
			bne(c, t1, t2, 0);
			break;
		case op_lt:
			if (signe) {
				blt(c, t1, t2, 0);
			} else {
				bltu(c, t1, t2, 0);
			}
			break;
		case op_lt_eq:
			if (signe) {
				ble(c, t1, t2, 0);
			} else {
				bleu(c, t1, t2, 0);
			}
			break;
		case op_gt:
			if (signe) {
				bgt(c, t1, t2, 0);
			} else {
				bgtu(c, t1, t2, 0);
			}
			break;
		case op_gt_eq:
			if (signe) {
				bge(c, t1, t2, 0);
			} else {
				bgeu(c, t1, t2, 0);
			}
			break;
	}
	set(c, var_num, 0);
	u64 jmppos = c->len;
	jump(c, 0);
	struct BranchI *br = (struct BranchI*)((u64)c->code + brpos);
	br->addr = c->len;
	set(c, var_num, 1);
	struct JmpI *jmp = (struct JmpI*)((u64)c->code + jmppos);
	jmp->addr = c->len;
	dtemp(vl);
	dtemp(vl);
}

void get_pointer(struct Chunk *c, struct AstNode *node, struct VarList *vl, int var_num) {
	u64 temp;
	switch (node->type) {
		case ast_block:
			return;
		case ast_proc_call:
			{
			struct ProcCall *pc = node->node;
			struct Typeinfo *pi = find_type(c->bg->t, pc->name);
			struct BytecodeGen *bg = c->bg;
			u64 pid;
			if (!pi->proc_info->cproc) {
				pid = find_func(c->bg, pc->name);
				if (!pid) {
					pid = gen_proc(c->bg, pi) + f_last;
				}
			}
			u16 fp = tempr(vl);
			u16 sp = tempr(vl);
			addi(c, fp, r_fp, 0); // store current frame pointer
			addi(c, sp, r_sp, 0); // store current stack pointer
										   //

			u64 *temp, max = 0, len = 0;
			if (pid != f_bc_call) {// && pid != f_call_native) {
			for (int i = 0; i < pc->args->len; i++) {
				struct Typeinfo *cur = pc->args->nodes[i].info;
				if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
					if (!max) {
						max = 20;
						temp = malloc(20 * sizeof(u64));
					}
					u16 t1 = tempr(vl);
					get_pointer(c, &pc->args->nodes[i], vl, t1);
					temp[len] = push(c, vl, t1, 8);
					len++;
					set(c, t1, pc->args->nodes[i].info->array_info->capacity);
					push(c, vl, t1, 8);
					dtemp(vl);
				} else if (cur->type == t_string_literal) {
					if (!max) {
						max = 20;
						temp = malloc(20 * sizeof(u64));
					}
					u16 t1 = tempr(vl);
					eval_expr(c, &pc->args->nodes[i], vl, t1);
					temp[len] = push(c, vl, t1, 8);
					len++;
					dtemp(vl);
				}
			}
			}
			u16 pos = tempr(vl);
			addi(c, pos, r_sp, 0);
			// u64 pos = vl->pos + fix_alignment(8, vl->pos);
			// vl->pos = pos;
			//if (pc->args->len > 0) {
				//pos += fix_alignment(alignment_of(pc->args->nodes[0].info), vl->pos);
			//}
			u64 t = 0;
			u16 t1 = tempr(vl);
			if (pi->proc_info->cproc) {
				struct ForeignLib *flibs = bg->t->flibs;
				int found = 0;
				u64 fidx = pi->proc_info->foreign_idx;
				u64 csidx = flibs->cslen[fidx] - 1;
				for (int i = 0; i < flibs->cslen[fidx]; i++) {
						if (flibs->cprocs[fidx][i] == pi) {
							found = 1;
							csidx = i;
							break;
						}
				}
				if (!found) {
					if (flibs->cslen[fidx] == flibs->csmax[fidx] - 1) {
						flibs->csmax[fidx] += 20;
						flibs->cprocs[fidx] = realloc(flibs->cprocs[fidx], sizeof(void*) * flibs->csmax[fidx]);
					}
					flibs->cprocs[fidx][flibs->cslen[fidx]] = pi;
					flibs->cslen[fidx] += 1;
					csidx = flibs->cslen[fidx] - 1;
				}
				pid = f_call_native;
				set(c, t1, pi->proc_info->foreign_idx);
				add_arg(c, t1); // push the first argument (c lib index)
				set(c, t1, csidx);
				add_arg(c, t1); // push the first argument (c native function to call) 
			}
			for (int i = 0; i < pc->args->len; i++) {
				struct Typeinfo *cur = pc->args->nodes[i].info;
				if (pid == f_bc_call) {// || pid == f_call_native) {
					eval_expr(c, &pc->args->nodes[i], vl, t1);
				} else {
					if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
						addi(c, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_string_literal) {
						load(c, 8, 0, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_struct && !cur->pointer_level) {
						get_pointer(c, &pc->args->nodes[i], vl, t1);
					} else {
						eval_expr(c, &pc->args->nodes[i], vl, t1);
					}
				}
				add_arg(c, t1);
			}
			dtemp(vl);
			add(c, r_fp, r_fp, pos); // offset current frame pointer by stack pos
			set(c, r_sp, 0); // set stack pointer to zero so the arguments work
			call(c, pid); // call function :flushed: (who would have guessed!!)
			load_arg(c, var_num); // load return value
			addi(c, r_fp, fp, 0); // restore frame pointer
			addi(c, r_sp, sp, 0); // restore stack pointer
			dtemp(vl);
			return;
			}
		case ast_proc_hdr:
		case ast_cproc_hdr:
		case ast_proc:
			return;
		case ast_ident:
			temp = find_var(vl, node->node); // TODO do the thing with the type sizes
			if (!node->info->pointer_level) {
				addi(c, var_num, temp, 0);
			} else {
				if (node->info->type == t_int) {
					load(c, size_of(node->info), node->info->isigned, var_num, temp, 0);
				} else {
					load(c, size_of(node->info), 0, var_num, temp, 0);
				}
			}
			return;
		case ast_literal:
			{
				struct Literal *lit = node->node;
				switch (lit->type) {
					case -1:
						set(c, var_num, 0); //TODO adjust per type
						break;
					case 0:
						set(c, var_num, lit->integer);
						break;
					case 1:
						set(c, var_num, lit->fl);
						break;
					case 2:
						load_rodata(c, var_num, c->bg->rodata.len);
						add_rodata(c->bg, lit->string, strlen(lit->string));
						break;
					case 3:
						set(c, var_num, lit->integer);
						break;
					case 4:
						{
							struct EnumInfo *ei = node->info->enum_info;
							for (int i = 0; i < ei->len; i++) {
								if (!strcmp(ei->names[i], lit->string)) {
									set(c, var_num, ei->values[i]);
								}
							}
							break;
						}
					case 5:
						set(c, var_num, lit->string[0]);
						break;
				}
				return;
			}
		case ast_ptr_literal:
			return;
		case ast_binary:
			{
				struct BinaryOp *bin = node->node;
				switch (bin->op) {
					case op_plus:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						add(c, var_num, t1, t2);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_minus:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						sub(c, var_num, t1, t2);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_mult:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						mul(c, var_num, t1, t2);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_div:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						_div(c, var_num, t1, t2);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_mod:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						mod(c, var_num, t1, t2);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_eq:
						{
						}
					case op_neq:
					case op_lt:
					case op_gt:
					case op_lt_eq:
					case op_gt_eq:
					case op_log_and:
					case op_log_or:
						//nothing
					case op_assign:
					case op_decl:
						{
							allocas(c, var_num, size_of(node->info));
							u16 pos = tempr(vl);
							addi(c, pos, var_num, 0);
							add_var(vl, var_num, bin->left->node, node->info);
							return;
						}
					case op_def:
					case op_const:
						return;
						//
					case op_plus_a:
					case op_minus_a:
					case op_mult_a:
					case op_div_a:
					case op_mod_a:
          case op_range:
            return;
          case op_subscript:
						{
							struct Typeinfo *ti = bin->left->info;
							switch (ti->array_info->atype) {
								case arrayt_dynamic:
								case arrayt_static:
									{
										u16 t1 = tempr(vl);
										eval_expr(c, bin->right, vl, var_num); // index
										set(c, t1, offset_of(node->info)); // size
										mul(c, var_num, var_num, t1); // index * size
										eval_expr(c, bin->left, vl, t1); // base
										load(c, 8, 0, t1, t1, 0); // load data pointer
										add(c, var_num, var_num, t1); // add offset to data pointer
										// if (node->info->type == t_int) {
										// 	load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
										// } else {
										// 	load(c, size_of(node->info), 0, var_num, var_num, 0);
										// }
										dtemp(vl);
										return;
									}
								case arrayt_stack:
									{
										u16 t1 = tempr(vl);
										eval_expr(c, bin->right, vl, var_num); // index
										set(c, t1, offset_of(node->info)); // size
										mul(c, var_num, var_num, t1); // index * size
										get_pointer(c, bin->left, vl, t1); // base
										add(c, var_num, var_num, t1); // base + offset
										// if (node->info->type == t_int) {
										// 	load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
										// } else {
										// 	load(c, size_of(node->info), 0, var_num, var_num, 0);
										// }
										dtemp(vl);
										return;
									}
							}
							return;
						}
					case op_cast:
						eval_expr(c, bin->left, vl, var_num);
						return;
					case op_access:
						{
							struct Typeinfo *ti = bin->left->info;
							if (ti->type == t_module) {
								struct AstModule *mod = c->bg->t->cur_module;
								c->bg->t->cur_module = ti->mod;
								eval_expr(c, bin->right, vl, var_num);
								c->bg->t->cur_module = mod;
								return;
							}
							if (ti->type == t_array) {
								if (!strcmp("data", bin->right->node)) {
									switch (ti->array_info->atype) {
										case arrayt_dynamic:
											eval_expr(c, bin->left, vl, var_num);
											break;
										case arrayt_static:
											eval_expr(c, bin->left, vl, var_num);
											break;
										case arrayt_stack:
											printf("cant set data of stack array");
											error(c->bg->t, node);
											segexit();
											break;
									}
								}
								if (!strcmp("len", bin->right->node)) {
									switch (ti->array_info->atype) {
										case arrayt_dynamic:
											eval_expr(c, bin->left, vl, var_num);
											addi(c, var_num, var_num, 8);
											break;
										case arrayt_static:
											set(c, var_num, ti->array_info->capacity);
											// printf("cant set len of static array");
											// error(c->bg->t, node);
											// segexit();
											break;
										case arrayt_stack:
											printf("cant set len of stack array");
											error(c->bg->t, node);
											segexit();
											break;
									}
								}
								if (!strcmp("max", bin->right->node)) {
									if (ti->array_info->atype != arrayt_dynamic) {
										printf("max is not available on stack or static arrays");
										error(c->bg->t, node);
									}
									eval_expr(c, bin->left, vl, var_num);
									addi(c, var_num, var_num, 16);
									// load(c, 8, 0, var_num, var_num, 16); // @Wordsize
								}
								return;
							}
							struct StructInfo *si = ti->struct_info;
							u64 offset = 0;
							int i;
							for (i = 0; i < si->len; i++) {
								offset += fix_alignment(alignment_of(si->types[i]), offset);
								if (!strcmp(si->names[i], bin->right->node)) { // @Fixup multiple struct level
									break;
								}
								offset += size_of(si->types[i]);
							}
							if (ti->pointer_level) {
								u16 t1 = tempr(vl);
								eval_expr(c, bin->left, vl, t1);
								addi(c, var_num, t1, offset);
								dtemp(vl);
							} else if (!ti->pointer_level) {
								u16 t1 = tempr(vl);
								get_pointer(c, bin->left, vl, t1);
								addi(c, var_num, t1, offset);
								dtemp(vl);
								// addi(c, var_num, r_fp, offset + find_var(vl, bin->left->node));
							}
							return;
						}
				}
				return;
			}
		case ast_unary:
			{
				struct UnaryOp *op = node->node;
				switch (op->op) {
					case op_minus:
						{
							u16 t1 = tempr(vl);
							eval_expr(c, op->on, vl, t1);
							set(c, var_num, 0);
							sub(c, var_num, var_num, t1); // @Fixup float support
	 						return;
						}
					case op_not:
						return;
					case op_take_pointer:
						if (op->on->type == ast_unary) {
							op = op->on->node;
							switch (op->op) {
								// case op_paren:
							}
							/*
							while (op->on->type == ast_unary) {
								//TODO multiple take pointer thingies
							}
							*/
						}
						// TODO how do i fix this tf
						get_pointer(c, op->on, vl, var_num);
						return;
					case op_ptr_type:
						return;
					case op_deref:
						eval_expr(c, op->on, vl, var_num);
						// if (node->info->type == t_int) {
						// 	load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
						// } else {
						// 	load(c, size_of(node->info), 0, var_num, var_num, 0);
						// }
						return;
					case op_paren:
						eval_expr(c, op->on, vl, var_num);
						return;
				}
			}
		case ast_module_stmt:
		case ast_module:
		case ast_import:
		case ast_arr_lit:
			return;
		case ast_array:
		case ast_for:
		case ast_loop:
		case ast_if:
		case ast_while:
		case ast_match:
		case ast_struct:
		case ast_enum:
		case ast_directive:
		case ast_type:
		case ast_cflow:
			break;
	}
}

void eval_expr(struct Chunk *c, struct AstNode *node, struct VarList *vl, int var_num) {
	u64 temp, res;

	switch (node->type) {
		case ast_block:
			return;
		case ast_proc_call:
			{
			struct ProcCall *pc = node->node;
			struct Typeinfo *pi = find_type(c->bg->t, pc->name);
			struct BytecodeGen *bg = c->bg;
			u64 pid;
			if (!pi->proc_info->cproc) {
				pid = find_func(c->bg, pc->name);
				if (!pid) {
					pid = gen_proc(c->bg, pi) + f_last;
				}
			}
			u16 fp = tempr(vl);
			u16 sp = tempr(vl);
			addi(c, fp, r_fp, 0); // store current frame pointer
			addi(c, sp, r_sp, 0); // store current stack pointer
										   //

			u64 *temp, max = 0, len = 0;
			if (pid != f_bc_call) {// && pid != f_call_native) {
				for (int i = 0; i < pc->args->len; i++) {
					struct Typeinfo *cur = pc->args->nodes[i].info;
					if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
						if (!max) {
							max = 20;
							temp = malloc(20 * sizeof(u64));
						}
						u16 t1 = tempr(vl);
						get_pointer(c, &pc->args->nodes[i], vl, t1);
						temp[len] = push(c, vl, t1, 8);
						len++;
						set(c, t1, pc->args->nodes[i].info->array_info->capacity);
						push(c, vl, t1, 8);
						dtemp(vl);
					} else if (cur->type == t_string_literal) {
						if (!max) {
							max = 20;
							temp = malloc(20 * sizeof(u64));
						}
						u16 t1 = tempr(vl);
						eval_expr(c, &pc->args->nodes[i], vl, t1);
						temp[len] = push(c, vl, t1, 8);
						len++;
						dtemp(vl);
					}
				}
			}
			u16 pos = tempr(vl);
			addi(c, pos, r_sp, 0);
			// u64 pos = vl->pos + fix_alignment(8, vl->pos);
			// vl->pos = pos;
			//if (pc->args->len > 0) {
				//pos += fix_alignment(alignment_of(pc->args->nodes[0].info), vl->pos);
			//}
			u64 t = 0;
			u16 t1 = tempr(vl);
			if (pi->proc_info->cproc) {
				struct ForeignLib *flibs = bg->t->flibs;
				int found = 0;
				u64 fidx = pi->proc_info->foreign_idx;
				u64 csidx = flibs->cslen[fidx] - 1;
				for (int i = 0; i < flibs->cslen[fidx]; i++) {
						if (flibs->cprocs[fidx][i] == pi) {
							found = 1;
							csidx = i;
							break;
						}
				}
				if (!found) {
					if (flibs->cslen[fidx] == flibs->csmax[fidx] - 1) {
						flibs->csmax[fidx] += 20;
						flibs->cprocs[fidx] = realloc(flibs->cprocs[fidx], sizeof(void*) * flibs->csmax[fidx]);
					}
					flibs->cprocs[fidx][flibs->cslen[fidx]] = pi;
					flibs->cslen[fidx] += 1;
					csidx = flibs->cslen[fidx] - 1;
				}
				pid = f_call_native;
				set(c, t1, pi->proc_info->foreign_idx);
				add_arg(c, t1); // push the first argument (c lib index)
				set(c, t1, csidx);
				add_arg(c, t1); // push the first argument (c native function to call) 
			}
			for (int i = 0; i < pc->args->len; i++) {
				struct Typeinfo *cur = pc->args->nodes[i].info;
				if (pid == f_bc_call) {// || pid == f_call_native) {
					eval_expr(c, &pc->args->nodes[i], vl, t1);
				} else {
					if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
						addi(c, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_string_literal) {
						load(c, 8, 0, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_struct && !cur->pointer_level) {
						get_pointer(c, &pc->args->nodes[i], vl, t1);
					} else {
						eval_expr(c, &pc->args->nodes[i], vl, t1);
					}
				}
				add_arg(c, t1);
			}
			dtemp(vl);
			add(c, r_fp, r_fp, pos); // offset current frame pointer by stack pos
			set(c, r_sp, 0); // set stack pointer to zero so the arguments work
			call(c, pid); // call function :flushed: (who would have guessed!!)
			load_arg(c, var_num); // load return value
			addi(c, r_fp, fp, 0); // restore frame pointer
			addi(c, r_sp, sp, 0); // restore stack pointer
			dtemp(vl);
			return;
			}
		case ast_proc_hdr:
		case ast_cproc_hdr:
		case ast_proc:
			return;
		case ast_ident:
			temp = find_var(vl, node->node); // TODO do the thing with the type sizes
			if (node->info->type == t_int) {
				load(c, size_of(node->info), node->info->isigned, var_num, temp, 0);
		  } else {
				load(c, size_of(node->info), 0, var_num, temp, 0);
			}
			return;
		case ast_literal:
			{
				struct Literal *lit = node->node;
				switch (lit->type) {
					case -1:
						set(c, var_num, 0); //TODO adjust per type
						break;
					case 0:
						set(c, var_num, lit->integer);
						break;
					case 1:
						if (node->info->size == 4) {
							float tempf = (float)lit->fl;
							set(c, var_num, *(u64*)&tempf);
						} else {
							set(c, var_num, *(u64*)&lit->fl);
						}
						break;
					case 2:
						load_rodata(c, var_num, c->bg->rodata.len);
						add_rodata(c->bg, lit->string, strlen(lit->string));
						add_rodata(c->bg, "\0", 1);
						u16 t1 = tempr(vl);
						allocas(c, t1, 8);
						// vl->pos += fix_alignment(8, vl->pos);
						// u64 pos = vl->pos;
						store(c, 8, var_num, t1, 0);
						// addi(c, var_num, t1, 0);
						u16 t2 = tempr(vl);
						allocas(c, t2, 8);
						// vl->pos += 8 + fix_alignment(8, vl->pos);
						set(c, var_num, strlen(lit->string));
						store(c, 8, var_num, t2, 0);
						addi(c, var_num, t1, 0);
						// vl->pos += 8 + fix_alignment(8, vl->pos);
						// addi(c, var_num, r_fp, pos);
						break;
					case 3:
						set(c, var_num, lit->integer);
						break;
					case 4:
						{
							struct EnumInfo *ei = node->info->enum_info;
							for (int i = 0; i < ei->len; i++) {
								if (!strcmp(ei->names[i], lit->string)) {
									set(c, var_num, ei->values[i]);
								}
							}
							break;
						}
					case 5:
						set(c, var_num, lit->string[0]);
						break;
				}
				return;
			}
		case ast_ptr_literal:
		case ast_binary:
			{
				struct BinaryOp *bin = node->node;
				switch (bin->op) {
					case op_plus:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						if (node->info->type == t_int || node->info->type == t_int_literal) {
							add(c, var_num, t1, t2);
						} else {
							addf(c, node->info->size, var_num, t1, t2);
						}
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_minus:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						if (node->info->type == t_int || node->info->type == t_int_literal) {
							sub(c, var_num, t1, t2);
						} else {
							subf(c, node->info->size, var_num, t1, t2);
						}
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_mult:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						if (node->info->type == t_int || node->info->type == t_int_literal) {
							if (node->info->isigned) {
								mul(c, var_num, t1, t2);
							} else {
								mulu(c, var_num, t1, t2);
							}
						} else {
							mulf(c, node->info->size, var_num, t1, t2);
						}
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_div:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						if (node->info->type == t_int || node->info->type == t_int_literal) {
							if (node->info->isigned) {
								_div(c, var_num, t1, t2);
							} else {
								_divu(c, var_num, t1, t2);
							}
						} else {
							_divf(c, node->info->size, var_num, t1, t2);
						}
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_mod:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						if (node->info->type == t_int || node->info->type == t_int_literal) {
							if (node->info->isigned) {
								mod(c, var_num, t1, t2);
							} else {
								modu(c, var_num, t1, t2);
							}
						} else {
							//_modf(c, var_num, var_num, var_num + 1);
							printf("dont mod floats rn\n");
							segexit();
						}
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_eq:
					case op_neq:
					case op_lt:
					case op_gt:
					case op_lt_eq:
					case op_gt_eq:
						if (bin->right->info->type == t_float) {
							u16 t1 = tempr(vl);
							u16 t2 = tempr(vl);
							u8 len = bin->right->info->size;
							eval_expr(c, bin->left, vl, t1);
							eval_expr(c, bin->right, vl, t2);
							switch (bin->op) {
								case op_eq:
									feq(c, len, var_num, t1, t2);
									break;
								case op_neq:
									feq(c, len, var_num, t1, t2);
									noti(c, var_num, var_num);
									break;
								case op_lt:
									flt(c, len, var_num, t1, t2);
									break;
								case op_gt:
									fgt(c, len, var_num, t1, t2);
									break;
								case op_lt_eq:
									fle(c, len, var_num, t1, t2);
									break;
								case op_gt_eq:
									fge(c, len, var_num, t1, t2);
									break;
							}
							dtemp(vl);
							dtemp(vl);
						} else {
							compare(c, bin, vl, var_num);
						}
						return;
					case op_log_and:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						u64 brpos = c->len;
						bne(c, t1, r_zero, 0);
						set(c, var_num, 0);
						u64 jmp0pos = c->len;
						jump(c, 0);
						struct BranchI *br = (struct BranchI*)((u64)c->code + brpos);
						br->addr = c->len;
						brpos = c->len;
						bne(c, t2, r_zero, 0);
						set(c, var_num, 0);
						u64 jmp1pos = c->len;
						jump(c, 0);
						br = (struct BranchI*)((u64)c->code + brpos);
						br->addr = c->len;
						set(c, var_num, 1);
						struct JmpI *jmp0 = (struct JmpI*)((u64)c->code + jmp0pos);
						struct JmpI *jmp1 = (struct JmpI*)((u64)c->code + jmp1pos);
						jmp0->addr = c->len;
						jmp1->addr = c->len;
						dtemp(vl);
						dtemp(vl);
						}
						return;
					case op_log_or:
						{
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->left, vl, t1);
						eval_expr(c, bin->right, vl, t2);
						u64 br0pos = c->len;
						bne(c, t1, r_zero, 0);
						u64 br1pos = c->len;
						bne(c, t2, r_zero, 0);
						set(c, var_num, 0);
						u64 jmppos = c->len;
						jump(c, 0);
						struct BranchI *br0 = (struct BranchI*)((u64)c->code + br0pos);
						struct BranchI *br1 = (struct BranchI*)((u64)c->code + br1pos);
						br0->addr = c->len;
						br1->addr = c->len;
						set(c, var_num, 1);
						struct JmpI *jmp = (struct JmpI*)((u64)c->code + jmppos);
						jmp->addr = c->len;
						dtemp(vl);
						dtemp(vl);
						}
						return;
						//nothing
					case op_assign:
						return;
					case op_decl:
					case op_def:
					case op_const:
						return;
						//
					case op_plus_a:
					case op_minus_a:
					case op_mult_a:
					case op_div_a:
					case op_mod_a:
					case op_range:
						return;
					case op_subscript:
						get_pointer(c, node, vl, var_num);
						if (node->info->type == t_int) {
							load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
						} else {
							load(c, size_of(node->info), 0, var_num, var_num, 0);
						}
						return;
					case op_slice:
						{
						eval_expr(c, bin->left, vl, var_num); // get array pointer?
						load(c, 8, 0, var_num, var_num, 0); // get array content pointer
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						if (((struct BinaryOp*)bin->right->node)->left->node) {
							eval_expr(c, ((struct BinaryOp*)bin->right->node)->left, vl, t1);
						} else {
							set(c, t1, 0);
						}
						set(c, t2, bin->left->info->array_info->type->size);
						mulu(c, t1, t1, t2);
						dtemp(vl);
						add(c, var_num, var_num, t1);
						u16 t3 = tempr(vl);
						u16 t4 = tempr(vl);
						allocas(c, t3, 16); // @Wordsize
						addi(c, t4, t3, 8);
						store(c, 8, var_num, t3, 0);
						if (((struct BinaryOp*)bin->right->node)->right->node) {
							eval_expr(c, ((struct BinaryOp*)bin->right->node)->right, vl, var_num);
						} else {
							eval_expr(c, bin->left, vl, var_num); // get array pointer?
							addi(c, var_num, var_num, 8);
							load(c, 8, 0, var_num, var_num, 0); // get array len
						}
						sub(c, var_num, var_num, t1);
						store(c, 8, var_num, t4, 0); // @Wordsize
						dtemp(vl);
						addi(c, var_num, t3, 0);
						return;
						}
					case op_cast:
						eval_expr(c, bin->left, vl, var_num);
						if (bin->left->info->type == t_float && bin->right->info->type == t_int) {
							u8 ffmt, ifmt = 0;
							if (bin->left->info->size == 4) {
								ffmt = ffmt_single;
							} else {
								ffmt = ffmt_double;
							}
							if (bin->right->info->isigned) {
								ifmt = 1;
							}
							ftoi(c, ffmt, ifmt, var_num, var_num);
						} else if (bin->left->info->type == t_int && bin->right->info->type == t_float) {
							u8 ffmt, ifmt = 0;
							if (bin->right->info->size == 4) {
								ffmt = ffmt_single;
							} else {
								ffmt = ffmt_double;
							}
							if (bin->left->info->isigned) {
								ifmt = 1;
							}
							itof(c, ifmt, ffmt, var_num, var_num);
						}
						return;
					case op_access:
						{
							struct Typeinfo *ti = bin->left->info;
							if (ti->type == t_module) {
								struct AstModule *mod = c->bg->t->cur_module;
								c->bg->t->cur_module = ti->mod;
								eval_expr(c, bin->right, vl, var_num);
								c->bg->t->cur_module = mod;
								return;
							}
							// struct StructInfo *si = ti->struct_info;
							if (ti->type == t_array) {
								if (!strcmp("data", bin->right->node)) {
									switch (ti->array_info->atype) {
										case arrayt_dynamic:
											eval_expr(c, bin->left, vl, var_num);
											load(c, 8, 0, var_num, var_num, 0);
											break;
										case arrayt_static:
											eval_expr(c, bin->left, vl, var_num);
											load(c, 8, 0, var_num, var_num, 0);
											break;
										case arrayt_stack:
											get_pointer(c, bin->left, vl, var_num);
											break;
									}
								}
								if (!strcmp("len", bin->right->node)) {
									switch (ti->array_info->atype) {
										case arrayt_dynamic:
											eval_expr(c, bin->left, vl, var_num);
											load(c, 8, 0, var_num, var_num, 8);
											break;
										case arrayt_static:
											eval_expr(c, bin->left, vl, var_num);
											load(c, 8, 0, var_num, var_num, 8);
											break;
										case arrayt_stack:
											set(c, var_num, ti->array_info->capacity);
											break;
									}
								}
								if (!strcmp("max", bin->right->node)) {
									if (ti->array_info->atype != arrayt_dynamic) {
										printf("cant get max of static or stack arrays");
										error(c, bin->left);
										segexit();
									}
									eval_expr(c, bin->left, vl, var_num);
									load(c, 8, 0, var_num, var_num, 16); // @Wordsize
								}
								return;
							} else {
								get_pointer(c, node, vl, var_num);
								if (node->info->type == t_int) {
									load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
								} else {
									load(c, size_of(node->info), 0, var_num, var_num, 0);
								}
							}
							return;
						}
				}
				return;
			}
		case ast_unary:
			{
				struct UnaryOp *op = node->node;
				switch (op->op) {
					case op_minus:
						{
							u16 t1 = tempr(vl);
							eval_expr(c, op->on, vl, t1);
							set(c, var_num, 0);
							sub(c, var_num, var_num, t1); // @Fixup float support
	 						return;
						}
					case op_not:
						{
							u16 t1 = tempr(vl);
							eval_expr(c, op->on, vl, t1);
							noti(c, var_num, t1);
							dtemp(vl);
							return;
						}
					case op_take_pointer:
						if (op->on->type == ast_unary) {
							op = op->on->node;
							switch (op->op) {
								// case op_paren:
							}
							/*
							while (op->on->type == ast_unary) {
								//TODO multiple take pointer thingies
							}
							*/
						}
						// TODO how do i fix this tf
						get_pointer(c, op->on, vl, var_num);
						return;
					case op_ptr_type:
						return;
					case op_deref:
						eval_expr(c, op->on, vl, var_num);
						if (node->info->type == t_int) {
							load(c, size_of(node->info), node->info->isigned, var_num, var_num, 0);
						} else {
							load(c, size_of(node->info), 0, var_num, var_num, 0);
						}
						return;
					case op_paren:
						eval_expr(c, op->on, vl, var_num);
						return;
				}
			}
		case ast_module_stmt:
		case ast_module:
		case ast_import:
		case ast_arr_lit:
			return;
		case ast_array:
				//load_rodata(c, var_num, c->bg->rodata.len);
				//add_rodata(c->bg, lit->string, strlen(lit->string));
				//return 1;
				{
					struct AstRoot *array = node->node;
					// load_rodata(c, var_num, c->bg->rodata.len);
					// add_rodata(c->bg, lit->string, strlen(lit->string));
					// return 1;
					allocas(c, var_num, size_of(node->info));
					u16 ball = tempr(vl);
					for (int i = 0; i < array->len; i++) {
						eval_expr(c, &array->nodes[i], vl, ball);
						store(c, size_of(node->info->array_info->type), ball, var_num, i * size_of(node->info->array_info->type));
					}
					return;
				}
		case ast_for:
		case ast_loop:
		case ast_if:
		case ast_while:
			break;
		case ast_match:
			break;
		case ast_struct:
		case ast_enum:
		case ast_directive:
		case ast_type:
		case ast_cflow:
			break;
	}
}

u64 init_to_zero(struct Chunk *c, u16 reg, struct Typeinfo *type, struct VarList *vl) {
		u16 pos = reg;
		switch (type->type) {
		case t_int:
		case t_float:
		case t_enum:
			store(c, size_of(type), r_zero, pos, 0);
			addi(c, pos, pos, size_of(type));
			return size_of(type);
		case t_struct:
			{
			struct StructInfo *si = type->struct_info;
			//addi(c, vl->len, r_fp, vl->pos + 8);
			//store(c, 8, vl->len, r_fp, vl->pos);
			u16 offset = 0;
			u16 t1 = tempr(vl);
			for (int i = 0; i < si->len; i++) {
				struct Literal *lit = &si->values[i];
				struct AstNode temp;
				temp.node = lit;
				temp.type = ast_literal;
				if (si->types[i]->type == t_struct || si->types[i]->type == t_array) {
					init_to_zero(c, pos, si->types[i], vl);
				} else {
					addi(c, pos, pos, fix_alignment(alignment_of(si->types[i]), offset));
					eval_expr(c, &temp, vl, t1);
					store(c, size_of(si->types[i]), t1, pos, 0);
					addi(c, pos, pos, size_of(si->types[i]));
				}
				offset += fix_alignment(alignment_of(si->types[i]), offset);
				offset += size_of(si->types[i]);
			}
			dtemp(vl);
			// dtemp(vl);
			return size_of(type); // @Fixup multiple struct levels
			}
		case t_array:
			{
				u64 offset = 0;
				struct ArrayInfo *ai = type->array_info;
				for (int i = 0; i < ai->capacity; i++) {
					init_to_zero(c, pos, ai->type, vl);
				}
				return offset + 8;
			}
	}
}

void add_debug(struct Chunk *c, u64 line_pos, u64 code_pos) {
	if (c->dlen == c->dmax - 1) {
		c->dmax += 20;
		c->debug = realloc(c->debug, sizeof(struct Debug) * c->dmax);
	}
	struct Debug debug;
	debug.line_pos = line_pos;
	debug.code_pos = code_pos;
	c->debug[c->dlen] = debug;
	c->dlen++;
}

void gen_bc(struct Chunk *c, struct AstNode *node, struct VarList *vl) {
	if (c->dlen == 0 || node->l0 != c->debug[c->dlen - 1].line_pos) {
		add_debug(c, node->l0, c->len);
	}
	switch (node->type) {
		case ast_block:
			{
			struct AstRoot *block = node->node;
			// u64 len = vl->temp;
			for (int i = 0; i < block->len; i++) {
				gen_bc(c, &block->nodes[i], vl);
			}
			// vl->temp = len;
			return;
			}
		case ast_proc_call:
			{
			struct ProcCall *pc = node->node;
			struct Typeinfo *pi = find_type(c->bg->t, pc->name);
			struct BytecodeGen *bg = c->bg;
			u64 pid;
			if (!pi->proc_info->cproc) {
				pid = find_func(c->bg, pc->name);
				if (!pid) {
					pid = gen_proc(c->bg, pi) + f_last;
				}
			}
			u16 fp = tempr(vl);
			u16 sp = tempr(vl);
			addi(c, fp, r_fp, 0); // store current frame pointer
			addi(c, sp, r_sp, 0); // store current stack pointer
										   //

			u64 *temp, max = 0, len = 0;
			if (pid != f_bc_call) {// && pid != f_call_native) {
			for (int i = 0; i < pc->args->len; i++) {
				struct Typeinfo *cur = pc->args->nodes[i].info;
				if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
					if (!max) {
						max = 20;
						temp = malloc(20 * sizeof(u64));
					}
					u16 t1 = tempr(vl);
					get_pointer(c, &pc->args->nodes[i], vl, t1);
					temp[len] = push(c, vl, t1, 8);
					len++;
					set(c, t1, pc->args->nodes[i].info->array_info->capacity);
					push(c, vl, t1, 8);
					dtemp(vl);
				} else if (cur->type == t_string_literal) {
					if (!max) {
						max = 20;
						temp = malloc(20 * sizeof(u64));
					}
					u16 t1 = tempr(vl);
					eval_expr(c, &pc->args->nodes[i], vl, t1);
					temp[len] = push(c, vl, t1, 8);
					len++;
					dtemp(vl);
				}
			}
			}
			u16 pos = tempr(vl);
			addi(c, pos, r_sp, 0);
			// u64 pos = vl->pos + fix_alignment(8, vl->pos);
			// vl->pos = pos;
			//if (pc->args->len > 0) {
				//pos += fix_alignment(alignment_of(pc->args->nodes[0].info), vl->pos);
			//}
			u64 t = 0;
			u16 t1 = tempr(vl);
			if (pi->proc_info->cproc) {
				struct ForeignLib *flibs = bg->t->flibs;
				int found = 0;
				u64 fidx = pi->proc_info->foreign_idx;
				u64 csidx = flibs->cslen[fidx] - 1;
				for (int i = 0; i < flibs->cslen[fidx]; i++) {
						if (flibs->cprocs[fidx][i] == pi) {
							found = 1;
							csidx = i;
							break;
						}
				}
				if (!found) {
					if (flibs->cslen[fidx] == flibs->csmax[fidx] - 1) {
						flibs->csmax[fidx] += 20;
						flibs->cprocs[fidx] = realloc(flibs->cprocs[fidx], sizeof(void*) * flibs->csmax[fidx]);
					}
					flibs->cprocs[fidx][flibs->cslen[fidx]] = pi;
					flibs->cslen[fidx] += 1;
					csidx = flibs->cslen[fidx] - 1;
				}
				pid = f_call_native;
				set(c, t1, pi->proc_info->foreign_idx);
				add_arg(c, t1); // push the first argument (c lib index)
				set(c, t1, csidx);
				add_arg(c, t1); // push the first argument (c native function to call) 
			}
			for (int i = 0; i < pc->args->len; i++) {
				struct Typeinfo *cur = pc->args->nodes[i].info;
				if (pid == f_bc_call) {// || pid == f_call_native) {
					eval_expr(c, &pc->args->nodes[i], vl, t1);
				} else {
					if (cur->type == t_array && cur->array_info->atype == arrayt_stack && pi->proc_info->types[i]->array_info->atype == arrayt_static) {
						addi(c, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_string_literal) {
						load(c, 8, 0, t1, temp[t], 0);
						t++;
					} else if (cur->type == t_struct && !cur->pointer_level) {
						get_pointer(c, &pc->args->nodes[i], vl, t1);
					} else {
						eval_expr(c, &pc->args->nodes[i], vl, t1);
					}
				}
				add_arg(c, t1);
			}
			dtemp(vl);
			add(c, r_fp, r_fp, pos); // offset current frame pointer by stack pos
			set(c, r_sp, 0); // set stack pointer to zero so the arguments work
			call(c, pid); // call function :flushed: (who would have guessed!!)
			load_arg(c, 0); // load return value
			addi(c, r_fp, fp, 0); // restore frame pointer
			addi(c, r_sp, sp, 0); // restore stack pointer
			dtemp(vl);
			return;
			}
		case ast_proc_hdr:
		case ast_cproc_hdr:
		case ast_proc:
		case ast_ident:
		case ast_literal:
		case ast_ptr_literal:
		case ast_binary:
			{
				struct BinaryOp *bin = node->node;
				switch (bin->op) {
					case op_assign:
						{
						if (bin->right->type == ast_no_init) {
							u16 t1 = tempr(vl);
							get_pointer(c, bin->left, vl, t1);
							return;
						}
						if (node->info->type == t_struct && !node->info->pointer_level) {
							u16 t1 = tempr(vl);
							u16 t2 = tempr(vl);
							u16 t3 = tempr(vl);
							get_pointer(c, bin->left, vl, t3);
							eval_expr(c, bin->right, vl, t1);
							u64 offset = 0;
							struct StructInfo *poopy = node->info->struct_info;
							for (int i = 0; i < poopy->len; i++) {
								load(c, size_of(poopy->types[i]), 0, t2, t1, offset);
								store(c, size_of(poopy->types[i]), t2, t3, offset); // TODO type
								offset += size_of(poopy->types[i]);
							}
							dtemp(vl);
							dtemp(vl);
							dtemp(vl);
							return;
						}
						// u16 t1 = tempr(vl);
						// eval_expr(c, bin->right, vl, t1);
						// store(c, size_of(node->info), t1, r_fp, left); // TODO type
						// dtemp(vl);
						u16 t1 = tempr(vl);
						u16 t2 = tempr(vl);
						eval_expr(c, bin->right, vl, t1);
						get_pointer(c, bin->left, vl, t2);
						store(c, size_of(node->info), t1, t2, 0);
						dtemp(vl);
						dtemp(vl);
						return;
						}
					case op_decl:
						{
							u16 t1 = tempr(vl);
							allocas(c, t1, size_of(node->info));
							u16 pos = tempr(vl);
							addi(c, pos, t1, 0);
							init_to_zero(c, pos, node->info, vl);
							add_var(vl, t1, bin->left->node, node->info);
							dtemp(vl);
							return;
						}
					case op_def:
						{
							if (node->info->type == t_struct && !node->info->pointer_level) {
								u16 t1 = tempr(vl);
								u16 t2 = tempr(vl);
								u16 t3 = tempr(vl);
								// get_pointer(c, bin->left, vl, t3);
								allocas(c, t3, size_of(node->info));
								eval_expr(c, bin->right, vl, t1);
								u64 offset = 0;
								struct StructInfo *poopy = node->info->struct_info;
								for (int i = 0; i < poopy->len; i++) {
									load(c, size_of(poopy->types[i]), 0, t2, t1, offset);
									store(c, size_of(poopy->types[i]), t2, t3, offset); // TODO type
									offset += size_of(poopy->types[i]);
								}
								dtemp(vl);
								dtemp(vl);
								dtemp(vl);
								add_var(vl, t3, bin->left->node, node->info);
								return;
							}
							if (bin->right->type == ast_array) {
								u16 t1 = tempr(vl);
								eval_expr(c, bin->right, vl, t1);
								add_var(vl, t1, bin->left->node, node->info);
								dtemp(vl);
								return;
							}
							u16 t1 = tempr(vl);
							u16 t2 = tempr(vl);
							allocas(c, t2, size_of(node->info));
							eval_expr(c, bin->right, vl, t1);
							store(c, size_of(node->info), t1, t2, 0);
							add_var(vl, t2, bin->left->node, node->info);
							dtemp(vl);
							dtemp(vl);
							return;
						}
					case op_const:
						return;
					case op_plus_a:
					case op_minus_a:
					case op_mult_a:
					case op_div_a:
					case op_mod_a:
						{
							struct BinaryOp tempb;
							struct AstNode tempn;
							struct AstNode tempn1;
							tempn.l0 = node->l0;
							tempn.node = &tempb;
							tempn.type = ast_binary;
							tempn.info = node->info;
							tempb.left = bin->left;
							tempn1.node = bin;
							tempb.op = op_assign;
							tempn1.type = ast_binary;
							tempn1.info = node->info;
							tempb.right = &tempn1;
							switch (bin->op) {
								case op_plus_a:
									bin->op = op_plus;
									break;
								case op_minus_a:
									bin->op = op_minus;
									break;
								case op_mult_a:
									bin->op = op_mult;
									break;
								case op_div_a:
									bin->op = op_div;
									break;
								case op_mod_a:
									bin->op = op_mod;
									break;
							}
							gen_bc(c, &tempn, vl);
							break;
						}
					case op_range:
					case op_subscript:
					case op_cast:
						return;
					case op_access:
						{
							struct Typeinfo *ti = bin->left->info;
							if (ti && ti->type == t_module) {
								struct AstModule *mod = c->bg->t->cur_module;
								c->bg->t->cur_module = ti->mod;
								gen_bc(c, bin->right, vl);
								c->bg->t->cur_module = mod;
								return;
							}
						}
				}
			}
		case ast_unary:
			{
				struct UnaryOp *op = node->node;
				switch (op->op) {
					case op_not:
						return;
					case op_take_pointer:
						return;
					case op_ptr_type:
					case op_deref:
						return;
					case op_paren:
						return;
				}
			}
		case ast_module_stmt:
		case ast_module:
		case ast_import:
		case ast_arr_lit:
		case ast_array:
			return;
		case ast_for:
			{
				struct AstFor *loop = node->node;
				struct BinaryOp *it = loop->iterator->node;
				u16 min = tempr(vl);
				eval_expr(c, it->left, vl, min);
				u16 stack_mem = tempr(vl);
				allocas(c, stack_mem, 8);
				store(c, 8, min, stack_mem, 0);
				// stack_mem = tempr(vl);
				// allocas(c, stack_mem, 8);
				u16 var = tempr(vl);
				add_var(vl, stack_mem, loop->var, find_type(c->bg->t, "u64"));
				u64 pos = c->len;
				struct AstNode temp;
				temp.node = loop->block;
				temp.type = ast_block;
				gen_bc(c, &temp, vl);
				add_debug(c, node->l1, c->len);
				u16 var_loaded = min;
				load(c, 8, 0, var, stack_mem, 0);
				addi(c, var, var, 1);
				store(c, 8, var, stack_mem, 0);
				u16 max = tempr(vl);
				eval_expr(c, it->right, vl, max);
				if (it->right->info->isigned) {
					blt(c, var, max, pos); // @Fixup replace with lt
				} else {
					bltu(c, var, max, pos);
				}
				dtemp(vl);
				dtemp(vl);
				return;
			}
		case ast_loop:
			{
				// struct AstRoot *loop = node->node;
				u64 len = c->len;
				struct AstNode temp;
				temp.node = node->node;
				temp.type = ast_block;
				gen_bc(c, &temp, vl);
				jump(c, len);
				return;
			}
		case ast_if:
			{
				struct AstIf *_if = node->node;
				u16 cond = tempr(vl);
				eval_expr(c, _if->cond, vl, cond);
				u64 brpos = c->len;
				beq(c, cond, r_zero, 0);
				dtemp(vl);
				struct AstNode temp;
				temp.node = _if->block;
				temp.type = ast_block;
				gen_bc(c, &temp, vl);
				u64 jmppos = c->len;
				if (_if->else_block) {
					jump(c, 0);
				}
				struct BranchI *br = (struct BranchI*)((u64)c->code + brpos);
				br->addr = c->len;
				if (_if->else_block) {
					gen_bc(c, _if->else_block, vl);
					struct JmpI *jmp = (struct JmpI*)((u64)c->code + jmppos);
					jmp->addr = c->len;
				}
				return;
			}
		case ast_while:
			{
				struct AstWhile *loop = node->node;
				u64 pos = c->len;
				struct AstNode temp;
				temp.node = loop->block;
				temp.type = ast_block;
				gen_bc(c, &temp, vl);
				u16 cond = tempr(vl);
				eval_expr(c, loop->cond, vl, cond);
				bne(c, cond, r_zero, pos);
				dtemp(vl);
				return;
			}
		case ast_match:
			{
				struct Match *match = node->node;
				u16 on = tempr(vl);
				eval_expr(c, match->on, vl, on);
				u64 *branches = malloc(sizeof(u64) * match->len);
				u16 cond = tempr(vl);
				for (int i = 0; i < match->len; i++) {
					eval_expr(c, match->conds[i], vl, cond);
					branches[i] = c->len;
					beq(c, on, cond, 0);
				}
				u64 _default = c->len;
				jump(c, 0);
				u64 *jmps = branches;
				for (int i = 0; i < match->len; i++) {
					struct BranchI *cur_branch = (struct BranchI*)((u64)c->code + branches[i]);
					cur_branch->addr = c->len;
					struct AstNode temp;
					temp.node = match->blocks[i];
					temp.type = ast_block;
					gen_bc(c, &temp, vl);
					jmps[i] = c->len;
					jump(c, 0);
				}
				for (int i = 0; i < match->len; i++) {
					struct JmpI *cur_jmp = (struct JmpI*)((u64)c->code + jmps[i]);
					cur_jmp->addr = c->len;
				}
				struct JmpI *cur_jmp = (struct JmpI*)((u64)c->code + _default);
				cur_jmp->addr = c->len;
				return;
			}
		case ast_struct:
		case ast_enum:
		case ast_directive:
		case ast_type:
				return;
		case ast_cflow:
			{
				struct CFlow *cf = node->node;
				switch (cf->type) {
					case 0: // break
						break;
					case 1: //return
						{
							u16 t1 = tempr(vl);
							if (cf->on) {
								eval_expr(c, cf->on, vl, t1);
								add_arg(c, t1);
								// store(c, size_of(cf->on->info), t1, r_fp, 0);
							}
							ret(c);
							dtemp(vl);
							return;
						}
				}
			}
	}
}

u64 gen_proc(struct BytecodeGen *bg, struct Typeinfo *info) {
	//TODO replace name with Proc
	struct BinaryOp *bin = info->node->node;
	if (bg->fmax == bg->flen - 1 ) {
		bg->fmax += 20;
		bg->functions = realloc(bg->functions, bg->fmax * sizeof(struct Function));
	}
	struct Function *f = &bg->functions[bg->flen];
	f->name = bin->left->node;
	struct Chunk *c = &f->c;
	c->bg = bg;
	c->proc = f;
	// c->t = bg->t->t;
	u64 fi = bg->flen;
	bg->flen += 1;
	init_chunk(c, 1);
	f->allocas = malloc(sizeof(struct Chunk));
	init_chunk(f->allocas, 0);
	f->labels = malloc(sizeof(struct Chunk));
	init_chunk(f->labels, 0);
	add_debug(c, info->node->l0, 0);
	struct VarList varlist;
	varlist.max = 20;
	varlist.len = r_last;
	varlist.temp = 0;
	varlist.vars = malloc(varlist.max * sizeof(char*));
	varlist.lens = malloc(varlist.max * sizeof(u64));
	for (int i = 0; i < info->proc_info->len; i++) {
		//TODO uh support diff arg types
		u16 t1 = tempr(&varlist);
		u16 t2 = tempr(&varlist);
		load_arg(c, t1);
		allocas(c, t2, size_of(info->proc_info->types[i]));
		if (info->proc_info->types[i]->type == t_struct && !info->proc_info->types[i]->pointer_level) {
			store(c, 8, t1, t2, 0); // @Wordsize @Fixup currently passing stack structs doesnt copy them
		} else {
			store(c, size_of(info->proc_info->types[i]), t1, t2, 0); 
		}
		add_var(&varlist, t2, info->proc_info->names[i], info->proc_info->types[i]);
	}
	struct AstNode *node;
	if (bin->right->type == ast_proc) {
		struct Proc *p = bin->right->node;
		for (int i = 0; i < p->body->len; i++) {
			node = &p->body->nodes[i];
			gen_bc(c, node, &varlist);
		}
	}
	ret(c);
	for (int i = 0; i < varlist.temp; i++) {
		printf("reg %i: \"%s\"\n", varlist.lens[i], varlist.vars[i]);
	}
	return fi;
}

struct BytecodeGen *generate_bytecode(struct Typechecker *t, struct AstModule *mainm) {
	struct BytecodeGen *bg = malloc(sizeof(struct BytecodeGen));
	bg->t = t;
	t->flibs->csmax = malloc(sizeof(u64) * t->flibs->liblen);
	t->flibs->cslen = malloc(sizeof(u64) * t->flibs->liblen);
	t->flibs->cprocs = malloc(sizeof(struct Typeinfo**) * t->flibs->liblen);
	for (int i = 0; i < t->flibs->liblen; i++) {
		t->flibs->csmax[i] = 20;
		t->flibs->cslen[i] = 0;
		t->flibs->cprocs[i] = malloc(sizeof(void*) * t->flibs->csmax[i]);
	}
	init_chunk(&bg->c, 1);
	init_chunk(&bg->rodata, 0);
	bg->fmax = 20;
	bg->flen = 0;
	bg->functions = malloc(sizeof(struct Function) * bg->fmax);
	bg->flibs = t->flibs;
	// struct AstRoot *gs = mainm->global_scope[0];
	struct Typeinfo *info = find_type(t, "main");
	gen_proc(bg, info);
	return bg;
}
