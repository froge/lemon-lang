struct CGen {
	int max;
	int len;
	char *res;
	char *cur;
	int depth;
	int in_block;
	struct Scope *cscope;
	struct Typechecker *t;
};

void setup_internal_types_c(struct CGen *cg) {
	cg->len += snprintf(cg->res, cg->max, "#include <stdint.h>\n\ntypedef int8_t lemi8;\ntypedef int16_t lemi16;\ntypedef int32_t lemi32;\ntypedef int64_t lemi64;\ntypedef uint8_t lemu8;\ntypedef uint16_t lemu16;\ntypedef uint32_t lemu32;\ntypedef uint64_t lemu64;\n\ntypedef float lemf32;\ntypedef double lemf64;\n\ntypedef char lemchar;\n\ntypedef lemu8 lembool;\ntypedef lemi32 lemint;\ntypedef void lemvoid;\n\n\n\n");
}

void resize_cgen(struct CGen *cg, int len) {
	if (cg->max - cg->len < len) {
		cg->max *= 2;
		cg->res = realloc(cg->res, cg->max);
	}
}

struct Typeinfo *find_idents(struct Scope *cs, char *ident) {
	int len = strlen(ident);
	for (int i = 0; i < cs->ilen; i++) {
		if (len == strlen(cs->idents[i])) {
			if (!memcmp(ident, cs->idents[i], len)) {
				return cs->itypes[i];
			}
		}
	}
	return 0;
}

void gen_cname_info(struct CGen *cg, struct Typeinfo *info) {
	switch (info->type) {
		default:
	}
}

void gen_array(struct CGen *cg, struct AstRoot *arr) {
}

void gen_c(struct CGen *cg, struct AstNode *node) {
	struct AstNode temp;
	switch (node->type) {
		case ast_block:
			struct AstRoot *block = node->node;
			int was_in_block = cg->in_block;
			if (!cg->in_block) {
				cg->in_block = 1;
			}
			cg->depth++;
			cg->cscope = block->scope;
			resize_cgen(cg, 3);
			cg->len += sprintf(&cg->res[cg->len-1], "{\n");
			for (int i = 0; i < block->len; i++) {
				resize_cgen(cg, 2 * cg->depth);
				for (int dt = 0; dt < cg->depth; dt++) {
					cg->len += sprintf(&cg->res[cg->len-1], "\t");
				}
				gen_c(cg, &block->nodes[i]);
				switch (block->nodes[i].type) {
					case ast_block:
					case ast_proc_hdr:
					case ast_cproc_hdr:
					case ast_proc:
					case ast_ptr_literal:
					case ast_unary:
						//unimplemented
					case ast_module:
					case ast_import:
					case ast_arr_type:
					case ast_for:
					case ast_if:
					case ast_while:
						resize_cgen(cg, 2);
						cg->len += sprintf(&cg->res[cg->len-1], "\n");
						break;
					case ast_proc_call:
					case ast_ident:
					case ast_literal:
						resize_cgen(cg, 3);
						cg->len += sprintf(&cg->res[cg->len-1], ";\n");
						break;
					case ast_binary:
						int type = ((struct BinaryOp*)block->nodes[i].node)->right->type;
						if (type == ast_proc || type == ast_struct || type == ast_enum) {
							resize_cgen(cg, 3);
							cg->len += sprintf(&cg->res[cg->len-1], "\n\n");
							break;
						}
					case ast_array:
					case ast_struct:
					case ast_cflow:
						// how
					case ast_enum:
						//huh
					case ast_directive:
					case ast_type:
						resize_cgen(cg, 3);
						cg->len += sprintf(&cg->res[cg->len-1], ";\n");
						break;
				}
			}
			resize_cgen(cg, 2 * (cg->depth - 1) + 3);
			for (int dt = 0; dt < cg->depth - 1; dt++) {
				cg->len += sprintf(&cg->res[cg->len-1], "\t");
			}
			cg->len += sprintf(&cg->res[cg->len-1], "}");
			if (!was_in_block) {
				cg->in_block = 0;
			}
			cg->depth--;
			return;
		case ast_proc_call:
			struct ProcCall *pc = node->node;
			temp.type = ast_ident;
			temp.node = pc->name;
			gen_c(cg, &temp);
			cg->res[cg->len-1] = '(';
			cg->len++;
			struct Typeinfo *pi = find_type(cg->t, pc->name);
			struct Typeinfo *tmp;
			struct AstNode *cur;
			for (int i = 0; i < pc->args->len; i++) {
				cur = &pc->args->nodes[i];
				if (pi->proc_info->types[i]->type == t_any) {
					resize_cgen(cg, snprintf(NULL, 0, "((lemany){.data=((void*)&"));
					cg->len += sprintf(&cg->res[cg->len-1], "((lemany){.data=((void*)&");
					tmp = default_literal(cg->t, cur->info);
					if (tmp != cur->info) {
						resize_cgen(cg, snprintf(NULL, 0, "(lem%s){", tmp->name));
						cg->len += sprintf(&cg->res[cg->len-1], "(lem%s){", tmp->name);
						gen_c(cg, &pc->args->nodes[i]);
						resize_cgen(cg, 2);
						cg->len += sprintf(&cg->res[cg->len-1], "}");
					} else {
						gen_c(cg, &pc->args->nodes[i]);
					}

					resize_cgen(cg, snprintf(NULL, 0, "), ._type=((lemTypeinfo){.type=%i, .pointer_level=%i,.size=%i,.name=((lemstring)&(lemarray){.data=\"%s\", .len=%i})})})",
							tmp->type, tmp->pointer_level, tmp->size, tmp->name, strlen(tmp->name)));
					cg->len += sprintf(&cg->res[cg->len-1], "), ._type=((lemTypeinfo){.type=%i, .pointer_level=%i,.size=%i,.name=((lemstring)&(lemarray){.data=\"%s\", .len=%i})})})",
							tmp->type, tmp->pointer_level, tmp->size, tmp->name, strlen(tmp->name));
				} else {
					gen_c(cg, &pc->args->nodes[i]);
				}
				if (i != pc->args->len -1) {
					resize_cgen(cg, 3);
					sprintf(&cg->res[cg->len-1], ", ");
					cg->len += 2;
				}
			}
			resize_cgen(cg, 2);
			sprintf(&cg->res[cg->len-1], ")");
			cg->len++;
			return;
		case ast_proc_hdr:
		case ast_cproc_hdr:
			struct ProcHeader *ph = node->node;
			if (ph->return_val) {
				resize_cgen(cg, 4);
				cg->len += sprintf(&cg->res[cg->len-1], "lem");
				gen_c(cg, ph->return_val);
			} else {
				resize_cgen(cg, 5);
				cg->len += sprintf(&cg->res[cg->len-1], "void");
			}
			if (!cg->cur) {
				printf("cur not defined this is my problem tbh");
			}
			resize_cgen(cg, strlen(cg->cur) + 3);
			cg->len += sprintf(&cg->res[cg->len-1], " %s(", cg->cur);
			for (int i = 0; i < ph->args->len; i++) {
				gen_c(cg, &ph->args->nodes[i]);
				if (i != ph->args->len -1) {
					resize_cgen(cg, 3);
					sprintf(&cg->res[cg->len-1], ", ");
					cg->len += 2;
				}
			}
			resize_cgen(cg, 2);
			cg->len += sprintf(&cg->res[cg->len-1], ")");
			return;
		case ast_proc:
			struct Proc *proc = node->node;
			temp.type = ast_proc_hdr;
			temp.node = proc->header;
			gen_c(cg, &temp);
			resize_cgen(cg, 2);
			cg->len += sprintf(&cg->res[cg->len-1], " ");
			temp.type = ast_block;
			temp.node = proc->body;
			gen_c(cg, &temp);
			return;
		case ast_ident:
			char *ident = node->node;
			resize_cgen(cg, strlen(ident) + 1);
			cg->len += sprintf(&cg->res[cg->len-1], "%s", ident);
			return;
		case ast_literal:
			struct Literal *lit = node->node;
			switch (lit->type) {
				case 0:
					resize_cgen(cg, snprintf(NULL, 0, "%llu", lit->integer) + 1);
					cg->len += sprintf(&cg->res[cg->len-1], "%llu", lit->integer);
					return;
				case 1:
					resize_cgen(cg, snprintf(NULL, 0, "%f", lit->fl) + 1);
					cg->len += sprintf(&cg->res[cg->len-1], "%f", lit->fl);
					return;
				case 2:
					resize_cgen(cg, snprintf(NULL, 0, "((lemstring)&(lemarray){.data=\"%s\", .len=%i})", lit->string, strlen(lit->string)));
					cg->len += sprintf(&cg->res[cg->len-1], "((lemstring)&(lemarray){.data=\"%s\", .len=%i})", lit->string, strlen(lit->string));
					return;
				case 3:
					resize_cgen(cg, snprintf(NULL, 0, "%llu", lit->integer) + 1);
					cg->len += sprintf(&cg->res[cg->len-1], "%llu", lit->integer);
					return;
				case 4:
					struct EnumInfo *ei = node->info->enum_info;
					for (int i = 0; i < ei->len; i++) {
						if (!strcmp(ei->names[i], lit->string)) {
							resize_cgen(cg, snprintf(NULL, 0, "%i", ei->values[i]) + 1);
							cg->len += sprintf(&cg->res[cg->len-1], "%i", ei->values[i]);
						}
					}
					return;
				case 5:
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "'%s'", lit->string);
					return;
			}
			return;
		case ast_ptr_literal:
			return;
		case ast_binary:
			{
				struct BinaryOp *op = node->node;
				switch (op->op) {
					case op_plus:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " + ");
						gen_c(cg, op->right);
						return;
					case op_minus:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " - ");
						gen_c(cg, op->right);
						return;
					case op_mult:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " * ");
						gen_c(cg, op->right);
						return;
					case op_div:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " / ");
						gen_c(cg, op->right);
						return;
					case op_mod:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " %% ");
						gen_c(cg, op->right);
						return;
					case op_eq:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " == ");
						gen_c(cg, op->right);
						return;
					case op_neq:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " != ");
						gen_c(cg, op->right);
						return;
					case op_lt:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " < ");
						gen_c(cg, op->right);
						return;
					case op_gt:
						gen_c(cg, op->left);
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], " > ");
						gen_c(cg, op->right);
						return;
					case op_lt_eq:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " <= ");
						gen_c(cg, op->right);
						return;
					case op_gt_eq:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " >= ");
						gen_c(cg, op->right);
						return;
					case op_log_and:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " && ");
						gen_c(cg, op->right);
						return;
					case op_log_or:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " || ");
						gen_c(cg, op->right);
						return;
					case op_assign:
						if (op->right->type == ast_array) {
						} else {
							gen_c(cg, op->left);
							if (op->left->type == ast_ident) {
								cg->cur = (char*)op->left->node;
							}
							if (op->left->type == ast_binary && ((struct BinaryOp*)op->left->node)->op == op_decl) {
								cg->cur = (char*)((struct BinaryOp*)op->left->node)->right->node;
							}
							resize_cgen(cg, 4);
							cg->len += sprintf(&cg->res[cg->len-1], " = ");
							gen_c(cg, op->right);
						}
						cg->cur = 0;
						return;
					case op_decl:
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], "lem");
						gen_c(cg, op->right);
						resize_cgen(cg, 2);
						cg->len += sprintf(&cg->res[cg->len-1], " ");
						gen_c(cg, op->left);
						// TODO add typeinfo to every ident and node
						/*
						if (op->right->info->type == t_array && cg->in_block) {
							int depth = 0;
							struct AstNode *tempp = op->right;
							while (tempp->type == ast_arr_type) {
								depth += 1;
								tempp = ((struct ArrayType*)tempp->node)->on;
							}
							depth--;
							resize_cgen(cg, 27);
							cg->len += sprintf(&cg->res[cg->len-1], " = init_array_struct(20);\n");
							resize_cgen(cg, 2 * cg->depth);
							if (depth) {
								for (int dt = 0; dt < cg->depth; dt++) {
									cg->len += sprintf(&cg->res[cg->len-1], "\t");
								}
							}
							for (int dt = 0; dt < depth; dt++) {
								resize_cgen(cg, strlen(op->left->node));
								cg->len += sprintf(&cg->res[cg->len-1], "%s", op->left->node);
								for (int k = 0; k < depth; k++) {
									resize_cgen(cg, 7 * k);
									cg->len += sprintf(&cg->res[cg->len-1], "->data");
								}
								resize_cgen(cg, 27);
								cg->len += sprintf(&cg->res[cg->len-1], " = init_array_struct(20);\n");
							}
							resize_cgen(cg, 2 * cg->depth);


							for (int dt = 0; dt < cg->depth; dt++) {
								cg->len += sprintf(&cg->res[cg->len-1], "\t");
							}
							resize_cgen(cg, strlen(op->left->node) + 19);
							cg->len += sprintf(&cg->res[cg->len-1], "init_array(%s", op->left->node);
							for (int k = 0; k < depth; k++) {
								resize_cgen(cg, 7 * k);
								cg->len += sprintf(&cg->res[cg->len-1], "->data");
							}
							resize_cgen(cg, 2);
							cg->len += sprintf(&cg->res[cg->len-1], ")");
						}
						*/
						return;
					case op_def:
						resize_cgen(cg, snprintf(NULL, 0, "lem%s", node->info->name));
						cg->len += sprintf(&cg->res[cg->len-1], "lem%s", node->info->name);
						resize_cgen(cg, node->info->pointer_level);
						for (int i = 0; i < node->info->pointer_level; i++) {
							cg->len += sprintf(&cg->res[cg->len-1], "*");
						}
						resize_cgen(cg, snprintf(NULL, 0, " ", node->info->name));
						cg->len += sprintf(&cg->res[cg->len-1], " ", node->info->name);
						gen_c(cg, op->left);
						resize_cgen(cg, snprintf(NULL, 0, " = ", node->info->name));
						cg->len += sprintf(&cg->res[cg->len-1], " = ", node->info->name);
						gen_c(cg, op->right);
						return;
					case op_const:
						if (op->right->type == ast_enum || op->right->type == ast_struct || op->right->type == ast_proc || op->right->type == ast_cproc_hdr) {
							cg->cur = (char*)op->left->node;
							gen_c(cg, op->right);
							cg->cur = 0;
						} else {
							resize_cgen(cg, snprintf(NULL, 0, "typedef lem"));
							cg->len += sprintf(&cg->res[cg->len-1], "typedef lem");
							gen_c(cg, op->right);
							resize_cgen(cg, 5);
							cg->len += sprintf(&cg->res[cg->len-1], " lem");
							gen_c(cg, op->left);
						}
						return;
					case op_plus_a:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " += ");
						gen_c(cg, op->right);
						return;
					case op_minus_a:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " -= ");
						gen_c(cg, op->right);
						return;
					case op_mult_a:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " *= ");
						gen_c(cg, op->right);
						return;
					case op_div_a:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " /= ");
						gen_c(cg, op->right);
						return;
					case op_mod_a:
						gen_c(cg, op->left);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " %%= ");
						gen_c(cg, op->right);
						return;
					case op_range:
						return;
					case op_subscript:
						{
						struct Typeinfo *info = node->info;
						if (info->type != t_array) {
							resize_cgen(cg, snprintf(NULL, 0, "((lem%s*)", info->name));
							cg->len += sprintf(&cg->res[cg->len-1], "((lem%s*)", info->name);
							gen_c(cg, op->left);
						} else {
							resize_cgen(cg, snprintf(NULL, 0, "((lemarray*)"));
							cg->len += sprintf(&cg->res[cg->len-1], "((lemarray*)");
							gen_c(cg, op->left);
						}
						if (op->left->type == ast_binary && ((struct BinaryOp*)op->left->node)->op == op_subscript) {
							resize_cgen(cg, snprintf(NULL, 0, ".data)["));
							cg->len += sprintf(&cg->res[cg->len-1], ".data)[");
							gen_c(cg, op->right);
							resize_cgen(cg, 2);
							cg->len += sprintf(&cg->res[cg->len-1], "]");
						} else {
							resize_cgen(cg, snprintf(NULL, 0, "->data)["));
							cg->len += sprintf(&cg->res[cg->len-1], "->data)[");
							gen_c(cg, op->right);
							resize_cgen(cg, 2);
							cg->len += sprintf(&cg->res[cg->len-1], "]");
						}
						return;
						}
					case op_access:
						gen_c(cg, op->left);
						struct Typeinfo *info = op->left->info;
						if (info->pointer_level) {
							resize_cgen(cg, 3);
							cg->len += sprintf(&cg->res[cg->len-1], "->");
						} else {
							resize_cgen(cg, 2);
							cg->len += sprintf(&cg->res[cg->len-1], ".");
						}
						gen_c(cg, op->right);
						return;
					case op_cast:
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], "(lem");
						gen_c(cg, op->right);
						resize_cgen(cg, 2);
						cg->len += sprintf(&cg->res[cg->len-1], ")");
						gen_c(cg, op->left);
						return;
				}
				return;
			}
		case ast_unary:
			struct UnaryOp *op = node->node;
			switch (op->op) {
				case op_mult:
					gen_c(cg, op->on);
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "*");
					return;
				case op_not:
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "!");
					gen_c(cg, op->on);
					return;
				case op_take_pointer:
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "&");
					gen_c(cg, op->on);
					return;
				case op_ptr_type:
				case op_deref:
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "*");
					gen_c(cg, op->on);
					return;
				case op_paren:
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "(");
					gen_c(cg, op->on);
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], ")");
			}
			return;
		case ast_module:
			struct AstModule *mod = node->node;
			for (int i = 0; i < mod->len; i++) {
				struct AstRoot *block = mod->global_scope[i];
				int was_in_block = cg->in_block;
				if (!cg->in_block) {
					cg->in_block = 1;
				}
				cg->cscope = block->scope;
				resize_cgen(cg, 3);
				for (int i = 0; i < block->len; i++) {
					resize_cgen(cg, 2 * cg->depth);
					for (int dt = 0; dt < cg->depth; dt++) {
						cg->len += sprintf(&cg->res[cg->len-1], "\t");
					}
					gen_c(cg, &block->nodes[i]);
					switch (block->nodes[i].type) {
						case ast_block:
						case ast_proc_hdr:
						case ast_cproc_hdr:
						case ast_proc:
						case ast_ptr_literal:
						case ast_unary:
							//unimplemented
						case ast_module:
						case ast_import:
						case ast_arr_type:
						case ast_for:
						case ast_if:
						case ast_while:
							resize_cgen(cg, 2);
							cg->len += sprintf(&cg->res[cg->len-1], "\n");
							break;
						case ast_proc_call:
						case ast_ident:
						case ast_literal:
							resize_cgen(cg, 3);
							cg->len += sprintf(&cg->res[cg->len-1], ";\n");
							break;
						case ast_binary:
							int type = ((struct BinaryOp*)block->nodes[i].node)->right->type;
							if (type == ast_proc || type == ast_struct || type == ast_enum) {
								resize_cgen(cg, 3);
								cg->len += sprintf(&cg->res[cg->len-1], "\n\n");
								break;
							}
						case ast_array:
						case ast_struct:
						case ast_cflow:
							// how
						case ast_enum:
							//huh
						case ast_directive:
						case ast_type:
							resize_cgen(cg, 3);
							cg->len += sprintf(&cg->res[cg->len-1], ";\n");
							break;
					}
				}
			}
			return;
		case ast_import:
			return;
		case ast_arr_type:
			struct ArrayType *at = node->node;
			resize_cgen(cg, 7);
			cg->len += sprintf(&cg->res[cg->len-1], "array*");
			return;
		case ast_array:
			return;
		case ast_for:
			struct AstFor *loop = node->node;
			// TODO: generalize for all types of iterators
			struct BinaryOp *bin = loop->iterator->node;
			struct Literal *left = bin->left->node;
			struct Literal *right = bin->right->node;
			resize_cgen(cg, snprintf(NULL, 0, "for (lemint %s = %d; %s < %d; %s++) ", loop->var, left->integer, loop->var, right->integer, loop->var));
			cg->len += sprintf(&cg->res[cg->len-1], "for (lemint %s = %d; %s < %d; %s++) ", loop->var, left->integer, loop->var, right->integer, loop->var);
			temp.type = ast_block;
			temp.node = loop->block;
			gen_c(cg, &temp);
			return;
		case ast_loop:
			resize_cgen(cg, 10);
			cg->len += sprintf(&cg->res[cg->len-1], "for (;;) ");
			temp.type = ast_block;
			temp.node = node->node;
			gen_c(cg, &temp);
			return;
		case ast_while:
			breakpoint();
			struct AstWhile *wl = node->node;
			resize_cgen(cg, 8);
			cg->len += sprintf(&cg->res[cg->len-1], "while (");
			gen_c(cg, wl->cond);
			resize_cgen(cg, 3);
			cg->len += sprintf(&cg->res[cg->len-1], ") ");
			temp.type = ast_block;
			temp.node = wl->block;
			gen_c(cg, &temp);
			return;
		case ast_if:
			struct AstIf *stmt = node->node;
			resize_cgen(cg, 5);
			cg->len += sprintf(&cg->res[cg->len-1], "if (");
			gen_c(cg, stmt->cond);
			resize_cgen(cg, 3);
			cg->len += sprintf(&cg->res[cg->len-1], ") ");
			temp.type = ast_block;
			temp.node = stmt->block;
			gen_c(cg, &temp);
			if (stmt->else_block) {
				gen_c(cg, stmt->else_block);
			}
			return;
		case ast_match:
			breakpoint();
			struct Match *match = node->node;
			resize_cgen(cg, 9);
			cg->len += sprintf(&cg->res[cg->len-1], "switch (");
			gen_c(cg, match->on);
			resize_cgen(cg, 5);
			cg->len += sprintf(&cg->res[cg->len-1], ") {");
			cg->depth++;
			for (int i = 0; i < match->len; i++) {
				resize_cgen(cg, 2);
				cg->len += sprintf(&cg->res[cg->len-1], "\n");
				for (int dt = 0; dt < cg->depth; dt++) {
					cg->len += sprintf(&cg->res[cg->len-1], "\t");
				}
				resize_cgen(cg, 6);
				cg->len += sprintf(&cg->res[cg->len-1], "case ");
				gen_c(cg, match->conds[i]);
				resize_cgen(cg, 3);
				cg->len += sprintf(&cg->res[cg->len-1], ":\n");
				for (int dt = 0; dt < cg->depth; dt++) {
					cg->len += sprintf(&cg->res[cg->len-1], "\t");
				}
				temp.type = ast_block;
				temp.node = match->blocks[i];
				gen_c(cg, &temp);
				resize_cgen(cg, 7);
				cg->len += sprintf(&cg->res[cg->len-1], "break;");
			}
			resize_cgen(cg, 2);
			cg->len += sprintf(&cg->res[cg->len-1], "\n");
			cg->depth--;
			for (int dt = 0; dt < cg->depth; dt++) {
				cg->len += sprintf(&cg->res[cg->len-1], "\t");
			}
			resize_cgen(cg, 2);
			cg->len += sprintf(&cg->res[cg->len-1], "}");
			resize_cgen(cg, 2);
			cg->len += sprintf(&cg->res[cg->len-1], "\n");
			return;
		case ast_struct:
			struct AstRoot *st = node->node;
			temp.type = ast_block;
			temp.node = st;
			resize_cgen(cg, 17);
			cg->len += sprintf(&cg->res[cg->len-1], "typedef struct ");
			gen_c(cg, &temp);
			resize_cgen(cg, strlen(cg->cur) + 6);
			cg->len += sprintf(&cg->res[cg->len-1], " lem%s;", cg->cur);
			return;
		case ast_enum:
			resize_cgen(cg, 14 + strlen(cg->cur));
			cg->len += sprintf(&cg->res[cg->len-1], "typedef int lem%s;", cg->cur);
			return;
		case ast_directive:
		case ast_type:
			return;
		case ast_cflow:
			struct CFlow *cf = node->node;
			switch (cf->type) {
				case 0:
					resize_cgen(cg, 6);
					cg->len += sprintf(&cg->res[cg->len-1], "break");
					break;
				case 1:
					resize_cgen(cg, 7);
					cg->len += sprintf(&cg->res[cg->len-1], "return");
					if (cf->on) {
						resize_cgen(cg, 2);
						cg->len += sprintf(&cg->res[cg->len-1], " ");
						gen_c(cg, cf->on);
					}
			}
	}
}

struct CGen *generate_c(struct Typechecker *t, struct AstRoot *gs) {
	struct CGen *cg = malloc(sizeof(struct CGen));
	cg->t = t;
	cg->max = 2000;
	cg->len = 0;
	cg->res = malloc(cg->max);

	setup_internal_types_c(cg);
	cg->cscope = gs->scope;

	for (int i = 0; i < gs->len; i++) {
		if (gs->nodes[i].type == ast_binary) {
			struct BinaryOp *bin = gs->nodes[i].node;
			if (bin->op == op_const) {
				switch (bin->right->type) {
					case ast_proc:
						struct Proc *proc = bin->right->node;
						struct ProcHeader *ph = proc->header;
						if (ph->return_val) {
							resize_cgen(cg, 4);
							cg->len += sprintf(&cg->res[cg->len-1], "lem");
							gen_c(cg, ph->return_val);
						} else {
							resize_cgen(cg, 5);
							cg->len += sprintf(&cg->res[cg->len-1], "void");
						}
						resize_cgen(cg, strlen(bin->left->node) + 3);
						cg->len += sprintf(&cg->res[cg->len-1], " %s(", bin->left->node);
						for (int i = 0; i < ph->args->len; i++) {
							gen_c(cg, &ph->args->nodes[i]);
							if (i != ph->args->len -1) {
								resize_cgen(cg, 3);
								sprintf(&cg->res[cg->len-1], ", ");
								cg->len += 2;
							}
						}
						resize_cgen(cg, 4);
						cg->len += sprintf(&cg->res[cg->len-1], ");\n");
						break;
					case ast_struct:
						struct AstRoot *st = bin->right->node;
						struct AstNode temp;
						temp.type = ast_block;
						temp.node = st;
						resize_cgen(cg, 17);
						cg->len += sprintf(&cg->res[cg->len-1], "typedef struct ");
						gen_c(cg, &temp);
						resize_cgen(cg, strlen(bin->left->node) + 7);
						cg->len += sprintf(&cg->res[cg->len-1], " lem%s;\n", bin->left->node);
						break;
					case ast_ident:
						resize_cgen(cg, snprintf(NULL, 0, "typedef lem"));
						cg->len += sprintf(&cg->res[cg->len-1], "typedef lem");
						gen_c(cg, bin->right);
						resize_cgen(cg, 5);
						cg->len += sprintf(&cg->res[cg->len-1], " lem");
						gen_c(cg, bin->left);
						break;
				}
			}
		}
	}

	for (int i = 0; i < gs->len; i++) {
		if (gs->nodes[i].type == ast_block) {
			cg->depth++;
		}
		gen_c(cg, &gs->nodes[i]);
		if (gs->nodes[i].type == ast_block) {
			cg->depth++;
		}
		switch (gs->nodes[i].type) {
			case ast_block:
			case ast_proc_hdr:
			case ast_cproc_hdr:
			case ast_proc:
			case ast_ptr_literal:
			case ast_unary:
			//unimplemented
			case ast_module:
			case ast_import:
			case ast_arr_type:
			case ast_for:
			case ast_if:
			case ast_while:
				resize_cgen(cg, 2);
				cg->len += sprintf(&cg->res[cg->len-1], "\n");
				break;
			case ast_proc_call:
			case ast_ident:
			case ast_literal:
				resize_cgen(cg, 3);
				cg->len += sprintf(&cg->res[cg->len-1], ";\n");
				break;
			case ast_binary:
				int type = ((struct BinaryOp*)gs->nodes[i].node)->right->type;
				if (type == ast_proc || type == ast_struct || type == ast_enum) {
					resize_cgen(cg, 2);
					cg->len += sprintf(&cg->res[cg->len-1], "\n\n");
					break;
				}
			case ast_array:
			case ast_struct:
			case ast_cflow:
			// how
			case ast_enum:
			//huh
			case ast_directive:
			case ast_type:
				resize_cgen(cg, 3);
				cg->len += sprintf(&cg->res[cg->len-1], ";\n");
				break;
		}
	}
	cg->res[cg->len] = 0;
	return cg;
}
