struct Debug {
	u64 line_pos;
	u64 code_pos;
};

struct Chunk {
	u64 max;
	u64 len;
	char *code;
	u64 dmax;
	u64 dlen;
	struct Function *proc;
	struct Debug *debug;
	// struct Tokenizer *t;
	struct BytecodeGen *bg;
};

struct Function {
	char *name;
	struct Chunk c;
	struct Chunk *allocas;
	struct Chunk *labels;
};

struct ForeignLib {
	u32 libmax;
	u32 liblen;
	char **libname;
	u64 *csmax;
	u64 *cslen;
	struct Typeinfo ***cprocs;
};

struct BytecodeGen {
	struct Typechecker *t;
	u64 offset;
	struct Chunk c;
	u64 fmax;
	u64 flen;
	struct Function *functions;
	struct ForeignLib *flibs;
	struct Chunk rodata;
};

struct ArgI {
	u8 instr_type;
	u16 reg;
};

struct ArithmeticI {
	u8 instr_type;
	u16 reg1;
	u16 reg2;
	u16 reg3;
};

struct ConvI {
	u8 instr_type;
	u8 fmt1; // TODO replace those with typeinfo indexes
	u8 fmt2;
	u16 reg1;
	u16 reg2;
};

struct AllocaSI {
	u8 instr_type;
	u16 reg;
	u32 len;
};

enum FFmts {
	ffmt_single,
	ffmt_double,
};

enum IFmts {
	ifmt_i8,
	ifmt_u8,
	ifmt_i16,
	ifmt_u16,
	ifmt_i32,
	ifmt_u32,
	ifmt_i64,
	ifmt_u64,
};

struct LoadRDI {
	u8 instr_type;
	u16 reg;
	u64 addr;
};

struct MemI {
	u8 instr_type;
	u16 reg;
	u16 addr;
	i64 offset;
};

struct SimpleI {
	u8 instr_type;
	u16 reg1;
	u16 reg2;
};

struct AddImmediateI {
	u8 instr_type;
	u16 reg1;
	u16 reg2;
	u64 val;
};

struct LPosI {
	u8 instr_type;
};

struct JmpI {
	u8 instr_type;
	u16 reg;
	i64 addr;
};

struct JmpRI { // jump relative instruction
	u8 instr_type;
	i64 addr;
};

struct BranchI {
	u8 instr_type;
	u16 reg1;
	u16 reg2;
	i64 addr;
};

struct RetI {
	u8 instr_type;
};

struct CallI {
	u8 instr_type;
	u64 func;
};

enum InternalFuncs {
	f_none, // so that i can check that find_func returned nothing
	f_malloc,
	f_memcpy,
	f_free,
	f_print,
	f_fopen,
	f_fwrite,
	f_fread,
	f_fclose,
	f_dlopen,
	f_dlsym,
	f_dlclose,
	f_ftell,
	f_fseek,
	f_call_native,
	f_bc_call, // integrated bytecode call function
	f_last,
};

enum Registers {
	r_zero,
	r_ip,
	r_fp,
	r_sp,
	r_last,
};

enum InstructionTypes {
	i_loadrd,
	i_loadd,
	i_loadw,
	i_loadh,
	i_loadb,
	i_stored,
	i_storew,
	i_storeh,
	i_storeb,
	i_loaddu,
	i_loadwu,
	i_loadhu,
	i_loadbu,
	i_storedu,
	i_storewu,
	i_storehu,
	i_storebu,
	i_addi,
	i_add,
	i_sub,
	i_mul,
	i_div,
	i_mod,
	i_mulu,
	i_divu,
	i_modu,
	i_not,
	i_addfs,
	i_subfs,
	i_mulfs,
	i_divfs,
	i_modfs,
	i_addfd,
	i_subfd,
	i_mulfd,
	i_divfd,
	i_modfd,
	i_ftoi,
	i_itof,
	i_lpos, // label position (idk)
	i_jmp,
	i_jmpr, // jump to label position (huh)
	i_bne,
	i_beq,
	i_blt,
	i_bltu,
	i_bge,
	i_bgeu,
	i_flts,
	i_fges,
	i_feqs,
	i_fltd,
	i_fged,
	i_feqd,
	i_call,
	i_allocas, // static alloca
	i_allocad, // dynamic alloca (do i need alignment arg?)
	i_aarg,
	i_larg,
	// i_push,
	// i_pop,
	//i_pushc,
	i_ret,
	// i_takep,
};
