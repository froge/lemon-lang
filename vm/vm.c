#include <malloc.h>
#include <stdio.h>
#include <ffi.h>
#ifndef _WIN32
#include <dlfcn.h>
#endif
#include <stdint.h>
#include <string.h>
#include <stdlib.h>


#include <common.h>
// #include <frontend/typeinfo.h>
#include <segexit.c>
#include <frontend/parser.h>
#include <sizes.c>
// #include <frontend/typeinfo.c>
#include "bytecode.h"
#include <bc_file.c>

int debug = 0;

struct FProc {
	void *fnptr;
	ffi_cif cif;
	ffi_type **arg_types;
	void **arg_values;
	ffi_type *return_type;
	int len;
};

struct VM {
	u8 stack[65536];
	u64 regs[500];
	u64 args[256];
	u8 arg_len;
	u8 arg_pos;
	struct Chunk *ch;
	void **libs;
	u64 ip;
	u64 sp;
	u64 ret;
	struct FProc **lcprocs;
};

u64 size_of(struct Typeinfo *);

void *to_ffi_type(struct Typeinfo *cur_type) {
	if (cur_type->type == t_struct) {
		ffi_type *ball = malloc(sizeof(ffi_type));
		struct StructInfo *si = cur_type->struct_info;
		ball->size = 0;
		ball->alignment = 0;
		ball->type = FFI_TYPE_STRUCT;
		ball->elements = malloc((si->len + 1) * sizeof(void*));
		for (int i = 0; i < si->len; i++) {
			ball->elements[i] = to_ffi_type(si->types[i]);
		}
		ball->elements[si->len] = 0;
		return ball;
	} else if (cur_type->type == t_float) {
		switch (size_of(cur_type)) {
			case 8:
				return &ffi_type_double;
			case 4:
				return &ffi_type_float;
		}
	} else {
		switch (size_of(cur_type)) {
			case 8:
				return &ffi_type_uint64;
			case 4:
				return &ffi_type_uint32;
			case 2:
				return &ffi_type_uint16;
			case 1:
				return &ffi_type_uint8;
		}
	}
}

void *get_instr(struct VM *vm) {
	return (void*)&vm->ch->code[vm->ip];
}

i64 get_reg(struct VM *vm, u16 reg) {
	return vm->regs[reg];
}

void set_reg(struct VM *vm, u16 reg, i64 val) {
	vm->regs[reg] = val;
}

u64 pop_arg(struct VM *vm) {
		vm->arg_len = 0;
		u64 ret = vm->args[vm->arg_pos];
		vm->arg_pos += 1;
		return ret;
}

void push_arg(struct VM *vm, u64 arg) {
		vm->arg_pos = 0;
		vm->args[vm->arg_len] = arg;
		vm->arg_len += 1;
}

// void pushs(struct VM *vm, u64 val) {
// 	memcpy(&vm->stack[vm->sp], &val, 8);
// 	vm->sp += 8; // TODO not only 64
// }

// u64 pops(struct VM *vm) {
// 	vm->sp -= 8; // TODO not only 64
// 	u64 ret;
// 	memcpy(&ret, &vm->stack[vm->sp], 8);
// 	return ret;
// }

void handle_native_funcs(struct VM *vm, u64 func) {
	if (debug)
		printf("call native ");
	u64 offset = get_reg(vm, r_fp);
	// u64 func_type = *(u64*)offset;
	u64 func_type = pop_arg(vm);
	// offset += 8;
	switch (func_type) {
		case f_print:
			{
			if (debug)
				printf("print\n");
			// u64 len = *(u64*)(offset + 8);
			// char *text = *(char**)(offset);
			char *text = (void*)pop_arg(vm);
			u64 len = pop_arg(vm);
			fwrite(text, len, 1, stdout);
			return;
			}
		case f_malloc:
			if (debug)
				printf("malloc\n");
			u64 len = pop_arg(vm);
			printf("balls %i\n", len);
			push_arg(vm, malloc(len));
			return;
		case f_memcpy:
			{
			if (debug)
				printf("memcpy\n");
			void *target = pop_arg(vm);
			void *src = pop_arg(vm);
			u64 len = pop_arg(vm);
			push_arg(vm, memcpy(target, src, len));
			return;
			}
		case f_free:
			if (debug)
				printf("free\n");
			void *ptr = pop_arg(vm);
			free(ptr);
			return;
		case f_fopen:
			if (debug)
				printf("fopen\n");
			char *filename = pop_arg(vm);
			char *mode = pop_arg(vm);
			push_arg(vm, fopen(filename, mode));
			return;
		case f_fread:
			{
			if (debug)
				printf("fread\n");
			void *buf = pop_arg(vm);
			u64 size = pop_arg(vm);
			u64 nmemb = pop_arg(vm);
			void *file = pop_arg(vm);
			push_arg(vm, fread(buf, size, nmemb, file));
			return;
			}
		case f_fwrite:
			{
			if (debug)
				printf("fwrite\n");
			void *buf = pop_arg(vm);
			u64 size = pop_arg(vm);
			u64 nmemb = pop_arg(vm);
			void *file = pop_arg(vm);
			push_arg(vm, fwrite(buf, size, nmemb, file));
			return;
			}
		case f_fseek:
			{
			if (debug)
				printf("fseek\n");
			void *file = pop_arg(vm);
			u64 offs = pop_arg(vm);
			u32 whence = pop_arg(vm);
			push_arg(vm, fseek(file, offs, whence));
			return;
			}
		case f_ftell:
			{
			if (debug)
				printf("ftell\n");
			void *file = pop_arg(vm);
			push_arg(vm, ftell(file));
			return;
			}
	}
}

void execute_function(struct VM *vm, struct Function *proc, struct BytecodeGen *bg) {
	struct Chunk *c = &proc->c;
	vm->ip = 0;
	if (proc->allocas) {
		vm->ch = proc->allocas;
		for (;;) {
			if (vm->ip >= proc->allocas->len) {
				break;
			}
			struct AllocaSI *inst = get_instr(vm);
			if (debug)
				printf("allocas: %d, %d\n", inst->reg, inst->len);
			set_reg(vm, inst->reg, get_reg(vm, r_sp) + get_reg(vm, r_fp));
			set_reg(vm, r_sp, get_reg(vm, r_sp) + inst->len);
			vm->ip += sizeof(struct AllocaSI);
		}
		vm->ip = 0;
	}
	vm->ch = c;
	for (;;) {
		set_reg(vm, 0, 0);
		if (debug) {
			for (int i = 0; i < c->dlen; i++) {
				if (vm->ch->debug[i].code_pos > vm->ip) {
					printf("line %d, ", vm->ch->debug[i-1].line_pos);
					break;
				} else if (i == vm->ch->dlen - 1) {
					printf("line %d, ", vm->ch->debug[i].line_pos);
					break;
				}
			}
			printf("%llx: ", vm->ip);
		}
		if (vm->ip > vm->ch->len) {
			break;
		}
		switch (vm->ch->code[vm->ip]) {
			case i_loaddu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loaddu: %llx to reg %d (%llu)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u64*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(u64*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadwu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadwu: %llx to reg %d (%u, %llx)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u32*)(inst->offset + get_reg(vm, inst->addr)), *(u32*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(u32*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadhu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadhu: %llx to reg %d (%u)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u16*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(u16*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadbu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadbu: %llx to reg %d (%u)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u8*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(u8*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storedu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storedu reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u64*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storewu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storewu reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u32*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storehu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storehu reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u16*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storebu:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storebu reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u8*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadd:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadd: %llx to reg %d (%lld)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u64*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(i64*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadw:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadw: %llx to reg %d (%d, %llx)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u32*)(inst->offset + get_reg(vm, inst->addr)), *(u32*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(i32*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadh:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadh: %llx to reg %d (%d)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u16*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(i16*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadb:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("loadb: %llx to reg %d (%d)\n", inst->offset + get_reg(vm, inst->addr), inst->reg, *(u8*)(inst->offset + get_reg(vm, inst->addr)));
					}
					set_reg(vm, inst->reg, *(i8*)(inst->offset + get_reg(vm, inst->addr)));
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_stored:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("stored reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u64*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storew:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storew reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u32*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storeh:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storeh reg %d in %llx\n", inst->reg, inst->offset + get_reg(vm, inst->addr));
					}
					*(u16*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storeb:
				{
					struct MemI *inst = get_instr(vm);
					if (debug) {
						printf("storeb reg %d in %d + %d (%llx)\n", inst->reg, inst->offset, inst->addr, get_reg(vm, inst->addr));
					}
					*(u8*)(inst->offset + get_reg(vm, inst->addr)) = get_reg(vm, inst->reg);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_addi:
				{
					struct AddImmediateI *inst = get_instr(vm);
					if (debug) {
						printf("addi %d %d (%lld) %lld\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->val);
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) + inst->val);
					vm->ip += sizeof(struct AddImmediateI);
					break;
				}
			case i_add:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("add %d %d (%lld) %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) + get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_sub:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("sub %d %d (%lld) %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) - get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mul:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("mul %d %d (%lld) %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) * get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_div:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("div %d %d (%lld) %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) / get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mod:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("mod %d %d (%lld) %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, get_reg(vm, inst->reg2) % get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("mulu %d %d (%llu) %d (%llu)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, (u64)get_reg(vm, inst->reg2) * (u64)get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("divu %d %d (%llu) %d (%llu)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, (u64)get_reg(vm, inst->reg2) / (u64)get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_modu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("modu %d %d (%llu) %d (%llu)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					set_reg(vm, inst->reg1, (u64)get_reg(vm, inst->reg2) % (u64)get_reg(vm, inst->reg3));
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_not:
				{
					struct SimpleI *inst = get_instr(vm);
					if (debug) {
						printf("not %d %d (%llu)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2));
					}
					set_reg(vm, inst->reg1, !get_reg(vm, inst->reg2));
					vm->ip += sizeof(struct SimpleI);
					break;
				}
			case i_addfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("addfd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, *(double*)&reg2, inst->reg3, *(double*)&reg3);
					}
					double res = *(double*)&reg2 + *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_subfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("subfd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, *(double*)&reg2, inst->reg3, *(double*)&reg3);
					}
					double res = *(double*)&reg2 - *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("mulfd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, *(double*)&reg2, inst->reg3, *(double*)&reg3);
					}
					double res = *(double*)&reg2 * *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("divfd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, *(double*)&reg2, inst->reg3, *(double*)&reg3);
					}
					double res = *(double*)&reg2 / *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_addfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("addfs %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, *(float*)&reg2, inst->reg3, *(float*)&reg3);
					}
					float res = *(float*)&reg2 + *(float*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_subfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("subfs %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, *(float*)&reg2, inst->reg3, *(float*)&reg3);
					}
					float res = *(float*)&reg2 - *(float*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("mulfs %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, *(float*)&reg2, inst->reg3, *(float*)&reg3);
					}
					float res = *(float*)&reg2 * *(float*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					if (debug) {
						printf("divfs %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, *(float*)&reg2, inst->reg3, *(float*)&reg3);
					}
					float res = *(float*)&reg2 / *(float*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			/*case i_modfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("modfd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					double res = *(double*)&reg2 % *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}*/ // might include fmod sometime
			case i_ftoi:
				{
					struct ConvI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					switch (inst->fmt1) {
						case ffmt_single:
							{
								float val = *(float*)&reg2;
								if (debug) {
									printf("ftoi %d %d (%f)\n", inst->reg1, inst->reg2, val);
								}
								set_reg(vm, inst->reg1, (u64)val);
							}
							break;
						case ffmt_double:
							{
								double val = *(double*)&reg2;
								if (debug) {
									printf("ftoi %d %d (%lf)\n", inst->reg1, inst->reg2, val);
								}
								set_reg(vm, inst->reg1, (u64)val);
							}
					}
					vm->ip += sizeof(struct ConvI);
					break;
				}
			case i_itof:
				{
					struct ConvI *inst = get_instr(vm);
					u64 reg2 = get_reg(vm, inst->reg2);
					switch (inst->fmt2) {
						case ffmt_single:
							{
							if (debug) {
								printf("itof %d %d (%lld)\n", inst->reg1, inst->reg2, reg2);
							}
							float val = (float)reg2;
							set_reg(vm, inst->reg1, *(u64*)&val);
							}
							break;
						case ffmt_double:
							if (debug) {
								printf("itof %d %d (%lld)\n", inst->reg1, inst->reg2, reg2);
							}
							double val = (double)reg2;
							set_reg(vm, inst->reg1, *(u64*)&val);
					}
					vm->ip += sizeof(struct ConvI);
					break;
				}
			case i_call:
				{
					struct CallI *inst = get_instr(vm);
					if (inst->func == f_bc_call) {
						handle_native_funcs(vm, inst->func);
					} else if (inst->func == f_call_native) {
						u64 offset = get_reg(vm, r_fp);
						// u64 lib_idx = *(u64*)offset;
						u64 lib_idx = pop_arg(vm);
						// offset += 8;
						u64 func_type = pop_arg(vm);
						// offset += 8;
						if (debug) {
							printf("call foreign %s from lib %s\n", (bg->flibs->cprocs[lib_idx][func_type])->name, bg->flibs->libname[lib_idx]);
						}
						struct FProc *cur = &vm->lcprocs[lib_idx][func_type];
						// void *libptr = vm->libs[lib_idx];
						void *fnptr = cur->fnptr;
						
						// setup values
						void **arg_values = cur->arg_values;
						for (int i = 0; i < cur->len; i++) {
							if (cur->arg_types[i]->type == FFI_TYPE_STRUCT) {
								// arg_values[i] = *(void**)offset;
								// arg_values[i] = *(void**)offset;
								arg_values[i] = *(void**)&vm->args[i + 2];
								// offset += sizeof(void*);
								continue;
							}
							// } else {
								arg_values[i] = (void*)&vm->args[i + 2];
							// }
							offset += cur->arg_types[i]->size;
						}


						offset = get_reg(vm, r_fp);
						
						void *return_pos;
						return_pos = vm->args;
						if (cur->return_type->type == FFI_TYPE_STRUCT) {
							return_pos = (void*)offset;
							push_arg(vm, offset);
						}
						vm->arg_pos = 0;
						ffi_call(&cur->cif, FFI_FN(fnptr), return_pos, arg_values);
					} else {
						if (debug)
							printf("call bc %s, %d\n", bg->functions[inst->func - f_last].name, inst->func - f_last);
						u64 registers[256];
						u64 ip = vm->ip;
						memcpy(registers, vm->regs, 256 * 8);
						execute_function(vm, &bg->functions[inst->func - f_last], bg);
						memcpy(vm->regs, registers, 256 * 8); // this is important
						vm->ch = c;
						vm->ip = ip;
					}
					vm->ip += sizeof(struct CallI);
					break;
				}
			case i_allocas:
				{
					struct AllocaSI *inst = get_instr(vm);
					if (debug)
						printf("allocas: %d, %d\n", inst->reg, inst->len);
					set_reg(vm, inst->reg, get_reg(vm, r_sp) + get_reg(vm, r_fp));
					set_reg(vm, r_sp, get_reg(vm, r_sp) + inst->len);
					vm->ip += sizeof(struct AllocaSI);
					break;
				}
			case i_allocad:
				{
					struct SimpleI *inst = get_instr(vm);
					if (debug)
						printf("allocad: %d, %d (%lld)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2));
					set_reg(vm, inst->reg1, get_reg(vm, r_sp) + get_reg(vm, r_fp));
					set_reg(vm, r_sp, get_reg(vm, r_sp) + get_reg(vm, inst->reg2));
					vm->ip += sizeof(struct SimpleI);
					break;
				}
			case i_aarg:
				{
					struct ArgI *inst = get_instr(vm);
					if (debug)
						printf("aarg: %d (%lld)\n", inst->reg, get_reg(vm, inst->reg));
					push_arg(vm, get_reg(vm, inst->reg));
					// vm->arg_len += 1;
					vm->ip += sizeof(struct ArgI);
					break;
				}
			case i_larg:
				{
					struct ArgI *inst = get_instr(vm);
					u64 arg = pop_arg(vm);
					if (debug)
						printf("larg: %d (%lld)\n", inst->reg, arg);
					set_reg(vm, inst->reg, arg);
					vm->ip += sizeof(struct ArgI);
					break;
				}
			case i_jmp:
				{
					struct JmpI *inst = get_instr(vm);
					if (debug)
						printf("jmp: %llx + %d (%d)\n", inst->addr, inst->reg, get_reg(vm, inst->reg));
					vm->ip = inst->addr + get_reg(vm, inst->reg);
					break;
				}
			case i_jmpr:
				{
					struct JmpRI *inst = get_instr(vm);
					if (debug)
						printf("jmpr: %llx\n", inst->addr);
					vm->ip += inst->addr;
					break;
				}
			case i_bne:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("bne: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if (get_reg(vm, inst->reg1) == get_reg(vm, inst->reg2)) {
						vm->ip += sizeof(struct BranchI);
					} else {
						vm->ip = inst->addr;
					}
					break;
				}
			case i_beq:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("beq: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if (get_reg(vm, inst->reg1) == get_reg(vm, inst->reg2)) {
						vm->ip = inst->addr;
					} else {
						vm->ip += sizeof(struct BranchI);
					}
					break;
				}
			case i_blt:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("blt: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if ((i64)get_reg(vm, inst->reg1) < (i64)get_reg(vm, inst->reg2)) {
						vm->ip = inst->addr;
					} else {
						vm->ip += sizeof(struct BranchI);
					}
					break;
				}
			case i_bltu:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("bltu: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if (get_reg(vm, inst->reg1) < get_reg(vm, inst->reg2)) {
						vm->ip = inst->addr;
					} else {
						vm->ip += sizeof(struct BranchI);
					}
					break;
				}
			case i_bge:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("bge: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if ((i64)get_reg(vm, inst->reg1) >= (i64)get_reg(vm, inst->reg2)) {
						vm->ip = inst->addr;
					} else {
						vm->ip += sizeof(struct BranchI);
					}
					break;
				}
			case i_bgeu:
				{
					struct BranchI *inst = get_instr(vm);
					if (debug)
						printf("bgeu: %d, %d, %llx\n", inst->reg1, inst->reg2, inst->addr);
					if (get_reg(vm, inst->reg1) >= get_reg(vm, inst->reg2)) {
						vm->ip = inst->addr;
					} else {
						vm->ip += sizeof(struct BranchI);
					}
					break;
				}
			case i_flts:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("flts %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(float*)&reg2 < *(float*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fges:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("fges %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(float*)&reg2 >= *(float*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_feqs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("flts %d %d (%f) %d (%f)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(float*)&reg2 == *(float*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fltd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("fltd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(double*)&reg2 < *(double*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fged:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("fged %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(double*)&reg2 >= *(double*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_feqd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					if (debug) {
						printf("feqd %d %d (%lf) %d (%lf)\n", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					}
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					set_reg(vm, inst->reg1, *(double*)&reg2 == *(double*)&reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_ret:
				{
					// struct RetI *inst = get_instr(vm);
					// TBD
					if (debug)
						printf("ret\n");
					vm->ip += sizeof(struct RetI);
					return;
				}
			case i_loadrd:
				{
					struct LoadRDI *inst = get_instr(vm);
					if (debug)
						printf("load rodata: %d, %llx\n", inst->reg, &bg->rodata.code[inst->addr]);
					set_reg(vm, inst->reg, (i64)&bg->rodata.code[inst->addr]);
					vm->ip += sizeof(struct LoadRDI);
					break;
				}
				/*
			case i_takep:
				{
					struct TakePointerI *inst = get_instr(vm);
					if (debug)
						printf("take pointer: %d, %llx\n", inst->reg, &vm->regs[inst->reg]);
					push(vm, (u64)&vm->regs[inst->reg]);
					vm->ip += sizeof(struct TakePointerI);
					break;
				}
				*/
			default:
				printf("invalid instruction: %i\n");
				segexit();
		}
	}
}

void init_vm(struct VM *vm, struct BytecodeGen *bg) {
	vm->sp = 8;
	vm->regs[r_zero] = 0;
	vm->regs[r_ip] = 0;
	vm->regs[r_fp] = (u64)&vm->stack[0];
	vm->regs[r_sp] = 0;
	vm->arg_len = 0;
	vm->arg_pos = 0;

	// init libs
	#ifndef _WIN32
	struct ForeignLib *flibs = bg->flibs;
	vm->libs = malloc(sizeof(void*) * flibs->liblen);
	char *balls = malloc(1);
	vm->lcprocs = malloc(sizeof(void*) * flibs->liblen);
	for (int i = 0; i < flibs->liblen; i++) {

		balls = realloc(balls, strlen(bg->flibs->libname[i]) + 4);
		sprintf(balls, "%s.so", bg->flibs->libname[i]);
		if (!strcmp(bg->flibs->libname[i], "libc")) {
			vm->libs[i] = RTLD_NEXT;
		} else {
			vm->libs[i] = dlopen(balls, RTLD_LAZY);
			if (!vm->libs[i]) {
				printf("library %s could not be found\n", flibs->libname[i]);
				segexit();
			}
		}
		vm->lcprocs[i] = malloc(sizeof(struct FProc) * flibs->cslen[i]);

		// init funcs
		for (int y = 0; y < flibs->cslen[i]; y++) {
			struct FProc *cur_lfproc = &vm->lcprocs[i][y];
			struct ProcInfo *cproc = bg->flibs->cprocs[i][y]->proc_info;

			cur_lfproc->fnptr = dlsym(vm->libs[i], (bg->flibs->cprocs[i][y])->name);
			if (!cur_lfproc->fnptr) {
				printf("function %s could not be loaded from library %s\n", (bg->flibs->cprocs[i][y])->name, flibs->libname[i]);
				segexit();
			}
			cur_lfproc->len = cproc->len;
			cur_lfproc->arg_types = malloc(sizeof(ffi_type) * cproc->len);
			cur_lfproc->arg_values = malloc(sizeof(void*) * cproc->len);
			
			ffi_status status;
			
			struct Typeinfo *cur_type;
			for (int i = 0; i < cproc->len; i++) {
				cur_type = cproc->types[i];
				cur_lfproc->arg_types[i] = to_ffi_type(cur_type);
			}
			
			
			ffi_arg result;
			ffi_type *return_type;
			if (cproc->return_val) {
				cur_lfproc->return_type = to_ffi_type(cproc->return_val);
			} else {
				cur_lfproc->return_type = &ffi_type_void;
			}

			status = ffi_prep_cif(&cur_lfproc->cif, FFI_DEFAULT_ABI, cproc->len, cur_lfproc->return_type, cur_lfproc->arg_types);

			if (status != FFI_OK) {
				printf("something is off\n");
			}
		}
	}
	free(balls);
	#endif
}

void execute(struct BytecodeGen *bg) {
	struct VM vm;
	init_vm(&vm, bg);
	execute_function(&vm, &bg->functions[0], bg);
	exit(vm.args[0]);
}

#include "disasm.c"

int main(int argc, char **argv) {
	int arg = 1;
	int disasm = 0;
	char *file;
	if (argc > 1) {
		if (!strcmp("-debug", argv[1])) {
			debug = 1;
			arg += 1;
		}
		if (!strcmp("-disasm", argv[1])) {
			disasm = 1;
			arg += 1;
		}
		if (argc != arg) {
			file = argv[arg];
		} else {
			printf("no input file to run given; exiting\n");
			segexit();
		}
	} else {
		printf("no input file to run given; exiting\n");
		segexit();
	}
	struct BytecodeData data = deserialize_bc_file(file);
	struct BytecodeGen bg;
	bg.flen = data.flen;
	bg.functions = data.procs;
	bg.flibs = &data.flibs;
	bg.rodata = data.rodata;
	if (!disasm) {
		execute(&bg);
	} else {
		for (int i = 0; i < bg.flen; i++) {
			printf("func: %s\n", bg.functions[i].name);
			disasm_func(&bg, &bg.functions[i]);
			printf("\n\n", bg.functions[i].name);
		}
	}
}
