#include "typeinfo.h"

enum Operator {
	// mathematic
	op_plus,
	op_minus,
	op_mult,
	op_div,
	op_mod,
	// comparison
	op_eq,
	op_neq,
	op_lt,
	op_gt,
	op_lt_eq,
	op_gt_eq,
	// logical
	op_log_and,
	op_log_or,
	// assignment
	op_assign,
	op_decl,
	op_def,
	op_const,
	op_plus_a,
	op_minus_a,
	op_mult_a,
	op_div_a,
	op_mod_a,
	//range
	op_range,
	// subscript
	op_subscript,
	// slice
	op_slice,
	//cast
	op_cast,
	// access struct member
	op_access,
	// singleops
	op_not,
	op_take_pointer,
	op_ptr_type,
	op_deref,
	op_paren,
};

struct Literal {
	char type; // -1 = init to zero, 0 = u64, 1 = float, 2 = string, 3 = bool, 4 = enum, 5 = char, 6 = array
	union {
		char *string;
		u64 integer;
		f64 fl;
	};
};

struct Range {
	struct AstNode *low;
	struct AstNode *high;
};

enum DirectiveType {
	dir_include,
};

struct Directive {
	enum DirectiveType type;
	struct AstNode *on;
};

enum AstType {
	ast_block,
	ast_foreign,
	ast_proc_call,
	ast_proc_hdr,
	ast_cproc_hdr,
	ast_proc,
	ast_ident,
	ast_literal,
	ast_ptr_literal,
	ast_binary,
	ast_unary,
	ast_module_stmt,
	ast_module,
	ast_import,
	ast_include,
	ast_arr_lit,
	ast_array,
	ast_for,
	ast_loop,
	ast_if,
	ast_while,
	ast_match,
	ast_struct,
	ast_enum,
	ast_directive,
	ast_type,
	ast_cflow,
	ast_no_init,
};

struct AstNode {
	enum AstType type;
	u64 l0;
	u64 c0;
	u64 l1;
	u64 c1;
	struct Typeinfo *info;
	void *node;
};

struct AstRoot {
	u64 len;
	struct AstNode *nodes;
	struct Scope *scope;
};

struct AstForeign {
	struct AstRoot *block;
	char *libname;
};

struct AstModule {
	char *name;
	int len;
	int max;
	struct Scope *scope;
	struct AstRoot **global_scope;
	struct Tokenizer *ts;
	char *path;
	int tlen;
	int tmax;
	struct Typeinfo **types; // idk i might need that sometime
};

struct Scope {
	int ilen;
	int imax;
	char **idents;
	struct Typeinfo **itypes;
};

struct CFlow {
	int type; // 0 = break, 1 = return
	struct AstNode *on;
};

struct ProcHeader {
	struct AstRoot *args;
	struct AstNode *return_val;
};

struct Proc {
	struct ProcHeader *header;
	struct AstRoot *body;
};

struct BinaryOp {
	enum Operator op;
	struct AstNode *left;
	struct AstNode *right;
};

struct UnaryOp {
	enum Operator op;
	struct AstNode *on;
};

struct ProcCall {
	char *name;
	struct AstRoot *args;
};

struct ModDef {
	char *name;
};

struct Import {
	char *name;
};

struct AstFor {
	char *var;
	struct AstNode *iterator;
	struct AstRoot *block;
};

struct AstWhile {
	struct AstNode *cond;
	struct AstRoot *block;
};

struct AstIf {
	struct AstNode *cond;
	struct AstRoot *block;
	struct AstNode *else_block;
};

struct Match {
	struct AstNode *on;
	u64 len;
	struct AstNode **conds;
	struct AstRoot **blocks;
};

struct PointerLit {
	u64 pointer_level;
	struct AstNode *on;
};

enum ArrayType {
	arrayt_dynamic,
	arrayt_stack,
	arrayt_static,
};

struct ArrayLit {
	u32 type;
	u32 capacity;
	struct AstNode *on;
};

enum Keyword {
	kw_none,
	kw_break,
	kw_for,
	kw_if,
	kw_struct,
};

struct Parser {
	u64 pos;
	u64 token_len;
	u64 first;
	struct Token *tokens;
	struct Tokenizer *t;
	int panic;
};

