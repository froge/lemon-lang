#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

int debug = 1;
int timings = 1;
int disasm = 0;

#include "common.h"
#include "segexit.c"
#include "frontend/tokenizer.c"
#include "frontend/parser.c"
#include "vm/bytecode.h"
// #include "vm/vm.c"
// #include "vm/disasm.c"
#include "frontend/typecheck.c"
// #include "backend/cbackend.c"
#include "backend/bytecode.c"
#include "bc_file.c"



#if !defined(_WIN32)
/// Convert seconds to milliseconds
#define SEC_TO_MS(sec) ((sec)*1000)
/// Convert seconds to microseconds
#define SEC_TO_US(sec) ((sec)*1000000)
/// Convert seconds to nanoseconds
#define SEC_TO_NS(sec) ((sec)*1000000000)

/// Convert nanoseconds to seconds
#define NS_TO_SEC(ns)   ((ns)/1000000000)
/// Convert nanoseconds to milliseconds
#define NS_TO_MS(ns)    ((ns)/1000000)
/// Convert nanoseconds to microseconds
#define NS_TO_US(ns)    ((ns)/1000)

/// Get a time stamp in milliseconds.
uint64_t millis()
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    uint64_t ms = SEC_TO_MS((uint64_t)ts.tv_sec) + NS_TO_MS((uint64_t)ts.tv_nsec);
    return ms;
}

/// Get a time stamp in microseconds.
uint64_t micros()
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    uint64_t us = SEC_TO_US((uint64_t)ts.tv_sec) + NS_TO_US((uint64_t)ts.tv_nsec);
    return us;
}

/// Get a time stamp in nanoseconds.
uint64_t nanos()
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    uint64_t ns = SEC_TO_NS((uint64_t)ts.tv_sec) + (uint64_t)ts.tv_nsec;
    return ns;
}

// NB: for all 3 timestamp functions above: gcc defines the type of the internal
// `tv_sec` seconds value inside the `struct timespec`, which is used
// internally in these functions, as a signed `long int`. For architectures
// where `long int` is 64 bits, that means it will have undefined
// (signed) overflow in 2^64 sec = 5.8455 x 10^11 years. For architectures
// where this type is 32 bits, it will occur in 2^32 sec = 136 years. If the
// implementation-defined epoch for the timespec is 1970, then your program
// could have undefined behavior signed time rollover in as little as
// 136 years - (year 2021 - year 1970) = 136 - 51 = 85 years. If the epoch
// was 1900 then it could be as short as 136 - (2021 - 1900) = 136 - 121 =
// 15 years. Hopefully your program won't need to run that long. :). To see,
// by inspection, what your system's epoch is, simply print out a timestamp and
// calculate how far back a timestamp of 0 would have occurred. Ex: convert
// the timestamp to years and subtract that number of years from the present
// year.
//
#endif

void get_base_dir(char *in) {
	int pos = -1;
	for (int i = strlen(in) - 1; i != 0; i--) {
		if (in[i] == '/') {
			pos = i;
			break;
		}
	}
	if (pos == -1) {
		return;
	}
	in[pos] = '\0';
}

struct StringArr {
	u32 len;
	u32 max;
	char **strings;
};

void handle_includes(struct Tokenizer **tarr, struct AstRoot **sarr, struct StringArr *files, u64 *len, u64 *max, u64 index, u32 ilen, char **incdirs) {
	struct AstRoot *cur = &sarr[0][index];
	for (int i = 0; i < cur->len; i++) {
		if (cur->nodes[i].type == ast_include) {
			if (*len == *max - 1) {
				*max += 20;
				*tarr = realloc(*tarr, sizeof(struct Tokenizer) * *max);
				*sarr = realloc(*sarr, sizeof(struct AstRoot) * *max);
			}
			cur->nodes[i].type = -1;
			char dir[255]; // bad becuase its not relative and following symlinks therefore kills everything
			getcwd(dir, 255);
			char *dir2 = dir;
			u32 balls = strlen(cur->nodes[i].node);
			char *temp = malloc(balls + 1);
			memcpy(temp, cur->nodes[i].node, balls + 1);
			get_base_dir(temp);
			struct stat st;
			int found = 0;
			if (stat(cur->nodes[i].node, &st)) {
				found = 0;
				for (int y = 0; y < ilen; y++) {
					chdir(incdirs[y]);
					// found = !stat(cur->nodes[i].node, &st);
					if (!stat(cur->nodes[i].node, &st)) {
						found = 1;
						char dir3[255];
						getcwd(dir3, 255);
						dir2 = dir3;
					}
				}
				if (!found) {
					printf("%s:%d:%d: ", tarr[0][index].filename, cur->nodes[i].l0, cur->nodes[i].c0);
					printf("ERROR: file \"%s\" doesn't exist\n", cur->nodes[i].node);
					chdir(dir);
					segexit();
				}
			}
			if (files->len == files->max - 1) {
				files->max += 20;
				files->strings = realloc(files->strings, files->max * sizeof(char*));
			}
			found = 0;
			for (u32 y = 0; y < files->len; y++) {
				if (!memcmp(files->strings[y], dir2, strlen(dir2))) {
					if (!memcmp(files->strings[y] + strlen(dir) + 1, cur->nodes[i].node, balls)) {
						found = 1;
						break;
					}
				}
			}
			if (found) {
				chdir(dir);
				return;
			} else {
				u32 fart = strlen(dir2);
				files->strings[files->len] = malloc(fart + balls + 2);
				memcpy(files->strings[files->len], dir2, fart);
				files->strings[files->len][fart] = '/'; // TODO this assumes its a relative path (why would you ever make it absolute??)
				memcpy(files->strings[files->len] + fart + 1, cur->nodes[i].node, balls + 1);
				printf("%s\n", files->strings[files->len]);
				files->len += 1;
			}
			tarr[0][*len] = *scan_file(cur->nodes[i].node);
			sarr[0][*len] = *parse(&tarr[0][*len]);
			*len += 1;
			chdir(temp);
			handle_includes(tarr, sarr, files, len, max, *len - 1, ilen, incdirs);
			chdir(dir);
		}
	}
}

void write_string(char *filename, char *text) {
  void *file = fopen(filename, "wb");
  u64 len = 0;
  fwrite(text, 1, strlen(text), file);
	fclose(file);
}


int main(int argc, char **argv) {
	//TODO optimize all this crap
    unsigned long long time_before, time_after;
#if !defined(_WIN32)
    time_before = micros();
#endif
	u64 max = 20;
	u64 len = 0;
	struct Tokenizer *tarr = malloc(sizeof(struct Tokenizer) * max);
	struct AstRoot *sarr = malloc(sizeof(struct AstRoot) * max);
	char dir[100];
	getcwd(dir, 100);
	struct StringArr ball;
	ball.max = 20;
	ball.len = 0;
	ball.strings = malloc(sizeof(char*) * ball.max);
	u32 ilen = 1;
	char *pref = getenv("HOME");
	char *loc = "/.local/share/lemstd";
	char *incdir = malloc(strlen(pref) + strlen(loc) + 1);
	strcpy(incdir, pref);
	strcpy(incdir + strlen(pref), loc);
	*(incdir + strlen(pref) + strlen(loc)) = 0;
	char **incdirs = &incdir;
	for (int i = 0; i < argc - 1; i++) {
		tarr[i] = *scan_file(argv[i+1]);
		sarr[i] = *parse(&tarr[i]);
		char *temp = malloc(strlen(argv[i+1]) + 1);
		memcpy(temp, argv[i+1], strlen(argv[i+1]) + 1);
		get_base_dir(temp);
		chdir(temp);
		len += 1;
		handle_includes(&tarr, &sarr, &ball, &len, &max, 0, ilen, incdirs);
		// leak temp
	}
	chdir(dir);
	char **strs = malloc(max * sizeof(char*));
	for (int i = 0; i < len; i++) {
		struct AstRoot *cur = &sarr[i];
		for (int y = 0; y < cur->len; y++) {
			if (cur->nodes[y].type == ast_module_stmt) {
				strs[i] = cur->nodes[y].node;
				break;
			}
		}
	}
	int mod_max = 20;
	int mod_len = 0;
	//TODO support more than 20 modules with 20 files
	struct AstModule *mods = calloc(1, mod_max * sizeof(struct AstModule));
	for (int i = 0; i < len; i++) {
		// char *cur = strs[i];
		int found = 0;
		for (int y = 0; y < mod_len; y++) {
			if (!strcmp(strs[i], mods[y].name)) {
				found = 1;
				mods[y].global_scope[mods[y].len] = &sarr[i];
				mods[y].ts[mods[y].len] = tarr[i];
				mods[y].len++;
				break;
			}
		}
		if (!found) {
			mods[mod_len].name = strs[i];
			mods[mod_len].len = 1;
			mods[mod_len].max = 20;
			mods[mod_len].global_scope = malloc(mods[mod_len].max * sizeof(struct AstRoot));
			mods[mod_len].global_scope[0] = &sarr[i];
			mods[mod_len].ts = malloc(mods[mod_len].max * sizeof(struct Tokenizer));
			mods[mod_len].ts[0] = tarr[i];
			mod_len++;
		}
	}
	for (int i = 0; i < mod_len; i++) {
		struct AstModule *cur = &mods[i];
		for (int x = 0; x < cur->len; x++) {
			struct AstRoot *curr = cur->global_scope[x];
			for (int y = 0; y < curr->len; y++) {
				if (curr->nodes[y].type == ast_import) {
					for (int m = 0; m < mod_len; m++) {
						if (!strcmp(mods[m].name, curr->nodes[y].node)) {
							if (cur == &mods[m]) {
								printf("ERROR: can't import itself\n");
								segexit();
							}
							curr->nodes[y].type = ast_module;
							curr->nodes[y].node = &mods[m];
						}
					}
				}
			}
		}
	}
	for (int i = 0; i < mod_len; i++) {
		if (debug)
			printf("%i: %s, len: %i\n", i, mods[i].name, mods[i].len);
	}
	struct AstModule *mainmod = 0;
	for (int i = 0; i < mod_len; i++) {
		if (mods[i].name && !strcmp(mods[i].name, "main")) {
			mainmod = &mods[i];
		}
	}
	if (!mainmod) {
		printf("ERROR: no main module, exiting\n");
		segexit();
	}
	struct AstNode modt;
	modt.type = ast_module;
	modt.node = mainmod;
	//print_node(&modt);
	struct Typechecker *t = typecheck(mainmod);
	// if (*t->error) {
	// 	segexit();
	// }
	struct BytecodeGen *bg = generate_bytecode(t, mainmod);
#if !defined(_WIN32)
    time_after = micros();
#endif
#if !defined(_WIN32)
	if (timings)
		printf("%lld\n", time_after - time_before);
#endif
	//disasm(&bg->functions[0].c);
#if !defined(_WIN32)
    time_before = micros();
#endif
	if (!disasm) {
		// execute(bg);
		generate_bc_file(bg, "fard.lbc");
	} else {
		// for (int i = 0; i < bg->flen; i++) {
		// 	printf("func: %s\n", bg->functions[i].name);
		// 	disasm_func(bg, &bg->functions[i].c);
		// 	printf("\n\n", bg->functions[i].name);
		// }
	}
#if !defined(_WIN32)
    time_after = micros();
#endif
#if !defined(_WIN32)
	if (timings)
		printf("%lld\n", time_after - time_before);
#endif
	//write_string("test.c", cg->res);
	//printf("%s\n", cg->res);
}
