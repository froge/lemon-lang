all:
	# gcc src/main.c -o lang -I. -O1 -ggdb -lffi
	clang src/main.c -std=c11 -o lang -I. -Isrc -Wno-switch -Wno-format -glldb
	clang vm/vm.c -std=c11 -lffi -o lemvm -I. -Isrc -Wno-switch -Wno-format -glldb -Wno-int-conversion
	#clang --target=wasm32-unknown-wasi --sysroot=/usr/share/wasi-sysroot src/main.c -o lang
	#clang src/main.c -o lang #-O3
	
debug:
	./lang test.lmn
	./lemvm -debug fard.lbc

run: all
	./lang test.lmn
	./lemvm fard.lbc

test: all
	cd tests; sh test.sh

win:
	clang src/main.c -std=c11 -o lang -I. -Isrc -Wno-switch -Wno-format -glldb -target x86_64-windows-gnu
	clang vm/vm.c -std=c11 -lffi -o lemvm -I. -Isrc -Wno-switch -Wno-format -glldb -Wno-int-conversion -target x86_64-windows-gnu

gdb: all
	lldb ./lang -- test.lmn
