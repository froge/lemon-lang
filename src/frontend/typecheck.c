#include "sizes.c"

struct Typechecker {
	struct Tokenizer *t;
	struct AstModule *cur_module;
	struct Typeinfo *cur_proc;
	int offset;
	int scopes_len;
	int scopes_max;
	int cur_foreign_idx;
	struct Scope **scopes;
	struct AstNode *cur_node;
	struct ForeignLib *flibs;
	int *error;
};

struct Typeinfo *check_scope(struct Typechecker *t, struct AstRoot *block);
struct Typeinfo *check_expr(struct Typechecker *t, struct AstNode *node);
int type_eq(struct Typeinfo *a, struct Typeinfo *b);
void check_foreign_scope(struct Typechecker *t, struct AstRoot *block);
void check_proc_scope(struct Typechecker *t, struct AstRoot *block, struct Typeinfo *info);
void check_for_scope(struct Typechecker *t, struct AstRoot *block, struct AstFor *af);
struct Typeinfo *default_literal(struct Typechecker *t, struct Typeinfo *i);
struct Typeinfo *check_module(struct Typechecker *t, struct AstModule *mod);

/*
enum ValueType {
	vtype_none,
	vtype_int,
	vtype_float,
	vtype_string,
	vtype_array,
	vtype_struct,
	vtype_enum,
};

struct Value {
	int type;
	union {
		int integer;
		float fl;
		char *string;
		struct Value *arr;
	};
};
*/


void error(struct Typechecker *tc, struct AstNode *node) {
	*tc->error += 1;
	printf("\nat: %i:%i\n", node->l0, node->c0);
	struct Tokenizer *t = tc->t;
	int curl = 1;
	int i = 0;
	int tk = 0;
	int s = 0;
	char curc;
	for (; i < t->len; i++) {
		curc = t->string[i];
		if (tk) {
			if (curc == '\n') {
				break;
			}
		} else {
			if (curc == '\n') {
				curl++;
			}
			if (curl == node->l0) {
				s = i;
				tk = 1;
			}
		}
	}
	int tabs = 0;
	for (int b = s; b < i; b++) {
		if (t->string[b] == '\t') {
			tabs++;
		}
		if (t->string[b] != '\n') {
			printf("%c", t->string[b]);
		}
	}
	printf("\n");
	for (i = 0; i < tabs; i++) {
		printf("\t");
	}
	if (node->c0) {
		for (i = 0; i < node->c0 - tabs; i++) {
			printf(" ");
		}
	}
	printf("^\n");
}

void add_scope(struct Typechecker *t, struct Scope *scope) {
	if (t->scopes_len == t->scopes_max) {
		t->scopes_max += 100;
		t->scopes = realloc(t->scopes, sizeof(struct Scope*) * t->scopes_max);
	}
	t->scopes[t->scopes_len] = scope;
	t->scopes_len++;
}

void delete_scope(struct Typechecker *t) {
	t->scopes_len--;
}

struct Typeinfo *find_type_module_nocheck(struct Typechecker *t, struct AstModule *mod, char *ident) {
	int len = strlen(ident);
	for (int i = 0; i < mod->tlen; i++) {
		if (len == strlen(mod->types[i]->name)) {
			if (!memcmp(ident, mod->types[i]->name, len)) {
				return mod->types[i];
			}
		}
	}
	return 0;
}

struct Typeinfo *find_type_module(struct Typechecker *t, struct AstModule *mod, char *ident) {
	int len = strlen(ident);
	for (int i = 0; i < mod->tlen; i++) {
		if (len == strlen(mod->types[i]->name)) {
			if (!memcmp(ident, mod->types[i]->name, len)) {
				if (mod->types[i]->type == t_pending) {
					struct Typechecker tempt;
					tempt.t = t->t; // @Fixup wrong tokenizer used
					tempt.error = t->error;
					tempt.scopes_len = 0;
					tempt.scopes_max = 20;
					tempt.scopes = malloc(tempt.scopes_max * sizeof(struct Scope*));
					tempt.scopes[0] = t->scopes[0];
					tempt.offset = 0;
					tempt.error = t->error;
					tempt.cur_module = mod;
					struct AstNode *node = mod->types[i]->node;

					if (mod->types[i]->proc_info) {
						tempt.cur_foreign_idx = (u64)mod->types[i]->proc_info - 1;
					}
					memcpy(mod->types[i], check_expr(&tempt, mod->types[i]->node), sizeof(struct Typeinfo));
					//mod->types[i] = check_expr(t, mod->types[i]->node); // @Fixup @Speedup @Memory uses twice as much mem as needed
					mod->types[i]->node = node;
				}
				return mod->types[i];
			}
		}
	}
	return 0;
}

struct Typeinfo *find_type(struct Typechecker *t, char *ident) {
	return find_type_module(t, t->cur_module, ident);
}

int check_type_exists_module(struct Typechecker *t, struct AstModule *mod, char *ident) {
	int len = strlen(ident);
	for (int i = 0; i < mod->tlen; i++) {
		if (len == strlen(mod->types[i]->name)) {
			if (!memcmp(ident, mod->types[i]->name, len)) {
				return 1;
			}
		}
	}
	return 0;
}

int check_type_exists(struct Typechecker *t, char *ident) {
	return check_type_exists_module(t, t->cur_module, ident);
}

struct Typeinfo *find_ident(struct Typechecker *t, char *ident) {
	int len = strlen(ident);
	struct Scope *cs;
	for (int y = t->offset; y < t->scopes_len; y++) {
		cs = t->scopes[y];
		for (int i = 0; i < cs->ilen; i++) {
			if (len == strlen(cs->idents[i])) {
				if (!memcmp(ident, cs->idents[i], len)) {
					return cs->itypes[i];
				}
			}
		}
	}
	return 0;
}

int add_type_module(struct Typechecker *t, struct AstModule *mod, struct Typeinfo *info) {
	if (check_type_exists_module(t, mod, info->name)) {
		printf("double definition of type");
		return 1;
	}
	if (mod->tlen == mod->tmax) {
		mod->tmax += 5;
		mod->types = realloc(mod->types, mod->tmax * sizeof(struct Typeinfo*));
	}
	mod->types[mod->tlen] = info;
	mod->tlen++;
	return 0;
}

int add_type(struct Typechecker *t, struct Typeinfo *type) {
	return add_type_module(t, t->cur_module, type);
}

int add_ident(struct Typechecker *t, char *ident, struct Typeinfo *type) {
	struct Scope *cs = t->scopes[t->scopes_len-1];
	if (find_ident(t, ident)) {
		printf("double definition of variable");
		return 1;
	}
	if (check_type_exists(t, ident)) {
		printf("variable cant have same name as type");
		return 1;
	}
	if (cs->ilen == cs->imax - 1) {
		cs->imax += 20;
		cs->idents = realloc(cs->idents, cs->imax * sizeof(char*));
		cs->itypes = realloc(cs->itypes, cs->imax * sizeof(struct Typeinfo*));
	}
	cs->idents[cs->ilen] = ident;
	cs->itypes[cs->ilen] = type;
	cs->ilen++;
	return 0;
}

void setup_internal_types(struct Typechecker *t) {
	struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 1;
	info->isigned = 1;
	info->name = "i8";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 2;
	info->isigned = 1;
	info->name = "i16";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 4;
	info->isigned = 1;
	info->name = "i32";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 8;
	info->isigned = 1;
	info->name = "i64";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 1;
	info->isigned = 0;
	info->name = "u8";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 2;
	info->isigned = 0;
	info->name = "u16";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 4;
	info->isigned = 0;
	info->name = "u32";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 8;
	info->isigned = 0;
	info->name = "u64";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_float;
	info->pointer_level = 0;
	info->size = 4;
	info->name = "f32";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_float;
	info->pointer_level = 0;
	info->size = 8;
	info->name = "f64";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 1;
	info->isigned = 0;
	info->name = "char";
	add_type(t, info);
	/*
	struct ArrayInfo *ai = malloc(sizeof(struct ArrayInfo));
	ai->dynamic = 0;
	ai->capacity = 0;
	ai->type = info;
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_array;
	info->pointer_level = 0;
	info->size = 16;
	info->isigned = 0;
	info->name = "string";
	info->array_info = ai;
	add_type(t, info);
	*/
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 1;
	info->isigned = 0;
	info->name = "bool";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 4;
	info->isigned = 1;
	info->name = "int";
	add_type(t, info);
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_int;
	info->pointer_level = 0;
	info->size = 0;
	info->isigned = 0;
	info->name = "void";
	add_type(t, info);
	struct ProcInfo *pi = calloc(1, sizeof(struct ProcInfo));
	pi->return_val = find_type(t, "u64");
	pi->len = 0;
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_proc;
	info->pointer_level = 0;
	info->size = 0;
	info->name = "___bytecode_call";
	info->proc_info = pi;
	add_type(t, info);
	/*
	info = malloc(sizeof(struct Typeinfo));
	info->type = t_any;
	info->pointer_level = 0;
	info->size = 0;
	info->isigned = 0;
	info->name = "any";
	add_type(t, info);
	*/
}

struct Typeinfo *check_proc_call(struct Typechecker *t, struct ProcCall *pc) {
	struct Typeinfo *pi = find_type(t, pc->name);
	if (!pi) {
		printf("proc %s doesn't exist", pc->name);
		error(t, t->cur_node);
		return 0;
	}
	if (pi->type != t_proc) {
		printf("can't call non-proc");
		error(t, t->cur_node);
		return 0;
	}
	if (!strcmp(pc->name, "___bytecode_call")) {
		for (int i = 0; i < pc->args->len; i++) {
			// TODO: check node types in parser
			// struct Typeinfo *temp = check_expr(t, &pc->args->nodes[i]);
			check_expr(t, &pc->args->nodes[i]);
		}
		return find_type(t, "u64"); // @Wordsize
	}
	if (pc->args->len != pi->proc_info->len) {
		if (pc->args->len > pi->proc_info->len) {
			printf("too many arguments given");
			error(t, t->cur_node);
			return 0;
		} else if (pc->args->len < pi->proc_info->len) {
			printf("too few arguments given");
			error(t, t->cur_node);
			return 0;
		}
	}
	for (int i = 0; i < pc->args->len; i++) {
		// TODO: check node types in parser
		struct Typeinfo *temp = check_expr(t, &pc->args->nodes[i]);

		if (pc->args->nodes[i].info->type == t_enum_literal) {
			pc->args->nodes[i].info = pi->proc_info->types[i];
		}
		if (!type_eq(temp, pi->proc_info->types[i])) {
			// TODO: make the errors not kill the compiler
			printf("wrong argument type used: %s", temp->name);
			error(t, &pc->args->nodes[i]);
			printf("function requires %s\n", pi->proc_info->types[i]->name);
			segexit();
		}
	}
	return pi->proc_info->return_val;
}

struct Typeinfo *check_type(struct Typechecker *t, struct AstNode *node, int emit_error) {
	t->cur_node = node;
	switch (node->type) {
		case ast_arr_lit:
			{
				struct ArrayLit *arr = node->node;
				struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
				// struct Typeinfo *temp = malloc(sizeof(struct Typeinfo));
				info->type = t_array;
				info->size = 0;
				info->pointer_level = 0; // why was this 1??
				struct ArrayInfo *ai = malloc(sizeof(struct ArrayInfo));
				info->array_info = ai;
				ai->type = check_type(t, arr->on, emit_error);
				ai->atype = arr->type;
				ai->capacity = arr->capacity;
				int namelen;
				int stlen;
				switch (ai->atype) {
					case arrayt_dynamic:
						{
							// i'm very lazy
							int len = snprintf(NULL, 0, "[..]%s", ai->type->name);
							info->name = malloc(len + 1);
							info->name[len] = 0;
							snprintf(info->name, len + 1, "[..]%s", ai->type->name);
							break;
						}
					case arrayt_stack:
						{
							int len = snprintf(NULL, 0, "[%d]%s", ai->capacity, ai->type->name);
							info->name = malloc(len + 1);
							info->name[len] = 0;
							snprintf(info->name, len + 1, "[%d]%s", ai->capacity, ai->type->name);
							break;
						}
					case arrayt_static:
						{
							int len = snprintf(NULL, 0, "[]%s", ai->type->name);
							info->name = malloc(len + 1);
							info->name[len] = 0;
							snprintf(info->name, len + 1, "[]%s", ai->type->name);
							break;
						}
				}
				node->info = info;
				return info;
			}
			case ast_enum:
			{
				struct AstRoot *e = node->node;
				struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
				struct EnumInfo *ei = malloc(sizeof(struct EnumInfo));
				info->type = t_enum;
				info->size = 4;
				info->pointer_level = 0;
				info->enum_info = ei;
				ei->names = malloc(sizeof(char*) * e->len);
				ei->values = malloc(sizeof(int) * e->len);
				ei->len = e->len;
				int cur = 0;
				struct BinaryOp *the_sus;
				for (int i = 0; i < e->len; i++) {
					switch (e->nodes[i].type) {
						case ast_binary:
							the_sus = e->nodes[i].node;
							ei->names[i] = (char*)the_sus->left->node;
							if (((struct Literal*)the_sus->right->node)->type != 0) {
								printf("can't assign non-integer to enum");
								error(t, node);
								segexit();
							}
							cur = (int)((struct Literal*)the_sus->right->node)->integer;
							ei->values[i] = cur;
							cur++;
							break;
						case ast_ident:
							ei->names[i] = (char*)e->nodes[i].node;
							ei->values[i] = cur;
							cur++;
							break;
					}
				}
				node->info = info;
				return info;
			}
			case ast_proc_hdr:
			case ast_cproc_hdr:
				{
					struct ProcHeader *ph = node->node;
					struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
					struct ProcInfo *pi = malloc(sizeof(struct ProcInfo));
					info->type = t_proc;
					info->pointer_level = 0;
					info->size = 8; // TODO: this compiler is 64-bit only rn idc
					info->proc_info = pi;
					struct AstRoot *args = ph->args;
					pi->len = args->len;
					pi->cproc = 1;
					pi->foreign_idx = t->cur_foreign_idx;
					int max = args->len;
					pi->names = malloc(sizeof(char*) * max);
					pi->types = malloc(sizeof(struct Typeinfo*) * max);
					if (ph->return_val) {
						pi->return_val = check_type(t, ph->return_val, emit_error);
					} else {
						pi->return_val = 0;
					}
					for (int i = 0; i < max; i++) {
						// TODO: lotsa assumptions here tbh
						struct BinaryOp *cur = args->nodes[i].node;
						pi->names[i] = cur->left->node;
						pi->types[i] = check_type(t, cur->right, emit_error);
					}
					return info;
				}
			case ast_proc:
				{
					struct Proc *proc = node->node;
					struct ProcHeader *ph = proc->header;
					struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
					struct ProcInfo *pi = malloc(sizeof(struct ProcInfo));
					info->type = t_proc;
					info->pointer_level = 0;
					info->size = 8; // TODO: this compiler is 64-bit only rn idc
					info->proc_info = pi;
					struct AstRoot *args = ph->args;
					pi->len = args->len;
					pi->cproc = 0;
					int max = args->len;
					pi->names = malloc(sizeof(char*) * max);
					pi->types = malloc(sizeof(struct Typeinfo*) * max);
					if (ph->return_val) {
						pi->return_val = check_type(t, ph->return_val, emit_error);
					} else {
						pi->return_val = 0;
					}
					for (int i = 0; i < max; i++) {
						// TODO: lotsa assumptions here tbh
						struct BinaryOp *cur = args->nodes[i].node;
						pi->names[i] = cur->left->node;
						pi->types[i] = check_type(t, cur->right, emit_error);
					}
					// TODO check proc scope tbh
					check_proc_scope(t, proc->body, info);
					return info;
				}
			case ast_struct:
				{
					struct AstRoot *s = node->node;
					struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
					struct StructInfo *si = malloc(sizeof(struct StructInfo));
					info->type = t_struct;
					info->size = 4;
					info->pointer_level = 0;
					info->struct_info = si;
					si->len = s->len;
					si->names = malloc(sizeof(char*) * s->len);
					si->types = malloc(sizeof(struct Typeinfo*) * s->len);
					si->values = malloc(sizeof(struct Literal) * s->len);
					struct BinaryOp *member;
					struct BinaryOp *submember;
					u64 offset = 0;
					for (int i = 0; i < s->len; i++) {
						member = s->nodes[i].node;
						switch (member->op) {
							case op_decl:
								si->names[i] = (char*)member->left->node;
								si->types[i] = check_type(t, member->right, emit_error);
								si->values[i].type = -1;
								break;
							case op_assign:
								submember = member->left->node;
								si->names[i] = (char*)submember->left->node;
								si->types[i] = check_type(t, submember->right, emit_error);
								si->values[i] = *(struct Literal*)member->right->node;
								break;
							case op_def:
								si->names[i] = (char*)member->left->node;
								si->types[i] = default_literal(t, check_expr(t, member->right));
								si->values[i] = *(struct Literal*)member->right->node;
								break;
						}
						// @Fixup struct nesting
							// switch (size_of(si->types[i])) {
							// 	case 8:
							// 		if (offset % 8 != 0) {
							// 			offset = offset - (offset % 8) + 8;
							// 		}
							// 		break;
							// 	case 4:
							// 		if (offset % 4 != 0) {
							// 			offset = offset - (offset % 4) + 4;
							// 		}
							// 		break;
							// 	case 2:
							// 		if (offset % 2 != 0) {
							// 			offset = offset - (offset % 2) + 2;
							// 		}
							// }
						offset += fix_alignment(alignment_of(si->types[i]), offset);
						offset += size_of(si->types[i]);
					}
					info->size = offset;

					struct AstNode *tst = malloc(sizeof(struct AstNode));
					tst->type = ast_literal;
					node->info = info;
					return info;
				}
		case ast_binary:
			{
				struct BinaryOp *op = node->node;
				switch (op->op) {
					case op_access:
						{
							struct Typeinfo *info = check_type(t, op->left, emit_error);
							if (info) {
								if (info->type != t_module) {
									printf("can't access member of a non-module type");
									error(t, node);
									segexit();
								}
								op->left->info = info;
								struct AstModule *mod = info->mod;
								struct Typechecker tempt;
								tempt.error = t->error;
								tempt.t = &info->mod->ts[0];
								tempt.cur_module = mod;
								tempt.scopes_len = t->scopes_len;
								tempt.scopes_max = t->scopes_max;
								tempt.scopes = t->scopes;
								tempt.offset = 0;
								return check_type(&tempt, op->right, emit_error);
							}
						}

				}
			}
		case ast_unary:
			{
				struct UnaryOp *op = node->node;
				switch (op->op) {
					case op_mult:
						{
							struct Typeinfo *infot = check_type(t, op->on, emit_error);
							struct Typeinfo *info = infot;
							// TODO fix this where it like makes a new type for every pointer level plus
							//if (find_type(t, infot->name)) {
								info = malloc(sizeof(struct Typeinfo));
								memcpy(info, infot, sizeof(struct Typeinfo));
							//}
							info->pointer_level++;
							/*
							char *temp = infot->name;
							int len = snprintf(NULL, 0, "*%s", temp);
							info->name = malloc(len + 1);
							info->name[len] = 0;
							snprintf(info->name, len + 1, "*%s", temp);
							*/
							node->info = info;
							return info;
						//TODO add the other things
						}
				}
			}
		case ast_ident:
			{
				struct Typeinfo *info = find_type(t, (char*)node->node);
				if (!info && emit_error) {
					printf("type doesn't exist: %s\n", node->node);
					error(t, node);
				}
				node->info = info;
				return info;
			}
	}
}

struct Typeinfo *default_literal(struct Typechecker *t, struct Typeinfo *i) {
	switch (i->type) {
		case t_int_literal:
			return find_type(t, "int");
		case t_bool_literal:
			return find_type(t, "bool");
		case t_float_literal:
			return find_type(t, "f32");
		case t_enum_literal:
			printf("enum literal can't be autodetected");
			error(t, t->cur_node);
			segexit();
		case t_array_literal:
			i->type = t_array;
			i->size = 0;
			i->array_info->type = default_literal(t, i->array_info->type);
			if (i->array_info->capacity) {
				int len = snprintf(NULL, 0, "[%d]%s", i->array_info->capacity, i->array_info->type->name);
				i->name = malloc(len + 1);
				i->name[len] = 0;
				snprintf(i->name, len + 1, "[%d]%s", i->array_info->capacity, i->array_info->type->name);
			} else {
				int len = snprintf(NULL, 0, "[]%s", i->array_info->type->name);
				i->name = malloc(len + 1);
				i->name[len] = 0;
				snprintf(i->name, len + 1, "[]%s", i->array_info->type->name);
			}
			return i;
		case t_string_literal:
			i->type = t_array;
			i->array_info = malloc(sizeof(struct ArrayInfo));
			i->array_info->type = find_type(t, "char");
			i->array_info->capacity = i->size;
			i->array_info->atype = arrayt_static;
			int len = snprintf(NULL, 0, "[]char");
			i->name = malloc(len + 1);
			i->name[len] = 0;
			snprintf(i->name, len + 1, "[]char");
			return i;
		case t_char_literal:
			return find_type(t, "char");
	}
	return i;
}

int type_eq(struct Typeinfo *a, struct Typeinfo *b) {
	if (!b) {
		printf("right side of expression doesn't return a value\n");
		return 0;
	}
	if (a->type == t_any || b->type == t_any) {
		return 1;
	}
	switch (a->type) {
		case t_int:
			if (b->type == t_int_literal) {
				return 1;
			} else if (b->type == t_char_literal) {
				if (!strcmp(a->name, "char")) {
					return 1;
				}
			} else if (b->type == t_int) {
				if (a == b) {
					return 1;
				}
				if (!strcmp(a->name, b->name)) {
					return 1;
				}
			} else if (b->type == t_bool_literal) {
				return 1;
			}
			return 0;
		case t_float:
			if (b->type == t_float_literal) {
				return 1;
			} else if (b->type == t_int_literal) {
				// u64 test = ((struct Literal*)b->node->node)->integer;
				// ((struct Literal*)b->node->node)->fl = (double)test;
				return 1;
			} else if (b->type == t_float) {
				if (a == b) {
					return 1;
				}
				if (!strcmp(a->name, b->name)) {
					return 1;
				}
			}
			return 0;
		case t_enum:
			if (b->type == t_enum_literal) {
				int len = strlen(b->name);
				for (int i = 0; i < a->enum_info->len; i++) {
					if (len == strlen(a->enum_info->names[i])) {
						if (!memcmp(b->name, a->enum_info->names[i], len)) {
							b->node->info = a;
							return 1;
						}
					}
				}
				printf("enum literal isn't part of enum\n");
				// TODO fix this poor dude
				//error(t, t->cur_node);
				segexit();
			}
			// TODO dont use type_eq for arithmetic ops
			if (b->type == t_int_literal) {
				return 1;
			}
			if (b->type == t_enum) {
				if (a == b) {
					return 1;
				}
			}
			return 0;
		case t_array:
			if (b->type == t_array_literal) {
				if (!type_eq(a->array_info->type, b->array_info->type)) {
					return 0;
				}
				return 1;
			} else if (b->type == t_array) {
				if (a == b) {
					return 1;
				}
				if (!strcmp(a->name, b->name)) {
					return 1;
				}
				if (type_eq(a->array_info->type, b->array_info->type)) { // autocast of [x]T to []T
					return 1;
				}
			}
			return 0;
		case t_int_literal:
			if (b->type == t_int_literal) {
				return 1;
			}
			if (b->type == t_float_literal || b->type == t_float) {
				return 2;
			}
			if (b->type == t_int) {
				return 2;
			}
			return 0;
		case t_char_literal:
			if (b->type == t_int_literal) {
				return 1;
			}
			if (b->type == t_int && !strcmp(b->name, "char")) {
				return 1;
			}
		case t_bool_literal:
			if (b->type == t_bool_literal) {
				return 1;
			}
			return 0;
		case t_float_literal:
			if (b->type == t_float_literal || b->type == t_int_literal) {
				return 1;
			}
			if (b->type == t_float) {
				return 2;
			}
			return 0;
		case t_enum_literal:
			if (b->type == t_enum_literal || b->type == t_int_literal) {
				return 1;
			}
			if (b->type == t_enum) {
				return 2;
			}
			return 0;
		case t_array_literal:
			if (b->type == t_array_literal) {
				if (type_eq(a->array_info->type, b->array_info->type) == 1) {
					if (a->array_info->capacity > b->array_info->capacity) {
						return 1;
					} else {
						b->array_info->type = a->array_info->type;
						return 2;
					}
				} else if (type_eq(a->array_info->type, b->array_info->type) == 2) {
					if (a->array_info->capacity > b->array_info->capacity) {
						a->array_info->type = b->array_info->type;
						return 1;
					} else {
						return 2;
					}
				}
			}
			if (b->type == t_array) {
				return 2;
			}
			return 0;
		case t_string_literal:
			if (b->type == t_string_literal) {
				return 1;
			}
			if (!strcmp("[]char", b->name)) {
				return 2;
			}
			return 0;
		default:
			if (a != b) {
				if (strcmp(a->name, b->name)) {
					return 0;
				}
			}
			return 1;
	}
}

struct Typeinfo *_check_expr(struct Typechecker *t, struct AstNode *node) {
	t->cur_node = node;
	switch (node->type) {
			case ast_block:
				// TODO: fix this and add returns in blocks
				{
					struct AstRoot *block = node->node;
					return check_scope(t, block);
				}
			case ast_foreign:
				{
					// struct AstForeign *af = node->node;
					// if (t->flibs->liblen == t->flibs->libmax - 1) {
					// 	t->flibs->libmax += 10;
					// 	t->flibs->libname = realloc(t->flibs->libname, t->flibs->libmax);
					// }
					// t->flibs->libname[t->flibs->liblen] = af->libname;
					// t->flibs->liblen += 1;
					// t->in_foreign_block = 1;
					// check_foreign_scope(t, af->block);
					// t->in_foreign_block = 0;
					return 0;
				}
			case ast_proc_call:
				{
					struct ProcCall *pc = node->node;
					struct Typeinfo *info = check_proc_call(t, pc);
					return info;
				}
			case ast_ident:
				{
					struct Typeinfo *info = find_ident(t, (char*)node->node);
					if (!info) {
						printf("variable doesn't exist: %s\n", node->node);
						error(t, node);
					}
					return info;
				}
			case ast_literal:
				{
					struct Literal *lit = node->node;
					switch (lit->type) {
						case 0:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_int_literal;
								return info;
							}
						case 1:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_float_literal;
								return info;
							}
						case 2:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_string_literal;
								info->size = strlen(lit->string);
								return info;
							}
						case 3:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_bool_literal;
								return info;
							}
						// enum literal
						case 4:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_enum_literal;
								info->name = lit->string;
								info->node = node;
								return info;
							}
						case 5:
							{
								struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
								info->type = t_char_literal;
								return info;
							}
					}
				}
			case ast_ptr_literal:
				{
					struct PointerLit *lit = node->node;
					/*
					struct Typeinfo *info = find_type(t, lit->string);
					struct Typeinfo *ret = malloc(sizeof(struct Typeinfo));
					memcpy(ret, info, sizeof(struct Typeinfo));
					ret->pointer_level += lit->pointer_level;
					*/
					// wtf this is not check_type()???
					struct Typeinfo *info = check_expr(t, lit->on);
					struct Typeinfo *ret = find_type(t, info->name);
					if (info->pointer_level < lit->pointer_level) {
						printf("can't dereference further than base type");
						error(t, node);
						segexit();
					}
					if (!ret || ret->pointer_level != info->pointer_level - lit->pointer_level) {
						ret = malloc(sizeof(struct Typeinfo));
						memcpy(ret, info, sizeof(struct Typeinfo));
						ret->pointer_level -= lit->pointer_level;
					}
					return ret;
				}
			case ast_binary:
			{
				struct BinaryOp *op = node->node;
				struct BinaryOp *bin = node->node;
				// TODO sure this is a good idea?
				switch (op->op) {
					case op_assign:
						{
							struct Typeinfo *ident_type = check_expr(t, op->left);
							struct Typeinfo *matching_type = check_expr(t, op->right);
							if (op->right->type == ast_no_init) {
								return ident_type;
							}
							if (!matching_type) {
								printf("expression returns no value");
								error(t, node);
								segexit();
							}
							if (ident_type->type >= t_int_literal) {
								printf("can't assign something to a literal");
								error(t, node);
								segexit();
							}
							if (!type_eq(ident_type, matching_type)) {
								// TODO improve error message
								if (matching_type->type != t_enum_literal) {
									matching_type = default_literal(t, matching_type);
								} else {
									matching_type->name = "enum literal";
								}
								printf("types dont match: left: %s, right: %s\n", ident_type->name, matching_type->name);
								error(t, node);
								segexit();
							}
							if (ident_type->type == t_array && ident_type->array_info->capacity) {
								if (matching_type->array_info->capacity > ident_type->array_info->capacity) {
									printf("can't assign array literal bigger than array capacity");
									error(t, node);
									segexit();
								}
							}
							op->right->info = ident_type; // is this good?
							return ident_type;
						}
					case op_decl:
						{
							// TODO: add errors like these all over the place
							struct Typeinfo *info = check_type(t, op->right, 1);
							if (op->right->type == ast_proc_call && !info) {
								printf("procedure returns nothing; can't assign");
								error(t, node);
								segexit();
							}
							if (op->left->type != ast_ident) {
								printf("can't declare anything that is not an ident");
								error(t, node);
								segexit();
							}
							if (add_ident(t, (char*)op->left->node, info)) {
								error(t, node);
								segexit();
							}
							return info;
						}
					case op_def:
						{
							// i'm trusting the coder to not assign 42 as type rn
							struct Typeinfo *info = check_expr(t, op->right);
							if (!info) {
								printf("expression returns no value");
								error(t, op->right);
								segexit();
							}
							info = default_literal(t, info);
							op->right->info = info; // this good?
							if (add_ident(t, (char*)op->left->node, info)) {
								error(t, node);
								segexit();
							}
							if (debug)
								printf("%s type: %s\n", (char*)op->left->node, info->name);
							return info;
						}
					case op_const:
						{
							struct Typeinfo *temp;
							if (op->right->type == ast_ident) {
								temp = malloc(sizeof(struct Typeinfo));
								memcpy(temp, find_type(t, op->right->node), sizeof(struct Typeinfo));
							} else {
								temp = check_type(t, op->right, 1);
							}
							temp->name = (char*)op->left->node;
							if (!strcmp(temp->name, "any")) {
								temp->type = t_any;
							}
							//memcpy(find_type(t, temp->name), temp, sizeof(struct Typeinfo));
							return temp;
						}
					case op_access:
						{
							struct Typeinfo *info = check_type(t, op->left, 0);
							if (info) {
								if (info->type != t_module) {
									printf("can't access member of a non-module type");
									error(t, node);
									segexit();
								}
								op->left->info = info;
								struct AstModule *mod = info->mod;
								struct Typechecker tempt;
								tempt.error = t->error;
								tempt.t = &info->mod->ts[0];
								tempt.cur_module = mod;
								tempt.scopes_len = t->scopes_len;
								tempt.scopes_max = t->scopes_max;
								tempt.scopes = t->scopes;
								tempt.offset = 0;
								return check_expr(&tempt, op->right);
							}
							info = check_expr(t, op->left);
							if (info->type != t_struct && info->type != t_array && info->type != t_any) {
								// TODO: exceptions like <array>.len
								printf("can't access member of a non-struct");
								error(t, node);
								segexit();
							}
							if (info->type == t_any) {
								//TODO please remove the hardcoding
								if (!strcmp(op->right->node, "data")) {
									struct Typeinfo *temp = malloc(sizeof(struct Typeinfo));
									memcpy(temp, find_type(t, "void"), sizeof(struct Typeinfo));
									temp->pointer_level++;
									return temp;
								}
								if (!strcmp(op->right->node, "data")) {
									return find_type(t, "Typeinfo");
								}
							}
							if (info->type == t_array) {
								if (!strcmp(op->right->node, "data")) {
									struct Typeinfo *pd = malloc(sizeof(struct Typeinfo));
									memcpy(pd, info->array_info->type, sizeof(struct Typeinfo));
									pd->pointer_level++;
									return pd;
								}
								if (!strcmp(op->right->node, "len")) {
									return find_type(t, "u64");
								}
								if (!strcmp(op->right->node, "max") && info->array_info->atype == arrayt_dynamic) {
									return find_type(t, "u64");
								}
								printf("array has no member %s", op->right->node);
								error(t, node);
								return 0;
							}
							struct StructInfo *si = info->struct_info;
							int len1 = strlen(op->right->node);
							for (int i = 0; i < si->len; i++) {
								int len2 = strlen(si->names[i]);
								if (len1 != len2) {
									continue;
								}
								if (!memcmp(op->right->node, si->names[i], len1)) {
									return si->types[i];
								}
							}
							printf("struct has no member %s", op->right->node);
							error(t, node);
							return 0;
						}
					case op_subscript:
						{
							check_expr(t, bin->right);
							struct Typeinfo *temp = check_expr(t, bin->left);
							if (temp->type != t_array) {
								printf("can't subscript non-arrays");
								error(t, op->right);
								return 0;
							}
							return temp->array_info->type;
						}
					case op_slice:
						{
							check_expr(t, bin->right);
							struct Typeinfo *temp = check_expr(t, bin->left);
							if (temp->type != t_array) {
								printf("can't subscript non-arrays");
								error(t, op->right);
								return 0;
							}
							struct BinaryOp *range = bin->right->node;
							temp->array_info->capacity = range->right - range->left;
							temp->array_info->atype = arrayt_stack;
							return temp;
						}
					case op_range:
						{
							struct Typeinfo *temp = 0;
							struct Typeinfo *temp2 = 0;
							if (bin->left->node) {
								temp = check_expr(t, bin->left);
							}
							if (bin->right->node) {
								temp2 = check_expr(t, bin->left);
							}
							if (temp && temp2) {
								int m = type_eq(temp, temp2);
								if (m == 1) {
									bin->right->info = temp;
									return temp;
								} else if (m == 2) {
									bin->left->info = temp2;
									return temp2;
								} else {
									temp = default_literal(t, temp);
									temp2 = default_literal(t, temp2);
									printf("types dont match: left: %s, right: %s\n", temp->name, temp2->name);
									error(t, node);
									segexit();
								}
							} else {
								if (temp) {
									return temp;
								}
								if (temp2) {
									return temp2;
								}
								return find_type(t, "int");
							}
						}
					case op_cast:
						check_expr(t, bin->left);
						return check_type(t, bin->right, 1);
				}
				// TODO: operator overloading, casting etc
				struct Typeinfo *temp = check_expr(t, bin->left);
				struct Typeinfo *temp2 = check_expr(t, bin->right);
				if (bin->left->type == ast_proc_call && !temp) {
					printf("procedure returns nothing");
					error(t, node);
					segexit();
				}
				if (bin->right->type == ast_proc_call && !temp2) {
					printf("procedure returns nothing");
					error(t, node);
					segexit();
				}
				int m = type_eq(temp, temp2);
				if (m == 1) {
					bin->right->info = temp;
					return temp;
				} else if (m == 2) {
					bin->left->info = temp2;
					return temp2;
				} else {
					temp = default_literal(t, temp);
					temp2 = default_literal(t, temp2);
					printf("types dont match: left: %s, right: %s\n", temp->name, temp2->name);
					error(t, node);
					segexit();
				}
			}
			case ast_unary:
				{
					struct UnaryOp *op = node->node;
					switch (op->op) {
						case op_take_pointer:
							{
							struct Typeinfo *infot = check_expr(t, op->on);
							struct Typeinfo *info = infot;
							//if (infot->name) {
								//if (find_type(t, infot->name)) {
									// @Fixup fix this tbh
									info = malloc(sizeof(struct Typeinfo));
									memcpy(info, infot, sizeof(struct Typeinfo));
								//}
							//}
							info->pointer_level++;
							/*
							char *temp = infot->name;
							int len = snprintf(NULL, 0, "*%s", temp);
							info->name = malloc(len + 1);
							info->name[len] = 0;
							snprintf(info->name, len + 1, "*%s", temp);
							*/
							return info;
							}
						case op_paren:
							return check_expr(t, op->on);
						case op_minus:
							{
								struct Typeinfo *info = check_expr(t, op->on);
								if (info->type != t_int && info->type != t_float && info->pointer_level) {
									printf("cant use the negate operator on a non-scalar type");
									error(t, node);
									segexit();
								}
								return info;
							}
						case op_not:
							{
								struct Typeinfo *info = check_expr(t, op->on);
								if (info->type != t_int && info->type != t_float && info->pointer_level) {
									printf("cant use the not operator on a non-scalar type");
									error(t, node);
									segexit();
								}
								return info;
							}
						case op_deref:
							{
								struct Typeinfo *infot = check_expr(t, op->on);
								struct Typeinfo *info = infot;
								struct Typeinfo temp;
								if (infot->pointer_level <= 0) {
									printf("can't dereference further than base type");
									error(t, node);
									segexit();
								}
								if (infot->name) {
									if (find_type(t, infot->name)) {
										info = find_type(t, infot->name);
										temp = *info;
										temp.pointer_level = infot->pointer_level;
										if (temp.pointer_level - 1 == info->pointer_level) {
											return info;
										} else {
											temp.pointer_level--;
											info = malloc(sizeof(struct Typeinfo));
											memcpy(info, &temp, sizeof(struct Typeinfo));
											return info;
										}
									}
								}
								info = malloc(sizeof(struct Typeinfo));
								memcpy(info, infot, sizeof(struct Typeinfo));
								info->pointer_level--;
								return info;
							}
					}
				}
			case ast_no_init:
				return 0;
			// unused here
			case ast_module_stmt:
			// unused here
			case ast_import:
			// unused here
			return 0;
			case ast_arr_lit:
				return check_type(t, node, 1);
			case ast_array:
				{
					//TODO: fix this idk
					// and make it not allocate more!
					struct AstRoot *arr = node->node;
					struct Typeinfo *temp;
					if (arr->len > 0) {
						temp = check_expr(t, &arr->nodes[0]);
						struct Typeinfo *amog;
						int te;
						for (int i = 1; i < arr->len; i++) {
							amog = check_expr(t, &arr->nodes[i]);
							te = type_eq(temp, amog);
							if (te == 1) {
								continue;
							} if (te == 2) {
								temp = amog;
								continue;
							}
							printf("array types aren't consistent");
							error(t, &arr->nodes[i]);
							segexit();
						}
						struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
						struct ArrayInfo *ai = malloc(sizeof(struct ArrayInfo));
						info->type = t_array_literal;
						info->pointer_level = 0;
						info->size = 0;
						info->array_info = ai;
						ai->atype = arrayt_stack;
						ai->capacity = arr->len;
						ai->type = temp;
						return info;
					}
					return 0;
				}
			case ast_for:
				{
					struct AstFor *loop = node->node;
					check_for_scope(t, loop->block, loop);
					return 0;
				}
			case ast_loop:
				check_scope(t, node->node);
				return 0;
			case ast_while:
				{
					struct AstWhile *wl = node->node;
					check_expr(t, wl->cond);
					check_scope(t, wl->block);
					return 0;
				}
			case ast_if:
				{
					struct AstIf *ai = node->node;
					// TODO check if the cond evaluates to an int
					//struct Typeinfo *cond = default_literal(check_expr(t, ai->cond));
					struct Typeinfo *ballin = check_expr(t, ai->cond);
					if (!ballin) {
						printf("expression returns no value");
						error(t, ai->cond);
						segexit();
					}
					check_scope(t, ai->block);
					if (ai->else_block) {
						check_expr(t, ai->else_block);
					}
					return 0;
				}
			case ast_match:
				{
					struct Match *match = node->node;
					struct Typeinfo *ontype;
					struct Typeinfo *tempt;
					//TODO add string support with a hash table
					ontype = check_expr(t, match->on);
					if (ontype->type != t_int && ontype->type != t_enum) {
						printf("can't match on anything other than an int or an enum currently");
						error(t, match->on);
						segexit();
					}
					for (int i = 0; i < match->len; i++) {
						tempt = check_expr(t, match->conds[i]);
						if (tempt->type != t_int && tempt->type != t_int_literal && tempt->type != t_enum_literal) {
							printf("can't match on anything other than an int or an enum currently");
							error(t, match->conds[i]);
							segexit();
						}
						match->conds[i]->info = ontype;
						struct AstNode tempn;
						tempn.type = ast_block;
						tempn.node = match->blocks[i];
						check_expr(t, &tempn);
					}
					//TODO add return for match
					return 0;
				}
			case ast_directive:
			case ast_type:
				return 0;
			case ast_module:
				return check_module(t, node->node);
			case ast_cflow:
				{
					struct CFlow *cf = node->node;
					switch (cf->type) {
						case 0:
							return 0;
						case 1:
							if (cf->on) {
								if (!t->cur_proc->proc_info->return_val) {
									printf("return in function that doesn't have a return type defined");
									error(t, node);
									segexit();
								}
								if (type_eq(t->cur_proc->proc_info->return_val, check_expr(t, cf->on))) {
									cf->on->info = t->cur_proc->proc_info->return_val;
									return t->cur_proc->proc_info->return_val;
								} else {
									printf("return of incorrect type");
									error(t, node);
									segexit();
								}
							}
					}
				}
				case -1:
					// comment prolly
					return 0;
	}
	printf("didn't implement that yet");
	error(t, node);
	segexit();
}

inline struct Typeinfo *check_expr(struct Typechecker *t, struct AstNode *node) {
	node->info = _check_expr(t, node);
	return node->info;
}

void check_proc_scope(struct Typechecker *t, struct AstRoot *block, struct Typeinfo *info) {
	struct AstNode *cur;

	struct ProcInfo *pi = info->proc_info;

	// init scope
	// ---
	block->scope = malloc(sizeof(struct Scope));
	struct Scope *scope = block->scope;
	scope->ilen = 0;
	scope->imax = 20;
	scope->idents = malloc(scope->imax * sizeof(void*));
	scope->itypes = malloc(scope->imax * sizeof(struct Typeinfo*));
	// ---
	//

	struct AstNode *p = t->cur_node;

	struct Scope *global_scope = t->scopes[0];

	int offset = t->offset;
	t->offset = t->scopes_len + 1;
	t->cur_proc = info;

	add_scope(t, global_scope);
	add_scope(t, scope);
	//TODO: this is a hack that's terrible for performance i think
	//
	for (int i = 0; i < pi->len; i++) {
		add_ident(t, pi->names[i], pi->types[i]);
	}

	int ret = 0;
	int retv = 0;

	for (int i = 0; i < block->len; i++) {
		cur = &block->nodes[i];
		t->cur_node = cur;
		if (cur->type != ast_cflow) {
			check_expr(t, cur);
		} else {
			struct CFlow *cf = cur->node;
			if (cf->on) {
				retv = 1;
			}
			//TODO restructure and put the other ifs there
			if (cf->type == 1) {
				if (cf->on) {
					ret = 1;
				}
				if (pi->return_val) {
					struct Typeinfo *returned = check_expr(t, cf->on);
					cf->on->info = pi->return_val;
					if (retv && !type_eq(pi->return_val, returned)) {
						printf("type returned doesn't match return type of procedure\n");
						printf("returned: %s but should return: %s\n", returned->name, pi->return_val->name);
						error(t, &block->nodes[i]);
						segexit();
					}
					if (!retv) {
						printf("procedure doesn't return anything when it should");
						error(t, p);
						segexit();
					}
				}
				if (ret && retv && !pi->return_val) {
					printf("procedure returns value but has no return value defined");
					error(t, &block->nodes[i]);
					segexit();
				}
			}
		}
	}
	if (!ret && pi->return_val) {
		printf("no value returned in procedure that should");
		error(t, p);
	}
	delete_scope(t);
	t->offset = offset;
}

void check_for_scope(struct Typechecker *t, struct AstRoot *block, struct AstFor *af) {
	struct AstNode *cur;

	// init scope
	// ---
	block->scope = malloc(sizeof(struct Scope));
	struct Scope *scope = block->scope;
	scope->ilen = 0;
	scope->imax = 20;
	scope->idents = malloc(scope->imax * sizeof(void*));
	scope->itypes = malloc(scope->imax * sizeof(struct Typeinfo*));
	// ---
	//

	struct Typeinfo *fortype = 0;

	if (af->iterator->type == ast_binary && ((struct BinaryOp*)af->iterator->node)->op == op_range) {
		struct BinaryOp *op = af->iterator->node;
		struct Typeinfo *left = check_expr(t, op->left);
		struct Typeinfo *right = check_expr(t, op->right);
		if (!type_eq(left, right)) {
			printf("types on for loop iterator aren't consistent");
			error(t, t->cur_node);
		}
		fortype = default_literal(t, left);
	}

	if (!fortype) {
		printf("gotta fix some things tbh");
		error(t, t->cur_node);

	}

	add_scope(t, scope);

	if (add_ident(t, af->var, fortype)) {
		error(t, t->cur_node);
		segexit();
	}
	for (int i = 0; i < block->len; i++) {
		cur = &block->nodes[i];
		t->cur_node = cur;
		check_expr(t, cur);
	}
	delete_scope(t);
}

void check_foreign_scope(struct Typechecker *t, struct AstRoot *block) {
	struct AstNode *cur;

	// init scope
	// ---
	block->scope = malloc(sizeof(struct Scope));
	struct Scope *scope = block->scope;
	scope->ilen = 0;
	scope->imax = 20;
	scope->idents = malloc(scope->imax * sizeof(void*));
	scope->itypes = malloc(scope->imax * sizeof(struct Typeinfo*));
	// ---
	//

	struct Typeinfo *ret = 0;

	add_scope(t, scope);
	for (int i = 0; i < block->len; i++) {
		cur = &block->nodes[i];
		t->cur_node = cur;
		
		struct AstModule *mod = t->cur_module;

		struct AstNode *node = mod->types[i]->node;

		memcpy(mod->types[i], check_expr(t, cur), sizeof(struct Typeinfo));

		mod->types[i]->node = node;
	}
	delete_scope(t);
}

struct Typeinfo *check_scope(struct Typechecker *t, struct AstRoot *block) {
	struct AstNode *cur;

	// init scope
	// ---
	block->scope = malloc(sizeof(struct Scope));
	struct Scope *scope = block->scope;
	scope->ilen = 0;
	scope->imax = 20;
	scope->idents = malloc(scope->imax * sizeof(void*));
	scope->itypes = malloc(scope->imax * sizeof(struct Typeinfo*));
	// ---
	//

	struct Typeinfo *ret = 0;

	add_scope(t, scope);
	for (int i = 0; i < block->len; i++) {
		cur = &block->nodes[i];
		t->cur_node = cur;
		check_expr(t, cur);
		/*
		if (cur->type == ast_cflow) {
			struct CFlow *cf = cur->node;
			if (cf->type == 1) {
				ret = check_expr(t, cf->on);
			}
		}
		*/
	}
	delete_scope(t);
	return ret;
}

struct Typeinfo *check_module(struct Typechecker *t, struct AstModule *mod) {
	struct AstNode *cur;

	struct Tokenizer *tok = t->t;
	// init scope
	// ---
	mod->scope = malloc(sizeof(struct Scope));
	struct Scope *scope = mod->scope;
	scope->ilen = 0;
	scope->imax = 20;
	scope->idents = malloc(scope->imax * sizeof(void*));
	scope->itypes = malloc(scope->imax * sizeof(struct Typeinfo*));
	// ---
	//

	t->cur_module = mod;
	mod->tlen = 0;
	mod->tmax = 20;
	mod->types = malloc(mod->tmax * sizeof(struct Typeinfo*));

	setup_internal_types(t);

	struct Typeinfo *ret = 0;

	add_scope(t, scope);
	struct AstRoot *block;
	for (int y = 0; y < mod->len; y++) {
		t->t = &mod->ts[y];
		block = mod->global_scope[y];
		for (int i = 0; i < block->len; i++) {
			cur = &block->nodes[i];
			t->cur_node = cur;
			switch (cur->type) {
				case ast_module:
					{
						struct AstModule *modt = cur->node;
						struct Typeinfo *info = malloc(sizeof(struct Typeinfo));
						info->type = t_module;
						info->name = modt->name;
						info->node = cur;
						info->mod = modt;
						check_module(t, modt);
						t->cur_module = mod;
						if (add_type(t, info)) {
							error(t, cur);
							segexit();
						}
						break;
					}
				case ast_binary:
					{
						struct BinaryOp *op = cur->node;
						switch (op->op) {
							case op_const:
								{
									struct Typeinfo *info = malloc(sizeof(struct Typeinfo)); //TODO instead of allocating two structs here, make types an array of double pointers so they can be switched out
									info->type = t_pending;
									info->name = (char*)op->left->node;
									info->node = cur;
									info->t = t->t;
									info->proc_info = 0;
									if (add_type(t, info)) {
										error(t, cur);
										segexit();
									}
									break;
								}
						}
						break;
					}
				case ast_foreign:
					{
						struct AstForeign *af = cur->node; // TODO this is terrible and not lazy but thats how im gonna roll for now ig kekee
						if (t->flibs->liblen == t->flibs->libmax - 1) {
							t->flibs->libmax += 10;
							t->flibs->libname = realloc(t->flibs->libname, t->flibs->libmax);
						}
						t->flibs->libname[t->flibs->liblen] = af->libname;
						struct AstRoot *ball = af->block;
						for (int i = 0; i < ball->len; i++) {
							struct AstNode *_cur = &ball->nodes[i];
							switch (_cur->type) {
								case ast_binary:
									{
										struct BinaryOp *op = _cur->node;
										switch (op->op) {
											case op_const:
												{
													struct Typeinfo *info = malloc(sizeof(struct Typeinfo)); //TODO instead of allocating two structs here, make types an array of double pointers so they can be switched out
													if (op->right->type != ast_cproc_hdr && op->right->type != ast_proc_hdr) {
														printf("foreign blocks are only for function headers\n");
														segexit();
													}
													info->type = t_pending;
													info->name = (char*)op->left->node;
													info->node = _cur;
													info->t = t->t;
													info->proc_info = (void*)((u64)t->flibs->liblen + 1); // TODO suboptimal solution but ball
																																	// what if i made a foreign_proc parser struct
													if (add_type(t, info)) {
														error(t, _cur);
														segexit();
													}
													break;
												}
										}
										break;
									}
							}
						}
						t->flibs->liblen += 1;
					}
				}
		}
	}
	/*
	for (int y = 0; y < mod->len; y++) {
		t->t = &mod->ts[y];
		block = mod->global_scope[y];
		for (int i = 0; i < block->len; i++) {
			cur = &block->nodes[i];
			t->cur_node = cur;
			switch (cur->type) {
				case ast_module:
					struct AstModule *modt = cur->node;
					if (find_type_module_nocheck(t, mod, modt->name)) {
						continue;
					}
					break;
				case ast_binary:
					struct BinaryOp *op = cur->node;
					switch (op->op) {
						case op_const:
							if (find_type_module_nocheck(t, (char*)op->left->node)) {
								continue;
							}
					}
			}
			check_expr(t, cur);
		}
	}
	*/
	delete_scope(t);
	t->t = tok;
	return ret;
}

struct Typechecker *typecheck(struct AstModule *mod) {
	struct Typechecker *t = malloc(sizeof(struct Typechecker));
	t->t = &mod->ts[0];
	t->scopes_len = 0;
	t->scopes_max = 30;
	t->scopes = malloc(t->scopes_max * sizeof(struct Scope*));
	t->cur_node = 0;
	t->offset = 0;
	t->cur_foreign_idx = 0;
	t->flibs = malloc(sizeof(struct ForeignLib));
	t->flibs->libmax = 20;
	t->flibs->liblen = 0;
	t->flibs->libname = malloc(sizeof(char*) * t->flibs->libmax);
	int *error = malloc(4);
	t->error = error;

	check_module(t, mod);
	t->cur_module = mod;
	find_type(t, "main");
	t->cur_module = mod;
	return t;
}
