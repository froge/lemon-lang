void print_indent(int indent) {
	for (int i = 0; i < indent; i++) {
		printf("	");
	}
}

void print_node_internal(struct AstNode *node, int indent) {
	if (node == 0) {
		print_indent(indent);
		printf("NULL\n");
		return;
	}
	if ((int)node->type == -1) {
		print_indent(indent);
		printf("[empty]\n");
		return;
	}
	switch (node->type) {
		case ast_block:
			{
				struct AstRoot *root = node->node;
				print_indent(indent);
				printf("AstBlock\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < root->len; i++) {
					print_node_internal(&root->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_foreign:
			{
				struct AstForeign *af = node->node;
				struct AstRoot *root = af->block;
				print_indent(indent);
				printf("AstForeign\n");
				print_indent(indent);
				printf("libname: %s\n", af->libname);
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < root->len; i++) {
					print_node_internal(&root->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_struct:
			{
				struct AstRoot *root = node->node;
				print_indent(indent);
				printf("AstStruct\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < root->len; i++) {
					print_node_internal(&root->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_enum:
			{
				struct AstRoot *root = node->node;
				print_indent(indent);
				printf("AstEnum\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < root->len; i++) {
					print_node_internal(&root->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_for:
			{
				struct AstFor *loop = node->node;
				print_indent(indent);
				printf("AstFor\n");
				print_indent(indent);
				printf("var name:\n");
				print_indent(indent + 1);
				printf("%s\n", loop->var);
				print_indent(indent);
				printf("iterator:\n");
				print_node_internal(loop->iterator, indent + 1);
				print_indent(indent);
				printf("block:\n");
				struct AstNode *block = malloc(sizeof(struct AstNode));
				block->type = ast_block;
				block->node = loop->block;
				print_node_internal(block, indent + 1);
				printf("\n");
				break;
			}
		case ast_loop:
			{
				struct AstBlock *loop = node->node;
				struct AstNode temp = *node;
				temp.type = ast_block;
				print_indent(indent);
				printf("AstLoop\n");
				print_indent(indent);
				printf("block:\n");
				print_node_internal(&temp, indent + 1);
				break;
			}
		case ast_while:
			{
				struct AstWhile *loop = node->node;
				struct AstNode temp;
				temp.node = loop->block;
				temp.type = ast_block;
				print_indent(indent);
				printf("AstWhile\n");
				print_indent(indent);
				printf("cond:\n");
				print_node_internal(loop->cond, indent + 1);
				print_indent(indent);
				printf("block:\n");
				print_node_internal(&temp, indent + 1);
				break;
			}
		case ast_match:
			{
				struct Match *match = node->node;
				print_indent(indent);
				printf("Match\n");
				print_indent(indent);
				printf("on:\n");
				print_node_internal(match->on, indent + 1);
				print_indent(indent);
				printf("len:\n");
				print_indent(indent + 1);
				printf("%i\n", match->len);
				struct AstNode block;
				block.type = ast_block;
				for (int i = 0; i < match->len; i++) {
					print_indent(indent);
					printf("cond:\n");
					print_node_internal(match->conds[i], indent + 1);
					print_indent(indent);
					printf("block:\n");
					block.node = match->blocks[i];
					print_node_internal(&block, indent + 1);
				}
				printf("\n");
				break;
			}
		case ast_if:
			{
				struct AstIf *cond = node->node;
				print_indent(indent);
				printf("AstIf\n");
				print_indent(indent);
				printf("cond:\n");
				print_node_internal(cond->cond, indent + 1);
				print_indent(indent);
				printf("block:\n");
				struct AstNode *block = malloc(sizeof(struct AstNode));
				block->type = ast_block;
				block->node = cond->block;
				print_node_internal(block, indent + 1);
				printf("else:\n");
				print_node_internal(cond->else_block, indent + 1);
				printf("\n");
				break;
			}
		case ast_proc_call:
			{
				struct ProcCall *call = node->node;
				print_indent(indent);
				printf("AstProcCall\n");
				print_indent(indent);
				printf("name: \"%s\"\n", call->name);
				print_indent(indent);
				printf("args:\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < call->args->len; i++) {
					print_node_internal(&call->args->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_proc_hdr:
			{
				struct ProcHeader *hdr = node->node;
				print_indent(indent);
				printf("AstProcHdr\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < hdr->args->len; i++) {
					print_node_internal(&hdr->args->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				print_indent(indent);
				printf("return_val:\n");
				print_node_internal(hdr->return_val, indent + 1);
				printf("\n");
				break;
			}
		case ast_cproc_hdr:
			{
				struct ProcHeader *hdr = node->node;
				print_indent(indent);
				printf("AstCprocHdr\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < hdr->args->len; i++) {
					print_node_internal(&hdr->args->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				print_indent(indent);
				printf("return_val:\n");
				print_node_internal(hdr->return_val, indent + 1);
				printf("\n");
				break;
			}
		case ast_proc:
			{
				struct Proc *proc = node->node;
				struct AstNode *header = malloc(sizeof(struct AstNode));
				struct AstNode *body = malloc(sizeof(struct AstNode));
				print_indent(indent);
				printf("AstProc\n");
				header->type = ast_proc_hdr;
				header->node = proc->header;
				print_indent(indent);
				printf("header:\n");
				print_node_internal(header, indent + 1);
				printf("\n");
				body->type = ast_block;
				body->node = proc->body;
				print_indent(indent);
				printf("body:\n");
				print_node_internal(body, indent + 1);
				printf("\n");
				break;
			}
		case ast_ident:
			{
				print_indent(indent);
				printf("AstIdent\n");
				print_indent(indent);
				printf("\"%s\"\n", (char*)node->node);
				break;
			}
		case ast_directive:
			{
				struct Directive *dir = node->node;
				print_indent(indent);
				printf("AstDirective\n");
				switch (dir->type) {
					case dir_include:
						print_indent(indent);
						printf("dir_include:\n");
						print_indent(indent + 1);
						printf("\"%s\"\n", (char*)dir->on);
				}
				break;
			}
		case ast_array:
			{
				struct AstRoot *arr = node->node;
				print_indent(indent);
				printf("Array\n");
				print_indent(indent);
				printf("[\n");
				for (u64 i = 0; i < arr->len; i++) {
					print_node_internal(&arr->nodes[i], indent + 1);
				}
				printf("\n");
				print_indent(indent);
				printf("]\n");
				break;
			}
		case ast_ptr_literal:
			{
				struct PointerLit *lit = node->node;
				print_indent(indent);
				printf("AstPtrLit\n");
				print_indent(indent);
				printf("pointer level: %li\n", lit->pointer_level);
				print_indent(indent);
				printf("on:\n");
				print_node_internal(lit->on, indent + 1);
				break;
			}
		case ast_cflow:
			{
				struct CFlow *cf = node->node;
				print_indent(indent);
				printf("Control Flow\n");
				print_indent(indent);
				printf("type:\n");
				print_indent(indent + 1);
				switch (cf->type) {
					case 0:
						printf("break\n");
					case 1:
						printf("return\n");
				}
				print_indent(indent);
				printf("on:\n");
				print_node_internal(cf->on, indent + 1);
				break;
			}
		case ast_literal:
			{
				struct Literal *lit = node->node;
				print_indent(indent);
				printf("AstLiteral\n");
				switch (lit->type) {
					case -1:
						print_indent(indent);
						printf("init to zero");
						break;
					case 0:
						print_indent(indent);
						printf("int:\n");
						print_indent(indent);
						printf("%lu", lit->integer);
						break;
					case 1:
						print_indent(indent);
						printf("float:\n");
						print_indent(indent);
						printf("%f", lit->fl);
						break;
					case 2:
						print_indent(indent);
						printf("string:\n");
						print_indent(indent);
						printf("\"%s\"", lit->string);
						break;
					case 3:
						print_indent(indent);
						printf("bool:\n");
						print_indent(indent);
						if (lit->integer) {
							printf("true");
						} else {
							printf("false");
						}
						break;
					case 4:
						print_indent(indent);
						printf("enum literal:\n");
						print_indent(indent);
						printf(".%s", lit->string);
						break;
					case 5:
						print_indent(indent);
						printf("char:\n");
						print_indent(indent);
						printf("\'%s\'", lit->string);
						break;
				}
				printf("\n");
				break;
			}
			/*
		case ast_range:
			{
				struct Range *range = node->node;
				print_indent(indent);
				printf("AstRange\n");
				print_indent(indent);
				printf("low:\n");
				print_node_internal(range->low, indent + 1);
				printf("\n");
				print_indent(indent);
				printf("high:\n");
				print_node_internal(range->high, indent + 1);
				printf("\n");
				break;
			}
			*/
		case ast_binary:
			{
				struct BinaryOp *op = node->node;
				print_indent(indent);
				printf("Binary\n");
				print_indent(indent);
				printf("%d\n", op->op);
				print_indent(indent);
				printf("left:\n");
				print_node_internal(op->left, indent + 1);
				printf("\n");
				print_indent(indent);
				printf("right:\n");
				print_node_internal(op->right, indent + 1);
				printf("\n");
				break;
			}
		case ast_unary:
			{
				struct UnaryOp *op = node->node;
				print_indent(indent);
				printf("UnaryOperator\n");
				print_indent(indent);
				printf("%d\n", op->op);
				print_indent(indent);
				printf("on:\n");
				print_node_internal(op->on, indent + 1);
				break;
			}
		case ast_module:
			{
				struct AstModule *mod = node->node;
				print_indent(indent);
				printf("AstModule\n");
				struct AstNode temp;
				temp.type = ast_block;
				for (int i = 0; i < mod->len; i++) {
					temp.node = mod->global_scope[i];
					print_node_internal(&temp, indent + 1);
				}
				break;
			}
		case ast_module_stmt:
			{
				print_indent(indent);
				printf("AstModule\n");
				print_indent(indent);
				printf("\"%s\"\n", (char*)node->node);
				break;
			}
		case ast_import:
			{
				print_indent(indent);
				printf("AstImport\n");
				print_indent(indent);
				printf("\"%s\"\n", (char*)node->node);
				break;
			}
		case ast_include:
			{
				print_indent(indent);
				printf("AstInclude\n");
				print_indent(indent);
				printf("\"%s\"\n", (char*)node->node);
				break;
			}
		case ast_arr_lit:
			{
				struct ArrayLit *arr = node->node;
				print_indent(indent);
				printf("Array\n");
				print_indent(indent);
				printf("type: %i\n", arr->type);
				print_indent(indent);
				printf("capacity: %i\n", arr->capacity);
				print_node_internal(arr->on, indent + 1);
			}
		default:
			{}
	}
}

void print_node(struct AstNode *node) {
	print_node_internal(node, 0);
}
