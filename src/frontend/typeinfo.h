enum TypeType {
	t_pending,
	t_int,
	t_float,
	t_struct,
	t_enum,
	t_array,
	t_proc,
	t_type,
	t_any,
	t_module, // this is not actually a type but makes checking easier
	t_int_literal,
	t_bool_literal,
	t_float_literal,
	t_enum_literal,
	t_array_literal,
	t_string_literal,
	t_char_literal,
};

struct Typeindex {
	int scope;
	int type;
};

struct EnumInfo {
	int len;
	char **names;
	int *values;
};

//TODO: do i need this??
struct StructInfo {
	int len;
	char **names;
	struct Typeinfo **types;
	struct Literal *values;
};

//TODO: do i need THIS?????
struct ArrayInfo {
	u8 atype;
	u32 capacity;
	struct Typeinfo *type;
};

//TODO: do i need THIS?????????
struct ProcInfo {
	int len;
	int cproc;
	int foreign_idx;
	char **names;
	struct Typeinfo **types;
	struct Typeinfo *return_val;
};

struct Typeinfo {
	enum TypeType type;
	int pointer_level;
	int size;
	char *name;
	struct AstNode *node;
	struct Tokenizer *t; //only used to set the tokenizer correctly, probably temporary fix
	union {
		int isigned;
		struct EnumInfo *enum_info;
		struct StructInfo *struct_info;
		struct ArrayInfo *array_info;
		struct ProcInfo *proc_info;
		struct Typeinfo *pointer_info;
		struct AstModule *mod;
	};
};
