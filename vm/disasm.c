void disasm_func(struct BytecodeGen *bg, struct Function *proc) {
	struct Chunk *c = &proc->c;
	struct VM *vm = malloc(sizeof(struct VM));
	vm->regs[0] = 0;
	vm->ch = c;
	vm->ip = 0;
	if (proc->allocas) {
		vm->ch = proc->allocas;
		for (;;) {
			if (vm->ip >= proc->allocas->len) {
				break;
			}
			struct AllocaSI *inst = get_instr(vm);
			printf("allocas: %d, %d\n", inst->reg, inst->len);
			vm->ip += sizeof(struct AllocaSI);
		}
		vm->ip = 0;
	}
	vm->ch = c;
	for (;;) {
		if (vm->ip >= vm->ch->len) {
			break;
		}
		for (int i = 0; i < c->dlen; i++) {
			if (vm->ch->debug[i].code_pos > vm->ip) {
				printf("line %d, ", vm->ch->debug[i-1].line_pos);
				break;
			} else if (i == vm->ch->dlen - 1) {
				printf("line %d, ", vm->ch->debug[i].line_pos);
				break;
			}
		}
		printf("%llx: ", vm->ip);
		switch (vm->ch->code[vm->ip]) {
			case i_loaddu:
				{
					struct MemI *inst = get_instr(vm);
					printf("loaddu: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadwu:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadwu: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadhu:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadhu: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadbu:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadbu: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadd:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadd: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadw:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadw: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadh:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadh: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_loadb:
				{
					struct MemI *inst = get_instr(vm);
					printf("loadb: to: %d, from: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_stored:
				{
					struct MemI *inst = get_instr(vm);
					printf("stored: from: %d, to: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storew:
				{
					struct MemI *inst = get_instr(vm);
					printf("storew: from: %d, to: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storeh:
				{
					struct MemI *inst = get_instr(vm);
					printf("storeh: from: %d, to: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_storeb:
				{
					struct MemI *inst = get_instr(vm);
					printf("storeb: from: %d, to: %d + %lld", inst->reg, inst->addr, inst->offset);
					vm->ip += sizeof(struct MemI);
					break;
				}
			case i_addi:
				{
					struct AddImmediateI *inst = get_instr(vm);
					printf("addi %d %d %lld", inst->reg1, inst->reg2, inst->val);
					vm->ip += sizeof(struct AddImmediateI);
					break;
				}
			case i_add:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("add %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_sub:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("sub %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mul:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("mul %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_div:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("div %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mod:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("mod %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("mulu %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("divu %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_modu:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("modu %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_not:
				{
					struct SimpleI *inst = get_instr(vm);
					printf("not %d %d", inst->reg1, inst->reg2);
					vm->ip += sizeof(struct SimpleI);
					break;
				}
			case i_addfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("addfd %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_subfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("subfd %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("mulfd %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("divfd %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_addfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("addfs %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_subfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("subfs %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_mulfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("mulfs %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_divfs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("divfs %d %d %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			/*case i_modfd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("modfd %d %d (%lf) %d (%lf)", inst->reg1, inst->reg2, get_reg(vm, inst->reg2), inst->reg3, get_reg(vm, inst->reg3));
					u64 reg2 = get_reg(vm, inst->reg2);
					u64 reg3 = get_reg(vm, inst->reg3);
					double res = *(double*)&reg2 % *(double*)&reg3;
					set_reg(vm, inst->reg1, *(u64*)&res);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}*/ // might include fmod sometime
			case i_ftoi:
				{
					struct ConvI *inst = get_instr(vm);
					switch (inst->fmt1) {
						case ffmt_single:
							{
							printf("ftoi %d %d", inst->reg1, inst->reg2);
							}
							break;
						case ffmt_double:
							printf("ftoi %d %d", inst->reg1, inst->reg2);
					}
					vm->ip += sizeof(struct ConvI);
					break;
				}
			case i_itof:
				{
					struct ConvI *inst = get_instr(vm);
					switch (inst->fmt2) {
						case ffmt_single:
							{
							printf("itof %d %d", inst->reg1, inst->reg2);
							}
							break;
						case ffmt_double:
							printf("itof %d %d", inst->reg1, inst->reg2);
					}
					vm->ip += sizeof(struct ConvI);
					break;
				}
			case i_call:
				{
					struct CallI *inst = get_instr(vm);
					if (inst->func == f_bc_call) {
						printf("call ___bytecode_call, -1");
					} else if (inst->func == f_call_native) {
						// printf("call foreign %s from lib %s\n", (bg->flibs->cprocs[lib_idx][func_type])->name, bg->flibs->libname[lib_idx]);
						printf("call ___call_native");
					} else {
						printf("call %s, %d", bg->functions[inst->func - f_last].name, inst->func - f_last);
					}
					vm->ip += sizeof(struct CallI);
					break;
				}
			case i_allocas:
				{
					struct AllocaSI *inst = get_instr(vm);
					printf("allocas: %d, %d", inst->reg, inst->len);
					vm->ip += sizeof(struct AllocaSI);
					break;
				}
			case i_allocad:
				{
					struct SimpleI *inst = get_instr(vm);
					printf("allocad: %d, %d (%lld)", inst->reg1, inst->reg2, get_reg(vm, inst->reg2));
					vm->ip += sizeof(struct SimpleI);
					break;
				}
			case i_aarg:
				{
					struct ArgI *inst = get_instr(vm);
					printf("aarg: %d", inst->reg);
					vm->ip += sizeof(struct ArgI);
					break;
				}
			case i_larg:
				{
					struct ArgI *inst = get_instr(vm);
					printf("larg: %d", inst->reg);
					vm->ip += sizeof(struct ArgI);
					break;
				}
			case i_jmp:
				{
					struct JmpI *inst = get_instr(vm);
					printf("jmp %d + %llx", inst->reg, inst->addr);
					vm->ip += sizeof(struct JmpI);
					break;
				}
			case i_jmpr:
				{
					struct JmpRI *inst = get_instr(vm);
					printf("jmpr %llx", inst->addr);
					vm->ip += sizeof(struct JmpRI);
					break;
				}
			case i_bne:
				{
					struct BranchI *inst = get_instr(vm);
					printf("bne: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_beq:
				{
					struct BranchI *inst = get_instr(vm);
					printf("beq: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_blt:
				{
					struct BranchI *inst = get_instr(vm);
					printf("blt: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_bltu:
				{
					struct BranchI *inst = get_instr(vm);
					printf("bltu: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_bge:
				{
					struct BranchI *inst = get_instr(vm);
					printf("bge: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_bgeu:
				{
					struct BranchI *inst = get_instr(vm);
					printf("bgeu: %d, %d, %llx", inst->reg1, inst->reg2, inst->addr);
					vm->ip += sizeof(struct BranchI);
					break;
				}
			case i_flts:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_flts: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fges:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_fges: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_feqs:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_feqs: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fltd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_fltd: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_fged:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_fged: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_feqd:
				{
					struct ArithmeticI *inst = get_instr(vm);
					printf("i_feqd: %d, %d, %d", inst->reg1, inst->reg2, inst->reg3);
					vm->ip += sizeof(struct ArithmeticI);
					break;
				}
			case i_ret:
				{
					// struct RetI *inst = get_instr(vm);
					// TBD
					printf("ret");
					vm->ip += sizeof(struct RetI);
					break;
				}
			case i_loadrd:
				{
					struct LoadRDI *inst = get_instr(vm);
					printf("load rodata: %d, %llx", inst->reg, inst->addr);
					vm->ip += sizeof(struct LoadRDI);
					break;
				}
		}
		printf("\n");
	}
	free(vm);
}
