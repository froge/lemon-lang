u64 size_of(struct Typeinfo *t) {
	if (t->pointer_level > 0) {
		return 8; // TODO 32
	}
	if (t->type == t_array) {
		struct ArrayInfo *ai = t->array_info;
		switch (ai->atype) {
			case arrayt_dynamic:
				return 24;
			case arrayt_static:
				return 8;
			case arrayt_stack:
				return ai->type->size * ai->capacity;
		}
	}
	return t->size;
}

u64 alignment_of(struct Typeinfo *t) {
	if (t->type == t_array) {
		return 8; // @Wordsize
	}
	if (t->type == t_struct) {
		return alignment_of(t->struct_info->types[0]);
	}
	return size_of(t);
}

u64 fix_alignment(u64 size, u64 pos) {
	u64 offset = pos;
	switch (size) {
		case 8:
			if (offset % 8 != 0) {
				offset = offset - (offset % 8) + 8;
			}
			break;
		case 4:
			if (offset % 4 != 0) {
				offset = offset - (offset % 4) + 4;
			}
			break;
		case 2:
			if (offset % 2 != 0) {
				offset = offset - (offset % 2) + 2;
			}
			break;
	}
	return offset - pos;
}

u64 offset_of_internal(struct Typeinfo *t, u64 offs) {
	u64 temp;
	if (t->type == t_array) {
		if (t->array_info->atype != arrayt_dynamic) {
			temp = offset_of_internal(t->array_info->type, 0) * t->array_info->capacity;
			temp += fix_alignment(8, offs); // @Wordsize
			return temp;
		}
		return 24; // @Wordsize
	}
	temp = size_of(t);
	temp += fix_alignment(alignment_of(t), offs);
	return temp;
}

u64 offset_of(struct Typeinfo *t) {
	if (t->pointer_level > 0) {
		return 8; // TODO 32
	}
	if (t->type == t_array) {
		if (t->array_info->atype != arrayt_dynamic) {
			if (t->array_info->capacity) {
				return offset_of_internal(t->array_info->type, 0) * t->array_info->capacity;
			} else {
				return 16; // @Wordsize
			}
		} else {
			return 24; // @Wordsize
		}
	}
	return t->size;
}
